<?php

require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>



<title>Pintura elestrost&aacute;tica - Original Portas</title>
<base>
<meta name="description"
	content="A pintura eletrost&aacute;tica &eacute; uma tinta em p&oacute; com processo de acabamento a seco que se tornou extremamente popular desde a sua introdu&ccedil;&atilde;o. Representando mais de 15% do mercado total de acabamento industrial, o p&oacute; &eacute; usado em uma ampla gama de produtos. Mais e mais empresas buscam est&aacute; t&eacute;cnica de tinta em p&oacute; para um acabamento dur&aacute;vel e de alta qualidade, permitindo a produ&ccedil;&atilde;o m&aacute;xima, eficiência aprimorada e conformidade ambiental simplificada. Usados ​​como acabamentos funcionais (protetores) e decorativos, revestimentos em p&oacute; est&atilde;o disponíveis em uma gama de cores e texturas, os avan&ccedil;os tecnol&oacute;gicos resultaram em excelentes propriedades de desempenho.">
<meta name="keywords"
	content="Pintura Eletrost&aacute;tica, pinturas, eletrost&aacute;ticas, pinturas eletrost&aacute;ticas, tinta em p&oacute;, tinta, p&oacute;">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="S&atilde;o Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="pintura-eletrostatica.php">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Pintura elestrost&aacute;tica - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/pintura-eletrostatica-01.png">
<meta property="og:url" content="pintura-eletrostatica.php">
<meta property="og:description"
	content="A pintura eletrost&aacute;tica &eacute; uma tinta em p&oacute; com processo de acabamento a seco que se tornou extremamente popular desde a sua introdu&ccedil;&atilde;o. Representando mais de 15% do mercado total de acabamento industrial, o p&oacute; &eacute; usado em uma ampla gama de produtos. Mais e mais empresas buscam est&aacute; t&eacute;cnica de tinta em p&oacute; para um acabamento dur&aacute;vel e de alta qualidade, permitindo a produ&ccedil;&atilde;o m&aacute;xima, eficiência aprimorada e conformidade ambiental simplificada. Usados ​​como acabamentos funcionais (protetores) e decorativos, revestimentos em p&oacute; est&atilde;o disponíveis em uma gama de cores e texturas, os avan&ccedil;os tecnol&oacute;gicos resultaram em excelentes propriedades de desempenho.">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=iso-Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">

<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro3">
				<img src="imagens/pintura-eletrostatica-11.png" width="100%" height="auto">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="eletro_div">
			<div class="row">
				<div class="col-md-12">
					<h1>Pintura Eletrost&aacute;tica</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					</div>
				<div class="col-md-6 col-xs-12 col-sm-12" style="background-color:transparent;">
					<h2> O que &eacute; Pintura Eletrost&aacute;tica ?</h2>
<p>A pintura eletrost&aacute;tica &eacute; uma tinta em p&oacute; com processo de acabamento a seco que se tornou extremamente popular desde a sua introdu&ccedil;&atilde;o. Representando mais de 15% do mercado total de acabamento industrial, o p&oacute; &eacute; usado em uma ampla gama de produtos. Mais e mais empresas buscam est&aacute; t&eacute;cnica de tinta em p&oacute; para um acabamento dur&aacute;vel e de alta qualidade, permitindo a produ&ccedil;&atilde;o m&aacute;xima, eficiência aprimorada e conformidade ambiental simplificada. Usados ​​como acabamentos funcionais (protetores) e decorativos, revestimentos em p&oacute; est&atilde;o disponíveis em uma gama de cores e texturas, os avan&ccedil;os tecnol&oacute;gicos resultaram em excelentes propriedades de desempenho.</p>
<br></div>

              <div class="col-md-6 col-xs-12 col-sm-12" style="background-color:transparent;">
				<img src="imagens/pintura-eletrostatica-07.jpg" width="100%" height="auto" style="margin-top: 20px;"> 
			</div>

			<div class="col-md-6 col-xs-12 col-sm-12" style="background-color:transparent;">
				<img src="imagens/pintura-eletrostatica-06.jpg" width="100%" height="auto" style="margin-top: 20px;"> 
			</div>
				<div class="col-md-6 col-xs-12 col-sm-12" style="background-color:transparent;">
				<br>
  <h2 style="margin-top: 20px;">Durabilidade da pintura eletrost&aacute;tica</h2>
  
<p>A Pintura Eletrost&aacute;tica tem um acabamento de alta qualidade encontrado em milhares de produtos em que você entra em contato com cada dia. O revestimento em p&oacute; protege a maquinaria mais &aacute;spera e resistente, bem como os itens dom&eacute;sticos que você depende diariamente. Ele fornece um acabamento mais dur&aacute;vel do que as tintas líquidas podem oferecer, enquanto ainda oferecem um acabamento atraente. Os produtos revestidos com p&oacute; s&atilde;o mais resistentes à diminui&ccedil;&atilde;o da qualidade do revestimento como resultado do impacto, umidade, produtos químicos, luz ultravioleta e outras condi&ccedil;ões clim&aacute;ticas extremas. Por sua vez, isso reduz o risco de arranhões, raspas, abrasões, corros&atilde;o, desvanecimento e outros problemas de desgaste.

&eacute; duro. Parece &oacute;timo. E dura muito, muito tempo. Al&eacute;m de ser dur&aacute;vel, o revestimento em p&oacute; &eacute; uma escolha atraente devido a vantagens ambientais.</p>       
</div>

				<div class="col-md-6 col-xs-12 col-sm-12" style="background-color:transparent;">
				<br>  
<h2 style="margin-top: 20px;">Como funciona a Pintura eletrost&aacute;tica em p&oacute;?</h2>
<p>
A composi&ccedil;&atilde;o da pintura Eletrost&aacute;tica &eacute;  baseada em sistemas de resina polim&eacute;rica, combinados com pigmentos, agentes de nivelamento, modificadores de fluxo e outros aditivos. Esses ingredientes s&atilde;o fundidos, arrefecidos e moídos em um p&oacute; uniforme semelhante à farinha de trigo. Um processo chamado deposi&ccedil;&atilde;o de pulveriza&ccedil;&atilde;o eletrost&aacute;tica (DPE) &eacute; tipicamente usado para conseguir a aplica&ccedil;&atilde;o do revestimento em p&oacute; a um substrato met&aacute;lico. Este m&eacute;todo de aplica&ccedil;&atilde;o usa uma pistola de pulveriza&ccedil;&atilde;o, que aplica uma carga eletrost&aacute;tica às partículas de p&oacute;, que s&atilde;o ent&atilde;o atraídas para a pe&ccedil;a aterrada. Ap&oacute;s a aplica&ccedil;&atilde;o do revestimento em p&oacute;, as pe&ccedil;as entram em um forno de cura onde, com a adi&ccedil;&atilde;o de calor, o revestimento reage quimicamente para produzir cadeias moleculares longas, resultando em alta densidade de reticula&ccedil;&atilde;o. Essas cadeias moleculares s&atilde;o muito resistentes à ruptura. Este tipo de aplica&ccedil;&atilde;o &eacute; o m&eacute;todo mais comum de aplica&ccedil;&atilde;o de p&oacute;.

A pintura eletrost&aacute;tica em p&oacute; s&atilde;o f&aacute;ceis de usar, ambientalmente amig&aacute;veis, econômicos e pr&aacute;ticas</p>       
</div>

        	<div class="col-md-6 col-xs-12 col-sm-12" style="background-color:transparent;">
				<img src="imagens/pintura-eletrostatica-02.jpg" width="100%" height="auto" style="margin-top: 20px;"> 
			</div>
				</div>
			</div>
		</div>
	</div>
	<br>
		<br>
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('554591058441', '554591047291'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>
</html>