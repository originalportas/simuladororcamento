<?php
require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script>
<script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Niveladora de Docas - Original Portas</title>
<base>
<meta name="description"
	content="Muitos operadores de empilhadeiras viajam através de
							niveladoras de doca convencionais, mais de 100.000 vezes por ano.
							Com cada passagem, eles experimentam um efeito irregular r
							desagradável criado pelos solavancos e lacunas que existem em
							projetos de nivelador desatualizados - assim como seus
							equipamentos e os produtos que estão sendo transportados.
							Também conhecida como choque de atracação ou vibração de corpo inteiro,
							essas vibrações podem levar a lesões crônicas nas costas e no
							pescoço, vazamentos de produtos e custos de reparo de
							equipamentos. Essas condições podem ser reduzidas ou eliminadas
							com um projeto nivelador de doca - todos os quais são testados
					        pela nossa equipe especializada.">
<meta name="keywords"
	content="Original Portas, kit porta de a&ccedil;o automatica de enrolar, porta, portas,kit,automaticas.">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="S&atilde;o Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="niveladora-docas">
<meta name="author" content="tworock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Niveladora para docas - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/niveladora-docas.png">
<meta property="og:url" content="niveladora-docas">
<meta property="og:description"
	content="Muitos operadores de empilhadeiras viajam através de
							niveladoras de doca convencionais, mais de 100.000 vezes por ano.
							Com cada passagem, eles experimentam um efeito irregular r
							desagradável criado pelos solavancos e lacunas que existem em
							projetos de nivelador desatualizados - assim como seus
							equipamentos e os produtos que estão sendo transportados.
							Também conhecida como choque de atracação ou vibração de corpo inteiro,
							essas vibrações podem levar a lesões crônicas nas costas e no
							pescoço, vazamentos de produtos e custos de reparo de
							equipamentos. Essas condições podem ser reduzidas ou eliminadas
							com um projeto nivelador de doca - todos os quais são testados
					        pela nossa equipe especializada.">

<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">
function disableselect(e){ 
return false 
} 
function reEnable(){ 
return true 
} 
//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript"
	src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro4">
				<img src="imagens/niveladora-docas.png" width="100%">
			</div>
		</div>
		<div class="container">
			<div class="eletro_div">
				<div class="row">
					<div class="col-md-12">
						<h1 style="text-align: center; font-size: 30px;">NIVELADORES DE
							DOCAS</h1>
						<hr style="width: auto; height: 2px; background-color: #ccc;">

						<h2>Procurando reduzir acidentes de trabalho,danos em equipamentos
							e tempo na platafroma de carga ?</h2>

						<p>Muitos operadores de empilhadeiras viajam através de
							niveladoras de doca convencionais, mais de 100.000 vezes por ano.
							Com cada passagem, eles experimentam um efeito irregular r
							desagradável criado pelos solavancos e lacunas que existem em
							projetos de nivelador desatualizados - assim como seus
							equipamentos e os produtos que estão sendo transportados.</p>
						<p>Também conhecida como choque de atracação ou vibração de corpo
							inteiro, essas vibrações podem levar a lesões crônicas nas costas
							e no pescoço, vazamentos de produtos e custos de reparo de
							equipamentos.</p>
						<p>Essas condições podem ser reduzidas ou eliminadas com um
							projeto nivelador de doca - todos os quais são testados pela
							nossa equipe especializada.</p>
					</div>

					<div class="col-md-12 col-xs-12 col-sm-12" style="background-color:transparent;">
						<h2>Beneficios</h2>
						<p>Proporciona uma transição suave para empilhadeiras, do piso do
							depósito até as camas do reboque - mesmo em reboques
							desbalanceados - com uma dobradiça traseira de raio constante,
							controle de coroa de dois pontos e um chanfro de rebordo
							otimizado.</p>
						<p>Previna-se contra acidentes como quedas de docas vazias e
							facilite o carregamento final sem obstruções.</p>
						<p>Ofereça flexibilidade para o uso de empilhadeiras de 3 e 4
							rodas com engenharia estrutural exclusiva</p>
						<p>A escolha de um nivelador de docas de transição suave resulta
							em menos acidentes com funcionários, menos danos ao produto e ao
							equipamento e custos baixos de propriedade vitalícia. Nossos
							projetos estão sempre evoluindo para enfrentar os desafios que
							você enfrenta na doca de carregamento.</p>
						<br>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<br>
						<h2 style="margin-top: 50px;">Niveladora Eletro-Hidráulica</h2>
						<p>Há uma boa razão para escolher um que ofereça proteção contra
							queda livre por meio de um fusível de velocidade.</p>

						<p>
							Sem esse recurso, o <strong>Nivelador Eletro-Hidraúlico </strong>pode
							descer rapidamente se o caminhão se afastar inesperadamente, o
							que poderia levar a ferimentos pessoais ou produtos danificados e
							investimentos de capital.
						</p>

						<p>
							Um <strong>Nivelador de Doca Eletro-Hidráulico</strong>
							devidamente equipado pode ajudar a evitar a descida rápida do
							nivelador, mesmo ao gerenciar cargas pesadas; Ele também pode
							ajudar a manter uma inclinação segura e razoável do piso da
							instalação até a plataforma do trailer enquanto flutua abaixo e
							acima da altura da doca.
						</p>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<img alt="niveladora eletro-hidraulica" src="imagens\niveladora-hidraulica.png"
							width="80%" height="auto"
							style="margin-top: 40px; margin-left: 30px;">
					</div>


					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<br>
						<h2 style="margin-top: 50px;">Niveladora Mola a gás</h2>
						<p>
							Em comparação com <strong>Niveladores de Doca Mecânicos</strong>,
							o <strong>Nivelador de Doca de Mola a Gás </strong>é muito mais
							fácil de operar. Para levantar os pistoes de mola a gás, basta
							puxar uma corrente e a niveladora subira levemente, para descê-la
							basta remover a trava de segurança e precionar a rampa para baixo
						</p>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<img alt="niveladora mola a gás" src="imagens\niveladora-mola-gas.png"
							width="80%" height="auto"
							style="margin-top: 40px; margin-left: 30px;">
					</div>

					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<br>
						<h2 style="margin-top: 50px;">Niveladora Manual</h2>
						<p>
							O <strong>Nivelador Manual</strong> é ideal para aplicações em
							que a instalação de niveladores do tipo embutir não são viáveis
							ou quando as alturas padrões da frota são atendidas na plataforma
							de carga.
						</p>
						<p>
							A <strong>Niveladora Manual</strong> é facilmente instalada na
							face da doca, fornecendo uma unidade de acoplamento de
							auto-armazenamento eficiente, substituindo placas de encaixe
							pesadas.
						</p>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<img alt="niveladora manual" src="imagens\niveladora-manual.png"
							width="80%" height="auto"
							style="margin-top: 40px; margin-left: 30px;">
					</div>



					<div class="col-md-12 col-xs-12 col-sm-12" style="background-color:transparent;">
						<br>
						<h2 style="margin-top: 50px;">Modelo de Embutir</h2>
						<hr style="width: auto; height: 2px; background-color: #ccc;">

					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<img alt="niveladora de embutir"
							src="imagens\niveladora-embutir.png" width="80%" height="auto"
							style="margin-top: 40px; margin-left: 30px;">
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<img alt="niveladora de embutir desenho"
							src="imagens\desenho-niveladora.jpg" width="80%" height="auto"
							style="margin-top: 40px; margin-left: 30px;">
					</div>
					<div class="col-md-12 col-xs-12 col-sm-12" style="background-color:transparent;">
						<br>
						<h2 style="margin-top: 50px;">Modelo Frontal Suspenso</h2>
						<hr style="width: auto; height: 2px; background-color: #ccc;">

					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<img alt="Niniveladora frontal de mola a gár"
							src="imagens\niveladora-frontal.jpg" width="80%" height="auto"
							style="margin-top: 40px; margin-left: 30px;">
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<img alt="Niniveladora frontal eletrohidraulica"
							src="imagens\niveladora-frontal2.jpg" width="80%" height="auto"
							style="margin-top: 40px; margin-left: 30px;">
					</div>
					<div class="col-md-12 col-xs-12 col-sm-12" style="background-color:transparent;">
						<br>
						<h2 style="margin-top: 80px;">Selo para Docas</h2>
						<hr style="width: auto; height: 2px; background-color: #ccc;">

						<p>A barreira formada pelo nosso selo de doca fornecem a vedação
							entre a plataforma e o Veículo, trazendo segurança para a carga;
							se livrando de contaminantes transmitidos pelo ar, insetos e mau
							tempo. Além de aumentar a segurança do edificio</p>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<img alt="selo para docas" src="imagens\selo-doca.png" width="80%"
							height="auto" style="margin-top: 40px; margin-left: 30px;">
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<br>
						<h2 style="margin-top: 50px;">Funcionamento do Selo de Doca</h2>
						<p>Veículos quando posicionados na doca comprimem as almofadas
							laterais resilientes, enquanto a cortina de largura total está em
							alinhamento com o caminhão, eliminando a lacuna entre o selo e a
							carroceria do veículo.</p>
						<p>Essa separação dos ambientes internos e externos permite uma
							área segura de carga, garantindo conforto e eficiência aos
							funcionários.</p>
												<div class="col-md-6 col-xs-6 col-sm-6">
							
							<img alt="selo doca demostração" src="imagens/selo-doca2.jpg" style="width: 100%;"></div>
					</div>
				</div>
			</div>
		</div>
		<br> <br>
	</div>

<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('554591058441', '554591047291'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>	