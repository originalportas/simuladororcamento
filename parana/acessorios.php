<?php
require 'main.php';
require 'footer.php';
?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
<title>Acessorios para Portas de Aço- Original Portas</title>
<base>
<meta name="description"
	content="A Original Portas oferece também uma linha completa de acessórios
						para portas de aço automÁticas. ">
<meta name="keywords"
	content="Acessórios para Portas de Enrolar, acessório, portas de aço, portas, automáticas, centarl do motor, infravermelho, borracha soleira, nobreak, controle remoto, fita auto lubrificante, fechadura portas, anti queda, antiquedas, trava lamina,  laminas de aço">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="acessorios">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Acessorios para Portas de Aço- Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/acessorios/trava-laminas.png">
<meta property="og:url" content="acessorios">
<meta property="og:description"
	content="A Original Portas oferece também uma linha completa de acessórios
						para portas de aço automÁticas. ">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
	<div class="container">
		<div class="class_aut2">
			<div class="row" style="margin-top: 50px;">

				<div class="col-md-12" style="background-color: transparent;"><br>

					<H1>ACESSÓRIOS</H1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					<br>
					<h4 style="color: #408080;">
						A Original Portas oferece também uma linha completa de acessórios
						para portas de aço automÁticas. <br>Conheça toda a nossa linha de
						acessórios:
					</h4>
				</div>
				<div class="col-md-4 col-xs-4"
					style="background-color: transparent;">
					<img src="imagens/acessorios/anti-quedas.png"
						title="Antiquedas para Portas de Aço Automáticas" width="100%"
						height="auto">
					<p>
						<br>
					</p>
				</div>
				<div class="col-md-4 col-xs-4"
					style="background-color: transparent;">
					<img src="imagens/acessorios/borracha-soleira.png"
						title="Borracha de Proteção para Soleira" width="100%"
						height="auto">
					<p>
						<br>
					</p>
				</div>
				<div class="col-md-4 col-xs-4"
					style="background-color: transparent;">
					<img src="imagens/acessorios/central-para-automatizador.png"
						title="Central para Automatizadores" width="100%" height="auto">
					<p>
						<br>
					</p>
				</div>
				<div class="col-md-4 col-xs-4"
					style="background-color: transparent;">
					<img src="imagens/acessorios/controle-remoto.png"
						title="Controle Remoto para Portas Automáticas" width="100%"
						height="auto">
					<p>
						<br>
					</p>
				</div>

				<div class="col-md-4 col-xs-4"
					style="background-color: transparent;">
					<img src="imagens/acessorios/fechadura-lateral.png"
						title="Fechadura Lateral para Portas de aço" width="100%"
						height="auto">
					<p>
						<br>
					</p>
				</div>
				<div class="col-md-4 col-xs-4"
					style="background-color: transparent;">
					<img src="imagens/acessorios/nobreak-para-automatizadores-dc.png"
						title="Nobreak para Automatizadores DC" width="100%" height="auto">
					<p>
						<br>
					</p>
				</div>
				<div class="col-md-4 col-xs-4"
					style="background-color: transparent;">
					<img src="imagens/acessorios/sensor-infravermelho.png"
						title="Sensor Infravermelho para Portas automáticas" width="100%"
						height="auto">
					<p>
						<br>
					</p>
				</div>
				<div class="col-md-4 col-xs-4"
					style="background-color: transparent;">
					<img src="imagens/acessorios/trava-laminas.png"
						title="trava laminas para portas de aço" width="100%"
						height="auto">
					<p>
						<br>
					</p>
				</div>
				<p>
					<br>
				</p>
		<p></div>
		<p><br></p>

			<div class="col-md-12" style="background-color: transparent;">


				<h2>Informações:</h2>
				<br>
				<p>
					<strong>Anti-Quedas:</strong> Utilizado para aumentar a sua
					segurança não deixando que a porta desça muito rápido , caso ocorra
					defeito nas molas.
				</p>
				<br>
				<p>
					<strong>Borracha de Vedação:</strong> Essa fita é utilizada abaixo
					da soleira e cria uma vedação entre a soleira e o piso no momento
					em que a porta de aço entra em contato com o piso, ela também
					proteje o piso de riscos
				</p>
				<br>
				<p>
					<strong>Central eletrônica:</strong> A central faz a comunicação
					entre o Automatizador e o controle remoto.
				</p>
				<br>
				<p>
					<strong>Controle remoto:</strong> Com o auxilio da Central torna
					possivel o acionamento remoto do automatizador
				</p>
				<br>
				<p>
					<strong>Nobreak:</strong> Deixa sua porta de aço com automatizador
					DC operando mesmo sem energia elétrica por um determinado tempo.
				</p>
				<br>
				<p>
					<strong>Fechaduras:</strong> Aumentam ainda mais a segurança da sua
					porta , reforçando com travas resistentes, que podem ser colocadas
					no centro ou laterais, sua abertura é feita por chaves
				</p>
				<br>
				<p>
					<strong>Sensor infravermelho:</strong> Ao detectar movimentos nos
					sensores , a porta pausa o seu funcionamento, impedindo que
					continue o seu funcionamento.
				</p>
				<br>
				<p>
					<strong>Trava lâminas:</strong> Auxiliam na fluidez da porta de
					aço reduzindo também o seu barulho e lubrificação constantes das
					guias, tudo isso porque eles travam as laminas impedindo seu
					deslizamento laterail.
				</p>
				<br>
				</div>
			</div>
		</div>
	<br>
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('554591058441', '554591047291'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	

<?php echo $footer;?>
</body>
</html>