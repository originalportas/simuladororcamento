<?php
require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
<title>Porta Rápida Dobrável - Original Portas</title>
<base>
<meta name="description"
	content="A porta rápida dobrável são preparadas pra áreas
							de trafego muito intenso , por esse meio a sua empresa consegue
							alcançar o objetivo na produtividade logística com qualidade
							total.">
<meta name="keywords"
	content="Porta rápida Dobravél, porta, rapida, dobravel, PV, porta enrolar, porta de plastico, porta de toldo, toldo, automática">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="porta-rapida-dobravel">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Porta Rápida Dobrável - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image"
	content="imagens/porta_rapida/porta-rapida-dobravel-01.png">
<meta property="og:url" content="porta-rapida-dobravel">
<meta property="og:description"
	content="A porta rápida dobrável são preparadas pra áreas
							de trafego muito intenso , por esse meio a sua empresa consegue
							alcançar o objetivo na produtividade logística com qualidade
							total.">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2">
								<img src="imagens/porta_rapida/porta-rapida-enrolavel-dobravel.png" width="100%" height="auto" >

			</div>
		</div>
	</div>
	<div class="container">
		<div class="class_aut2">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<h1 style="margin-top: -20px;">Portas Rápidas de Enrolar</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					<div class="col-md-12">
						<p>As Portas Rápidas de Enrolar da Original Portas, possuem um
							sistema de recolhimento rápido e automático, sendo ativado por
							sensores de movimento, tirando totalmente a necessidade de
							pressionar algum tipo de botão de acionamento, sendo por controle
							remoto ou manual. São ideais para Indústrias ou Estabelecimentos
							Comerciais que necessitam de soluções Logísticas com velocidade e
							qualidade, sem abrir mão de um ambiente moderno e ao mesmo tempo
							funcional. Confeccionadas com materiais de alta resistência,
							possuem máxima qualidade e são extremamente duráveis, contando
							ainda com a vantagem de se adaptarem perfeitamente em qualquer
							tipo de negócio ou construção. O Funcionamento das Portas Rápidas
							de Enrolar é bem simples, ao passar pelo sensor que é ativado
							pelo movimento, (podendo ser fluxo de pessoas, máquinas ou
							equipamentos) aciona-se o recolhimento da porta em 2 segundos,
							logo após 10 segundos sem movimentos nos sensores, a porta se
							fecha automaticamente.</p>
					</div>
					<div class="col-md-12" style="margin-top: 30px;">
				  <video width="100%" muted loop autoplay>
                    <source src="video/porta_rapida.mp4" type="video/mp4">
                  </video>
                  </div>
					<div class="col-md-12 col-sm-12">
						<br>
						<h2>Quais as Características de uma Porta Rápida de Enrolar?</h2>
						<p>As Portas Rápidas de Enrolar são confeccionadas em PVC,
							material leve e resistente, podendo ser coloridas, transparentes
							ou mistas, tendo em seu centro um outro tipo de PVC incolor e
							translúcido, que possibilita enxergar o outro lado mesmo com a
							porta fechada, possibilitando a visibilidade e diminuindo os
							riscos de acidentes.</p>
					</div>
					<div class="col-md-6 col-sm-12" style="background-color:transparent;">
						<br>
						<h4>- RESPEITA OS REQUISITOS DE HIGIENE</h4>
						<p>A eficiência da produção, velocidade de processamento, higiene e armazenamento do frio são cruciais em todos os ambientes de processamento de alimentos. As Portas Rápidas de Enrolar da Original Portas ajudam a atender esses requisitos. A cortina da porta de PVC é flexível e uniformemente texturizada, uma porta de alta velocidade e muito fácil de limpar. A porta de enrolar pode ser instalada com uma estrutura de aço galvanizado e postes laterais. As impurezas não se acumulam e torna-se possível uma boa higiene.</p>
					</div>
					<div class="col-md-6 col-sm-12" style="background-color:transparent;">
						<br>
						<h4>- MANTÉM A BAIXAS TEMPERATURAS.</h4>
						<p>Porta rápida de enrolar abre e fecha muito rápido. O tempo que uma porta permanece aberta é limitado a poucos segundos. Isso evita flutuações de temperatura na corrente de ar frio, protegendo a qualidade dos alimentos. Uma vedação superior em torno do perímetro total da porta rápida de enrolar mantém a poeira, sujeira e insetos fora de suas áreas de armazenamento e processamento. Isso ajuda a evitar contaminações.</p>
					</div>
					<div class="col-md-12 col-sm-12" style="background-color:transparent;">
		   <img src="imagens/porta_rapida/porta_rapida_04.png" width="100%" height="auto">                  
			</div>
					<div class="col-md-12 col-sm-12" style="background-color:transparent;">
						<br>
						<h1>Porta Rápida Dobravél</h1>
						<hr style="width: auto; height: 2px; background-color: #ccc;">
						<p>A Porta Rápida Dobrável é preparada pra áreas de trafego muito intenso, por esse meio a sua empresa consegue alcançar o objetivo na produtividade logística com qualidade total.</p>
					</div>
					<div class="col-md-4" style="margin-top: 30px; background-color:transparent;">

						<h2>Alta Velocidade</h2>
						<p>Esta porta rápida dobrável leva 1 metro por 1,5 segundos para abrir e fechar , é a segunda porta mais rápida que temos , ficando atrás da porta rápida de enrolar que leva 1 metro por segundo para abrir e fechar.</p>
						<h2>Composição estrutural</h2>
						<p>Composta por dois elementos e uma viga superior, onde se oculta o mecanismo da porta, esta estrutura é confeccionada com chapa galvanizada ou aço inox, possui escovas de vedação em todo o perímetro.</p>
					</div>
					<div class="col-md-8">
						<br> <img src="imagens/porta_rapida/porta-rapida-dobravel-07.jpg"
							width="100%" height="auto" style="margin-top: 20px;">
					</div>

					<div class="col-md-8" style="margin-top: 50px;">
						<img src="imagens/porta_rapida/porta-rapida-dobravel-10.jpg"
							width="100%" height="auto">
					</div>
					<div class="col-md-4" style="margin-top: 20px;">
						<h2>Composição do Tecido</h2>
						<p>
							Confeccionado com um tecido multifilamento de poliéster revestido de PVC em ambos os lados, tornando -se impermeável e antichama, a porta rápida dobrável também possui proteção contra Raios UV e nevoas salinas; com reforços transversais de barras de aço galvanizado.
						</p></div>
						<div class="col-md-6" style="margin-top: 20px; background-color:transparent;">
						
						<h2>Indicações</h2>
						<p>As Portas Rápidas de Lona e as Dobráveis são ideais para uma infinidade de aplicações, geralmente são utilizadas em locais que possuem uma alta necessidade de aberturas e fechamentos, juntamente rapidez e segurança. Nós da Original Portas recomendamos o seu uso em:</p><br><br>
						<p>- Indústrias Farmacêuticas.<br>
- Indústrias de Alimentos.<br>
- Indústrias de Cosméticos.<br>
- Câmaras Frias e Frigoríficos.<br>
- Indústrias de Produtos Químicos.<br>
- Super e Hipermercados.<br>
- Centros de Distribuição.<br>
- Montadoras e Concessionárias.<br><br></p></div>
<div class="col-md-6" style="margin-top: 30px; background-color:transparent;">
				  <video width="100%" muted loop autoplay>
                    <source src="video/porta-dobravel.mp4" type="video/mp4">
                  </video><br><br>
</div>
<div class="col-md-12">


<p>

						Enfim, as Portas Rápidas de Enrolar da Original Portas são diferenciadas principalmente por sua rapidez e leveza, além de resistentes e segura, e com todos estas vantagens são a melhor escolha para lugares que necessitam destes atributos, pouco contato com o ambiente externo, evitar contaminações e pragas.</p>
					</div>
					
						<div class="div_rapid">
						<div class="col-md-12 col-xs-12">
							<hr style="width: auto; height: 2px; background-color: green;">

							<h2>
								<i class="fa fa-check" style="color: green; font-size: 30px;"></i>
								Benefícios
							</h2>

						</div>
						<div class="col-md-4 col-xs-4">

							<p>
								<br>
								<Strong>• Respeita os requisitos de higiene.</strong><br> O
								tecido da porta rápida de enrolar é fácil de limpar e pode ser
								combinada com uma estrutura de aço inoxidável (opcional).
							</p>
							<br>

							<p>
								<br> <Strong>• Evita contaminação.</strong><br> ciclo rápido de
								funcionamento e com boa vedação, protegem da poeira, sujeira e
								parasitas.
							</p>
							<br>
						</div>
						<div class="col-md-4 col-xs-4">

							<p>
								<br> <Strong>• Economize energia.</strong><br> Temperaturas
								estáveis graças à alta velocidade de abertura e fechamento e
								vedação superior.
							</p>
							<br>

							<p>
								<br> <Strong>• Aumenta a eficiência da produção</strong><br>
								Otimização do fluxo de produção e aceleração no trafego.
							</p>
							<br>
						</div>
						<div class="col-md-4 col-xs-4">

							<p>
								<br> <Strong>• Redução de Custo</strong><br> Os desgastes e
								manutenção são limitados.
							</p>
							<br>
							<p>
								<br> <Strong>• Segurança para pessoas e equipamentos</strong><br>
								Possui tecido flexível sem elementos rígidos evitando lesões graves ou
								danos.
							</p>
							<br>

						</div>

					</div>
					
					
					
				</div>

			</div>
		</div>
	</div>
	<br>
	<br>
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('554591058441', '554591047291'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>
</html>