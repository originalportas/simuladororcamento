<!DOCTYPE html>
<head><!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Nosaa Empresa - Original Portas</title>
<base>
<meta name="description"
	content="Na Original Portas voc� encontra tudo para Portas de A�o, acess�rios, automatizadores com nobreaks, botoeiras, portas de a�o transvision, entre outros">
<meta name="keywords"
	content="Original Portas, portas, portas de a�o, portas a�o autom�ticas">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="S�o Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="1 days">
<link rel="canonical" href="nossa_empresa.php">
<meta name="author" content="Original Portas">
<link rel="shortcut icon" href="imagens/favicon.png">

<meta property="og:region" content="Brasil">
<meta property="og:title" content="Nossa Empresa - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url" content="nossa_empresa.php">
<meta property="og:description"
	content="Na Original Portas voc� encontra tudo para Portas de A�o, acess�rios, automatizadores com nobreaks, botoeiras, portas de a�o transvision, entre outros">
<meta property="og:site_name" content="Original Portas">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="/site3.2/bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="/site3.2/bootstrap/css/bootstrap.min.css" rel="stylesheet" />


<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type='text/css' href="css/style.css">


</head>
<body>
	<header>


		<div class="wrapper">
			<nav id="menu">

				<ul>
					<li id="img_nav"><img alt=""
						src="/site3.2/imagens/logotipo_original_medium.png" width="300px"
						height="80px"></li>
					<li><a href="index.php" title="P�gina inicial">Home</a></li>
					<li><a href="empresa.php" title="Sobre a Empresa Original Portas">Quem
							Somos</a></li>
					<li class="dropdown"><a href="produtos.php"
						title="Produtos Original Portas">Produtos<!--[if gte IE 7]><!--></a>
						<!--<![endif]-->
						<ul class="sub-menu2">
							<li><a href="acessorios.php" title="Acess�rios">Acess&oacute;rios</a></li>
							<li><a href="automatizadores.php" title="Automatizadores">Automatizadores</a></li>
							<li><a href="pintura-eletrostatica.php"
								title="Pintura Eletrost�tica">Pintura Eletrost&aacute;tica</a></li>
							<li><a href="porta-comercial.php" title="Porta Comercial">Porta
									Comercial</a></li>
							<li><a href="porta-industrial.php" title="Porta Industrial">Porta
									Industrial</a></li>
							<li><a href="porta-meia-cana.php" title="Porta Meia Cana">Porta
									Meia Cana</a></li>
							<li><a href="porta-transvision.php" title="Porta Transvision">Porta
									Transvision</a></li>
						</ul> <!--[if lte IE 6]></td></tr></table></a><![endif]--></li>
					<li><a href="https://goo.gl/photos/YTenbCXuHDhy9i97A"
						target="_blank" title="Galeria Original Portas">Galeria</a></li>
					<li><a href="video.php" title="Videos de portas de a�o">Videos</a></li>
					<li><a href="contato.php" title="Fale com a Original Portas">Fale
							Conosco</a></li>
				</ul>
			</nav>
		</div>
	</header>

	
	
	
	
	