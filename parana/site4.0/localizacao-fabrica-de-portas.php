<?php require 'main.php';
require 'footer.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Localização - Original Portas</title>
<meta name="description" content="Encontre a original portas fabrica de portas de aço , a maior fabica de portas do pais, seja original , venha conhecer a fabrica de laminas transvison para portas de aço">
<meta name="keywords" content="localização fabrica de portas, fabrica de portas em São Paulo, portas de enrolar Guarulhos ,onde comprar portas de loja?, portas de enrolar, portinhola , alçapão,distribuidora de automatizadores, controle portão. ">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="localizacao-fabrica-de-portas.php">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" href="/site4.0/imagens/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title" content="Localização Fabrica de portas de aço - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url" content="localizacao-fabrica-de-portas.php">
<meta property="og:description" content="Encontre a original portas fabrica de portas de aço , a maior fabica de portas do pais, seja original , venha conhecer a fabrica de laminas transvison para portas de aço">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="/site4.0/bootstrap/css/bootstrap.css"type="text/css" />
<link href="/site4.0/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/site4.0/bootstrap/css/beta.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="/site4.0/css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 
//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script> 
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="/site4.0/js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="/site4.0/js/jquery.slicknav.js"></script>
<script defer src="/site4.0/js/geral.js"></script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
<div class="local-text">
  <div class="row">
     <div class="col-md-12">
                <p><i class="fa fa-map-o"></i><p>
                <h1>Localização - Fábrica de Portas de Enrolar</h1>
                <p>Rua Luiz Delgado, 42 - Jardim Modelo - São Paulo - CEP: 02238-220</p>
                <p>(prox. Assai Atacadista  - Fernão Dias)</p>
       </div>
</div>
 <div class="col-md-12" style="margin-top:10px; padding:0; margin-bottom: 10px;">
 <br/>
     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5175.788106717855!2d-46.572410023675495!3d-23.46642026665626!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cef5be621a01ad%3A0x3ac8b99d825bd7b5!2sR.+Lu%C3%ADs+Delgado+-+Jardim+Modelo%2C+S%C3%A3o+Paulo+-+SP%2C+02238-220!5e0!3m2!1spt-BR!2sbr!4v1513178669321" width="100%" height="500px;" frameborder="0" style="border:0" allowfullscreen></iframe>
     </div>
  </div>	
</div>
<?php echo $footer;?>
</body>
</html>