<?php

require 'main.php';
require 'footer.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Rastrear Pedido Porta - Original Portas</title>
<base>
<meta name="description"
	content="A pintura eletrostática é uma tinta em pó com processo de acabamento a seco que se tornou extremamente popular desde a sua introdução. Representando mais de 15% do mercado total de acabamento industrial, o pó é usado em uma ampla gama de produtos. Mais e mais empresas buscam está técnica de tinta em pó para um acabamento durável e de alta qualidade, permitindo a produção máxima, eficiência aprimorada e conformidade ambiental simplificada. Usados ​​como acabamentos funcionais (protetores) e decorativos, revestimentos em pó estão disponíveis em uma gama de cores e texturas, os avanços tecnológicos resultaram em excelentes propriedades de desempenho.">
<meta name="keywords"
	content="Pintura Eletrostática, pinturas, eletrostáticas, pinturas eletrostáticas, tinta em pó, tinta, pó">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="pintura-eletrostatica.php">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" href="imagens/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Pintura elestrostática - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/pintura-eletrostatica-01.png">
<meta property="og:url" content="pintura-eletrostatica.php">
<meta property="og:description"
	content="A pintura eletrostática é uma tinta em pó com processo de acabamento a seco que se tornou extremamente popular desde a sua introdução. Representando mais de 15% do mercado total de acabamento industrial, o pó é usado em uma ampla gama de produtos. Mais e mais empresas buscam está técnica de tinta em pó para um acabamento durável e de alta qualidade, permitindo a produção máxima, eficiência aprimorada e conformidade ambiental simplificada. Usados ​​como acabamentos funcionais (protetores) e decorativos, revestimentos em pó estão disponíveis em uma gama de cores e texturas, os avanços tecnológicos resultaram em excelentes propriedades de desempenho.">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=iso-Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">

<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>

	<div class="container">
		<div class="busc_ped">
			<div class="row">
			<div class="col-md-12 col-xs-12">
			<h1><i class="fa fa-truck" style="font-size:36px;"></i>
			Rastreamento do Pedido</h1>
			<form class="form-inline"  method="POST" action="status_produto.php">
  <div class="form-group mb-2">
       <h4><label for="consulta" class="sr-only">Numero do Pedido</label>
    <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Numero do Pedido"></h4>
  </div>
  <div class="form-group mx-sm-3 mb-2">
    <label for="inputPassword2" class="sr-only">Nº Pedido</label>
    <input type="number" class="form-control" id="consulta" name="cons_status" maxlength="8" placeholder="Ex: 4556">
  </div>
  <button type="submit" class="btn btn-primary mb-2" style="padding:8px;font-size: 12px;">Rastrear</button>
</form>
  <p>As informações ficam disponíveis por até 30 dias corridos após a confirmação da coleta ou entrega.</p>

	
			</div>
		</div>
	</div>
	</div>
	
	<br>
		<br>
<?php echo $footer;?>
</body>
</html>