<?php require 'main.php';
require 'footer.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Contato - Original Portas</title>
<base>
<meta name="description" content="Envie sua mensagem pelo formulário e logo entraremos em contato.">
        <meta name="keywords" content="Contato Original Portas, Contato Portas, Preço Porta de Enrolar, Orçamento portas">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximun-scale=1.0">
        <meta name="geo.position" content="-23.4664636;-46.5701426">        
        <meta name="geo.placename" content="São Paulo-SP"> 
        <meta name="geo.region" content="SP-BR">
        <meta name="ICBM" content="-23.4664636;-46.5701426">
        <meta name="robots" content="index,follow">
        <meta name="rating" content="General">
        <meta name="revisit-after" content="7 days">
        <link rel="canonical" href="contato-original-portas.php">
	    <meta name="author" content="Original Portas">    
        <link rel="shortcut icon" href="imagens/favicon.png">
        <meta property="og:region" content="Brasil">
        <meta property="og:title" content="Contato - Original Portas">
        <meta property="og:type" content="article">
        <meta property="og:image" content="imagens/logotipo.png">
        <meta property="og:url" content="contato-original-portas.php">
        <meta property="og:description" content="Envie sua mensagem pelo formulário e logo entraremos em contato.">
        <meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="/site4.0/bootstrap/css/bootstrap.css"type="text/css" />
<link href="/site4.0/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/site4.0/bootstrap/css/beta.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="/site4.0/css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script> 
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="/site4.0/js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="/site4.0/js/jquery.slicknav.js"></script>
<script defer src="../apis.google.com/js/plusone.js"></script>
<script defer src="/site4.0/js/geral.js"></script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container">
  <div class="class_contat">
    <div class="row">
     <div class="col-md-12">
          <h1>Central de Atendimento</h1>
       	   <hr style="width: auto; height: 2px; background-color: #ccc;">
          <br>
     </div>
     <br>
     <div class="col-md-6">
      <h3><strong>Vendas</strong></h3>      
           <p>Telefone: (11) 4386-1001</p>
            <p>Whatsapp: <?php  $array = array( '(11) 97775-5648', '(11) 95906-5418','(11) 94045-9379','(11)95353-6427');  shuffle( $array );
                                                                echo current( $array );?></p>
           <p>Email: contato@originalportas.com.br</p><br />
      </div>
      <div class="col-md-6">
      <h3><strong>Financeiro</strong></h3>      
          <p>Telefone: (11) 4386-1001/Ramal:223</p>
          <p>Email: financeiro2@originalportas.com.br</p><br />
      </div>
       <div class="col-md-6">
          <h3><strong>SAC</strong></h3>      
          <p>Telefone: (11) 4386-2002</p>
          <p>Email: sac@originalportas.com.br</p><br />
      </div>
      <div class="col-md-6">
          <h3><strong>Projetos</strong></h3>      
          <p>Telefone: (11) 4386-1001/Ramal:210</p>
          <p>Email: projetos@originalportas.com.br</p><br />
      </div>
    </div>
   </div>
</div>
<?php echo $footer;?>
</body>
</html>         