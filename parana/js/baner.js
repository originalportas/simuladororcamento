 jQuery(document).ready(function ($) {

            var jssor_1_SlideshowTransitions = [
              {$Duration:800,$Opacity:2},
              {$Duration:800,$Opacity:2},
              {$Duration:800,$Opacity:2},
              {$Duration:700,$Opacity:2,$Brother:{$Duration:700,$Opacity:2}}
            ];

            var jssor_1_SlideoTransitions = [
              [{b:2400,d:700,x:544}],
              [{b:2600,d:620,x:-686,y:-20}],
              [{b:3900,d:300,x:-380}],
              [{b:4100,d:300,x:-383}],
              [{b:4200,d:300,x:-383}],
              [{b:1600,d:2600,y:-727}],
              [{b:4220,d:1080,y:473,e:{y:30}}],
              [{b:2200,d:700,y:43}],
              [{b:-1,d:1,o:-1},{b:400,d:600,x:434,y:-2,o:1},{b:2200,d:700,y:28}],
              [{b:-1,d:1,o:-1},{b:600,d:600,y:-340,o:1},{b:2200,d:700,y:39}],
              [{b:-1,d:1,sY:-0.2},{b:2200,d:700,y:-213,sY:0.2}],
              [{b:1400,d:700,y:568},{b:2200,d:700,y:-213}],
              [{b:2200,d:700,y:35}],
              [{b:300,d:100,o:-1},{b:480,d:100,o:1},{b:660,d:100,o:-1},{b:840,d:100,o:1},{b:1020,d:100,o:-1},{b:1200,d:100,o:1}],
              [{b:-1,d:1,o:-1},{b:1000,d:1000,o:1,rY:360}],
              [{b:600,d:800,x:573,kX:30},{b:6260,d:140,rX:360},{b:8000,d:800,x:1501,o:-1}],
              [{b:2200,d:800,x:622,kX:30},{b:6560,d:140,x:2,rX:360},{b:8400,d:800,x:1490,o:-1}],
              [{b:3800,d:800,x:665,y:-8,kX:30},{b:6860,d:140,x:2,rX:360},{b:8800,d:700,x:1490,o:-1}],
              [{b:1400,d:800,x:574,y:4},{b:6000,d:200,o:-1},{b:8000,d:800,x:1460}],
              [{b:3000,d:800,x:550,y:-1},{b:6300,d:200,o:-1},{b:8400,d:800,x:1390}],
              [{b:4600,d:800,x:584,y:1},{b:6600,d:200,o:-1},{b:8800,d:700,x:1350}],
              [{b:-1,d:1,o:-1},{b:6260,d:140,o:1,rX:360},{b:8000,d:800,x:1259,o:-1}],
              [{b:-1,d:1,o:-1},{b:6560,d:140,x:-21,y:-3,o:1,rX:360},{b:8400,d:800,x:1213}],
              [{b:-1,d:1,o:-1},{b:6860,d:140,o:1,rX:360},{b:8800,d:800,x:1075,o:-1}],
              [{b:9100,d:1800,y:736}]
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $PauseOnHover: 0,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 1440;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        });