<?php
require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
<title>Portas de A&ccedil;o Autom&aacute;ticas || Original Portas</title>
<meta name="description"
	content="Fabrica Especializada em Portas de A&ccedil;o, Temos os Produtos de melhor qualidade do Mercado com Pre&ccedil;os Imbatíveis. Confira!">
<meta name="keywords"
	content="Portas de A&ccedil;o, portas autom&aacute;ticas, porta, a&ccedil;o, aco">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="S&atilde;o Paulo-SP">
<meta name="geo.region" content="SP-BR">	
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="porta-aco">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Portas de A&ccedil;o - Atendemos em Todo Brasil, Bolivia e Paraguay, grande S&atilde;o Paulo-SP">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/porta-aco.png">
<meta property="og:url" content="porta-aco">
<meta property="og:description"
	content="Portas de a&ccedil;o - Aqui você encontra tudo para Portas e Portões Autom&aacute;ticos.">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 
function reEnable(){ 
return true 
} 
//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2">
				<img src="imagens/porta-aco-automatica.png" width="100%" height="auto">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="class_aut2">
			<div class="row">
				<div class="col-md-12">
					<h1 style="margin-top: -20px;">Porta de A&ccedil;o Autom&aacute;tica</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					
					
					  <div class="col-sm-12 col-md-12 col-xl-7" style="margin-top: 20px;">
				  <video width="100%" muted loop autoplay>
                    <source src="video/porta-feicon.mp4" type="video/mp4">
                  </video>
                  
			</div><br><br>
					<h2>Vantagens da porta de a&ccedil;o</h2>
					<p>A porta de a&ccedil;o é cada vez mais
						procurada por comerciantes e por pessoas que querem deixar a sua
						casa ou estabelecimento comercial protegido. Além de ser elaborada
						com material extremamente resistente, ela é super f&aacute;cil de
						manusear.
					</p>
					<p>As Portas de a&ccedil;o Antigamente, as pessoas buscavam portões manuais elaborados em
						diversos metais e isso ainda acontece, porém, a porta de
							a&ccedil;o autom&aacute;tica tem ganhado espa&ccedil;o e muitas vezes até
						substituído antigos portões. O a&ccedil;o é extremamente resistente e em
						caso de uma tentativa de invas&atilde;o, ele faz papel de uma barreira
						protetora, difícil de ser ultrapassada.
					</p>
					<p>A porta de a&ccedil;o autom&aacute;tica, além de tudo, pode ser
						controlada apenas com um bot&atilde;o, isso faz com que a pessoa n&atilde;o
						precise se deslocar até ela para abrir e fechar, poupando esfor&ccedil;o
						físico.
					</p>
					<p>Portas de a&ccedil;o podem ser desenvolvidas com lâminas bastante diversificadas. É
						possível, por exemplo, uma porta de a&ccedil;o autom&aacute;tica
						totalmente fechada ou com v&atilde;os para que se tenha vis&atilde;o interna.
						Essa é preferida por comerciantes de shoppings e chama-se
						Transvision: consegue garantir a prote&ccedil;&atilde;o, mas também permite que
						a vitrine seja observada quando a loja se encontra fechada.
					</p>
					<p>O custo de instala&ccedil;&atilde;o e manuten&ccedil;&atilde;o das portas de a&ccedil;o também s&atilde;o em conta. Mas é
						preciso manter cuidados com a conserva&ccedil;&atilde;o da porta. Dessa maneira,
						se torna um investimento bastante vantajoso.</p>
					<h2>A porta de a&ccedil;o autom&aacute;tica tem desvantagens?</h2>
					<p>Se pararmos para observar bem a porta de a&ccedil;o autom&aacute;tica
						n&atilde;o apresenta desvantagens, diferentemente da porta de a&ccedil;o manual,
						que é bastante pesada e exige um pouco mais de esfor&ccedil;o para que
						possa ser movimentada.
					</p>
					<h2>Qualquer empresa pode comercializar uma porta de a&ccedil;o
						autom&aacute;tica?</h2>
					<p>A venda das portas de a&ccedil;o  deve ser realizada por
						empresas especializadas na presta&ccedil;&atilde;o desse servi&ccedil;o, como a
						Original Portas, que trabalha com portas desenvolvidas nas medidas
						exatas do ambiente. A empresa ainda oferece servi&ccedil;o de instala&ccedil;&atilde;o
						com técnicos capacitados.</p>
					<p>Na dúvida antes de contratar o servi&ccedil;o, vale a pena conferir o
						histórico da empresa de porta de a&ccedil;o autom&aacute;tica e
						também a avalia&ccedil;&atilde;o de outros clientes. Referencias assim permitem
						que você entenda se a empresa presta um bom servi&ccedil;o e se outros
						clientes j&aacute; tiveram problemas com ela ou n&atilde;o. É a melhor forma de
						garantir que você ser&aacute; bem atendido em suas necessidades!
					</p>

				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('554591058441', '554591047291'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>
</html>