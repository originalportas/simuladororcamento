<?php
require 'main.php';
require 'footer.php';
?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
        <title>Porta Seccionada Industrial- Original Portas</title>
<base>
<meta name="description"
	content="A porta seccionada industrial preço irá variar conforme o modelo dela, porém, todas contam com um sistema de trilhos laterais, que não atrapalha o seu design">
<meta name="keywords"
	content="Porta Seccionada Preço, porta, seccionada, preço, portão garagem americano,Europa, europeu , portão, americano ">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="porta-seccionada-industrial">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Porta Seccionada industrial - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/porta_seccionada/porta-seccionada.png">
<meta property="og:url" content="porta-seccionada-industrial">
<meta property="og:description"
	content="A porta seccionada industrial preço irá variar conforme o modelo dela, porém, todas contam com um sistema de trilhos laterais, que não atrapalha o seu design">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2">
				<img src="imagens/porta_seccionada/porta-seccionada-industrial-11.png" width="100%" height="auto" >
			</div>
		</div>
	</div>
	<div class="container">
		<div class="class_aut2">
			<div class="row">
				<div class="col-md-12">
				
				
        <h1 style="margin-top:-20px;">Portas Seccionadas Industriais</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
			
                     <h2></h2>

                <p>A <strong>porta seccionada industrial</strong> é elaborada em estrutura que lhe garante uma maior resistência e durabilidade. Isso também faz com que ela não consiga ser facilmente violada, sendo resistente uma barreira para possíveis invasores.</p>
                <p>Esse tipo de porta é composto com paineis de 40mm , formados por duas chapas galvanizadas com o miolo preenchido de poliuretano expandido, e podem ser pintados com pintura eletrostática.</p>
                <p>O valor da <strong>porta seccionada industrial</strong> irá variar conforme o modelo dela. Porém, todas contam com um sistema de trilhos laterais, que fica super discreto e não atrapalha o seu design. Além disso, garante um espaço mínimo nos laterais do vão, o que faz com que seu fechamento seja perfeito, impedindo que o ambiente seja observado.</p>

                <p>Ela ainda conta com um isolamento acústico, que faz com que o local fique mais silencioso, mesmo quando do lado de fora existe um grande barulho. Isolamento térmico também é uma de suas vantagens. Assim, o local conta com uma temperatura sempre agradável internamente.</p>

                <p>A porta também é automatizada, o que facilita a abertura e o fechamento dela.</p>
<br>
<br>
                <div class="col-md-12">
				<img src="imagens/porta_seccionada/seccional-interior.jpg" width="100%" height="auto" ><br>
			<br>
			</div>
			
			
                <p>Adquira <strong>porta seccionada industrial</strong> na Original Portas . <br>Consulte nossos preços.</p>


				</div>

			</div>
		</div>
	</div>
	<br>
	<br>
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('554591058441', '554591047291'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>
</html>