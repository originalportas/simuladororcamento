<?php
require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async
	src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script>
<script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Or&ccedil;amento - Original Portas</title>
<base>
<meta name="description"
	content="Envie sua mensagem atrav&eacute;s do formulario de or&ccedil;amento e entraremos em contato com voc&ecirc;, saiba o pre&ccedil;o de portas de enrolar, portas de a&ccedil;o &eacute; caro? ,portas de enrolar &eacute; caro?">
<meta name="keywords"
	content="Original Portas, or&ccedil;amento de portas e enrolar, pre&ccedil;o portas de a&ccedil;o, portas a&ccedil;o autom&aacute;ticas, portas de enrolar, portinhola, al&ccedil;ap&atilde;o, automatizadores, controle port&atilde;o. tudo para portas de a&ccedil;o ">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="S&atilde;o Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="orcamento-porta-enrolar-automatica">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Or&ccedil;amento de portas de a&ccedil;o - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url"
	content="orcamento-porta-enrolar-automatica">
<meta property="og:description"
	content="Original Portas, or&ccedil;amento de portas e enrolar, pre&ccedil;o portas de a&ccedil;o, portas a&ccedil;o autom&aacute;ticas, portas de enrolar, portinhola, al&ccedil;ap&atilde;o, automatizadores, controle port&atilde;o. tudo para portas de a&ccedil;o">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="//bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- -----------------JS----------------- -->
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script src="js/validation.js" type="text/javascript"></script>
<!-- -------------Valida&ccedil;&atilde;o input/telefone ----------------------------------->
<script type="text/javascript" src="js/mask_number.js"></script>
<!-----------------font Google------------------>
<link href="https://fonts.googleapis.com/css?family=Play"
	rel="stylesheet">

<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<style>
.container, .row, label, h1, h2, h3, h4, h5, p {
	font-family: 'Play', sans-serif;
}
</style>

<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>
<!--  busca cep -->
<script defer src="js/busca_cep.js"></script>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container">
		<div class="form">
			<div class="row" style="background-color: #fff;">
				<div class="col-md-12">
					<h1>Or&ccedil;amento</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					<br>
				</div>
				<form enctype="multipart/form-data" id="formContato" method="post"
					name="contato-envia" action="contato-envia" class="form2">
					<!-- input type="hidden" name="ACAO" value="ENVIAR" / -->

					<div class="col-md-6">
						<label for="nome">Nome: <span>*</span></label><br> <input
							onkeypress="return Onlychars(event)"  onKeyUp="UcWords(this)"
							type="text" name="nome" class="nome" value="" id="nome" size="32" />
					</div>

					<div class="col-md-6">
						<label for="telefone"> DDD/Telefone: <span>*</span></label><br> <input
							type="text" name="ddd" class="ddd" value="" id="ddd" size="2"
							maxlength="2"  onkeypress="return Onlynumbers(event)" /> 
							
							<input
							type="text" name="telefone" class="telefone" value=""
							id="telefone" size="24" maxlength="9"
							onkeypress="return Onlynumbers(event)" />
					</div>

					<div class="col-md-6">
						<label for="email">E-mail: <span>*</span>
						</label><br> <input onKeyUp="minusculas(this)" type="text"
							name="email" class="email" value="" id="email" size="32" />
					</div>

					<div class="col-md-6">
						<label> Como nos conheceu?: <span>*</span>
						</label><br> <select name="como_conheceu">
							<option value="">Selecione</option>
							<option value="Busca do Google">Busca do Google</option>
							<option value="Outros Buscadores">Outros Buscadores</option>
							<option value="Links patrocinados">Links Patrocinados</option>
							<option value="Outros Anúncios">Outros Anúncios</option>
							<option value="Facebook">Facebook</option>
							<option value="Google+">Google+</option>
							<option value="Indica&ccedil;&atilde;o">Indica&ccedil;&atilde;o</option>
							<option value="Outros">Outros</option>
						</select>
					</div>
					<br>
					<div class="col-md-12">
						<label>Cep:<a style="font-size: 15px"></a><br>
							<input name="cep" type="text" id="cep" value="" size="10"
							maxlength="9" onkeypress="return Onlynumbers(event)" /></label><br>
					</div>
					<div class="col-md-6">

						<label>Rua: <br> <input name="rua" type="text" id="rua" size="32"
							maxlength="60" />
						</label><br> <label>Bairro:<br> <input name="bairro" type="text"
							id="bairro" size="32" maxlength="50" /></label><br>
					</div>
					<div class="col-md-6">
						<label>Cidade:<br> <input name="cidade" type="text" id="cidade"
							size="32" maxlength="50" />
						</label><br> <label> UF:<br> <input name="uf" type="text" id="uf"
							size="3" maxlength="2" />
						</label><br> <br>

					</div>
					<div class="col-md-7">
						<label> Qual modelo de porta voc&ecirc; deseja? </label><br> <select
							name="porta_modelo">
							<option value="Nenhuma">Nenhuma</option>
							<option value="Porta de Enrolar Autom&aacute;tica">Porta de A&ccedil;o
								Autom&aacute;tica</option>
							<option value="Porta de Enrolar Manual">Porta de A&ccedil;o Manual</option>
							<option value="Porta R&aacute;pida de Enrolar">Porta R&aacute;pida de Enrolar</option>
							<option value="Porta R&aacute;pida Dobrav&eacute;l">Porta R&aacute;pida Dobrav&eacute;l</option>
							<option value="Porta Seccionada">Porta Seccionada</option>
							<option value="Porta de Vidro &aacute;utomatica">Porta de Vidro
								Autom&aacute;tica</option>
						</select>
						<label> Qtd.<input name="qtd_porta" type="text"
							id="qtd_porta" value="" size="3" maxlength="3"
							onkeypress="return Onlynumbers(event)" /></label>
						<br> <br> <label> Tipo de Soleira? </label><br> <select
							name="tp_soleira">
							<option value="Nenhuma">Nehuma</option>
							<option value="Soleira em L">Soleira em 'L'</option>
							<option value="Soleira em t">Soleira em 'T'</option>
							<option value="Soleira tubular">Soleira em 'Tubo'</option>

						</select> <br> <br> <label> Espessura da L&acirc;mina? </label><br> <select
							name="esp_lamina">
							<option value="Nenhma">Nenhuma</option>
							<option value="Chapa-20">Chapa - 20</option>
							<option value="Chapa-22">Chapa - 22</option>
							<option value="Chapa-24">Chapa - 24</option>

						</select> <br> <br> <label> Tipo da L&acirc;mina? </label><br> <select
							name="tp_lamina">
							<option value="Nehuma">Nenhuma</option>
							<option value="Fechada">Meia-cana (fechada)</option>
							<option value="Transvison">Transvision (perfurada)</option>
							<option value="Mista">Mista</option>

						</select> <br> <br> <label> Pintura eletrostatica? (cores aprox.)
						</label><br> <select name="pt_eletro">
							<option value="Sem Pintura">Sem Pintura</option>
							<option value="Branco Brilho" id="square_branco_b">Branco Brilho</option>
							<option value="Branco Fosco" id="square_branco_f">Branco Fosco</option>
							<option value="Prata Semi-Brilho" id="square_prata_sm">Prata
								Semi-Brilho</option>
							<option value="Cinza Claro" id="square_cinza_c">Cinza Claro</option>
							<option value="Cinza Escuro" id="square_cinza_e">Cinza Escuro</option>
							<option value="Cinza Chumbo" id="square_cinza_ch">Cinza Chumbo</option>
							<option value="Preto Brilho" id="square_preto_b">Preto Brilho</option>
							<option value="Preto Fosco" id="square_preto_f">Preto Fosco</option>
							<option value="Bege Liso" id="square_bege_l">Bege Liso</option>
							<option value="Bronze" id="square_bronze">Bronze</option>
							<option value="Verde Escuro" id="square_verde_e">Verde Escuro</option>
							<option value="Verde M&eacute;dio" id="square_verde_m">Verde M&eacute;dio</option>
							<option value="Azul Escuro" id="square_azul_e">Azul Escuro</option>
							<option value="Azul Claro" id="square_azul_c">Azul Claro</option>
							<option value="Amarelo" id="square_amarelo">Amarelo</option>
							<option value="Marrom" id="square_marrom">Marrom</option>
							<option value="Vermelho" id="square_vermelho">Vermelho</option>
							<option value="Vermelho Escuro" id="square_vermelho_e">Vermelho
								Escuro</option>
						</select> <br> <br> <label> Posi&ccedil;&atilde;o da Guia ? </label><br> <select
							name="ps_guia">
							<option value="Nehuma">Nenhuma</option>
							<option value="Escondida">Escondidas</option>
							<option value="Aparentes">Aparentes</option>
							<option value="Mistas">Mistas</option>

						</select> <br> <br> <label>As Medidas do V&atilde;o Livre devem ser
							preenchidas em Milimetros<a style="font-size: 15px"><br>( Ex:
								1.00mt = 1.000mm )</a>
						</label><br> <br> <label> Largura <input name="largura_vao"
							type="number" id="largura_vao" value="" size="10" maxlength="9"
							onkeypress="return Onlynumbers(event)" /></label> &nbsp; <label>
						</label> <label> Altura <input name="altura_vao" type="number"
							id="altura_vao" value="" size="10" maxlength="9"
							onkeypress="return Onlynumbers(event)" /></label><br> <br> <br>
					</div>

					<div class="col-md-5">
						<img id="img_vao" alt="vao_livre" src="imagens/vao-livre.png"
							style="width: 100%; height: auto">
					</div>




					<div class="col-md-6">
						<hr style="width: 100%; height: 2px; background-color: #ccc;">

						<h2>
							<label>Itens Opcionais</label>
						</h2>
						<hr style="width: 100%; height: 2px; background-color: #ccc;">

						<div class="checkbox">
							<label><input type="checkbox" name="alcapao_chk" value="SIM">Al&ccedil;ap&atilde;o</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="automatizador_no_chk"
								value="SIM">Automatizador c/ Nobreak</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="automatizador_mn_chk"
								value="SIM">Automatizador p/ Manual</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="bandeira_chk" value="SIM">Bandeira</label>
						</div>
						<div class="checkbox">

							<label><input type="checkbox" name="cobre_rolo_chk" value="SIM">Cobre
								Rolo</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="guia_sob_chk" value="SIM">Guia
								Sobreposta</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="guia_tel_chk" value="SIM">Guia
								Telescopica</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="portinhola_chk" value="SIM">Portinhola</label>
						</div>

						<div class="checkbox">
							<label><input type="checkbox" name="tubo_afast_chk" value="SIM">Tubo
								de Afastamento</label>
						</div>
					</div>
					<div class="col-md-6">
						<hr style="width: 100%; height: 2px; background-color: #ccc;">
						<h2>
							<label>Acess&oacute;rios</label>
						</h2>
						<hr style="width: 100%; height: 2px; background-color: #ccc;">
						<div class="checkbox">
							<label><input type="checkbox" name="anti_qd_chk" value="SIM">Anti-Quedas</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="borracha_v_chk" value="SIM">Borracha
								Veda&ccedil;&atilde;o Soleira</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="controle_r_chk" value="SIM">Controle
								Remoto</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="central_chk" value="SIM">Central
								c/ Controle </label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="fech_l_chk" value="SIM">Fechaduras
								Laterais c/ Chaves</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="fech_c_chk" value="SIM">Fechaduras
								centrais c/ Chaves</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="fech_p_chk" value="SIM">Fechaduras
								para piso c/ Chaves</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="fita_auto_lub_chk"
								value="SIM">Fita Auto Lubrificante</label>
						</div>

						<div class="checkbox">
							<label><input type="checkbox" name="sensor_infra_chk" value="SIM">Sensor
								Infravermelho</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="trava_l_chk" value="SIM">Trava
								l&acirc;minas</label>
						</div>
					</div>

					<div class="col-md-12" id="img_vao">
						<br> <label>Para mais detalhes escreva aqui</label><br>
						<textarea name="mensagem" cols="35" rows="10"></textarea>
						<div class="col-md-12"><p>Os campos com (*) s&atilde;o obrigat&oacute;rios</p>
	</div>
						<br> <div class="bt-submit">
							<button type="submit" value="Enviar" class="btn btn-primary">Enviar</button>
						</div><br>
					</div>

				</form>

			</div>
		</div>
	</div>

<?php echo $footer;?>
</body>
</html>

