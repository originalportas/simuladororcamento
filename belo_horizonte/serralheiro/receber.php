<?php
$checkPintura = $_POST['checkPintura'];
$CheckPortinhola = $_POST['CheckPortinhola'];
$CheckalcaEnrolar = $_POST['CheckalcaEnrolar'];
$CheckBorraVedacao = $_POST['CheckBorraVedacao'];
$CheckPvcGuias = $_POST['CheckPvcGuias'];
$CheckMkit = $_POST['CheckMkit'];
$CheckCentral = $_POST['CheckCentral'];
$nome = $_POST['nome'];
$endereco = $_POST['endereco'];
$cidade = $_POST['cidade'];
$telefone = $_POST['telefone'];
$porta = $_POST['porta'];
$resChapa = $_POST['resChapa'];
$totalChapa = $_POST['totalChapa'];
$guia = $_POST['guia'];
$tamGuia = $_POST['tamGuia'];
$resulGuia = $_POST['resulGuia'];
$nomeEixo = $_POST['nomeEixo'];
$lgr = $_POST['lgr'];
$resulEixo = $_POST['resulEixo'];
$valorSoleira = $_POST['valorSoleira'];
$motor = $_POST['motor'];
$motorValor = $_POST['motorValor'];
$pvcGuias = $_POST['pvcGuias'];
$resultPinEletros = $_POST['resultPinEletros'];
$valPortinhola = $_POST['valPortinhola'];
$valAlcapao = $_POST['valAlcapao'];
$centDoisContr = $_POST['centDoisContr'];
$valorMontKit = $_POST['valorMontKit'];
$valBorraVedacao = $_POST['valBorraVedacao'];
$totalPvcGuias = $_POST['totalPvcGuias'];
$totalOrcam = $_POST['totalOrcam'];

$linha1 = "ORÇAMENTO SERRALHEIRO";
$linha2 = "NOME: " . $nome;
$linha3 = "ENDEREÇO: " . $endereco;
$linha12 = "CIDADE: " . $cidade;
$linha4 = "TELEFONE: " . $telefone;
$linha5 = $porta . " - M² " . $resChapa . " - R$ " . number_format($totalChapa,2,',','.');
$linha6 = $guia . " - ML. " . $tamGuia . " - R$ " . number_format($resulGuia,2,',','.');
$linha7 = $nomeEixo . " - ML. " . str_replace('.', ',', $lgr) . " - R$ " . number_format($resulEixo,2,',','.');
$linha8 = "Soleira em T - ML. " . str_replace('.', ',', $lgr) . " - R$ " . number_format($valorSoleira,2,',','.');
$linha9 = $motor . " - PC. 1 - R$ " . number_format($motorValor,2,',','.');
$linha13 = "ITENS ADICIONAIS";
if(isset($checkPintura)){
    $linha14 = "Pintura Eletrostatica - M² " . $resChapa. " - R$ ".number_format($resultPinEletros,2,',','.');
    $totalOrcam = $totalOrcam += $resultPinEletros;
}else{
    $linha14 = "Pintura Eletrostatica - ======= ";
}
if(isset($CheckPortinhola)){
    $linha15 = "Portinhola - PC. 1 - R$ ".number_format($valPortinhola,2,',','.');
    $totalOrcam = $totalOrcam += $valPortinhola;
}else{
    $linha15 = "Portinhola - ======= ";
}
if(isset($CheckalcaEnrolar)){
    $linha16 = "Alçapão de Enrolar - PC. 1 - R$ ".number_format($valAlcapao,2,',','.');
    $totalOrcam = $totalOrcam += $valAlcapao;
}else{
    $linha16 = "Alçapão de Enrolar - ======= ";
}
if(isset($CheckCentral)){
    $linha17 = "Central com 2 controles - PC. 1 - R$ ".number_format($centDoisContr,2,',','.');
    $totalOrcam = $totalOrcam += $centDoisContr;
}else{
    $linha17 = "Central com 2 controles - ======= ";
}
if(isset($CheckBorraVedacao)){
    $linha10 = "Borracha vedação - ML. " . str_replace('.', ',', $lgr) . " - R$ " . number_format($valBorraVedacao,2,',','.');
    $totalOrcam = $totalOrcam += $valBorraVedacao;
}else{
    $linha10 = "Borracha vedação - ======= ";
}
if(isset($CheckPvcGuias)){
    $linha11 = "PVC Guias - ML. " . $pvcGuias . " - R$ " . number_format($totalPvcGuias,2,',','.');
    $totalOrcam = $totalOrcam += $totalPvcGuias;
}else{
    $linha11 = "PVC Guias - ======= ";
}
if(isset($CheckMkit)){
    $linha18 = "Montagem de Kit - PC. 1 - R$ ".number_format($valorMontKit,2,',','.');
    $totalOrcam = $totalOrcam += $valorMontKit;
}else{
    $linha18 = "Montagem de Kit - ======= ";
}
$Ultimalinha = "TOTAL ORÇAMENTO";
$totalAdic = $totalOrcam;
$total = "== R$".number_format($totalAdic,2,',','.')." ==";
$espaco = " ";


    $mensagem =
    $linha1.
    "\r".
    "\r".
    $linha2.
    "\r".
    $linha3.
    "\r".
    $linha12.
    "\r".
    $linha4.
    "\r".
    "\r".
    $linha5.
    "\r".
    $linha6.
    "\r".
    $linha7.
    "\r".
    $linha8.
    "\r".
    $linha9.
    "\r".
    $espaco.
    "\r".
    "\r".
    $linha13.
    "\r".
    "\r".
    $linha14.
    "\n\r".
    $linha15.
    "\n\r".
    $linha16.
    "\n\r".
    $linha17.
    "\n\r".
    $linha10.
    "\n\r".
    $linha11.
    "\n\r".
    $linha18.
    "\r\n".
    "\r\n".
    $Ultimalinha.
    "\r\n".
    "\r\n".
    $total.
    "\r\n";
    //echo $mensagem.'<br/>';
    //echo gettype ($valorLinha14).'<br/>';
    //echo $valorLinha15.'<br/>';
    //echo $valorLinha16.'<br/>';
    //echo $valorLinha17.'<br/>';
    //echo $valorLinha18.'<br/>';
    //echo $total.'<br/>';
    //echo $mensagem;


	$destino = "cleyton@originalportas.com.br";
	$assunto = "Orçamento Serralheiro";
	if (PATH_SEPARATOR ==":") {
		$quebra = "\r\n";
	} else {
		$quebra = "\n";
	}
	$headers = "MIME-Version: 1.1".$quebra;
	$headers .= "Content-type: text/plain; charset=utf-8".$quebra;
	$headers .= "From: cleyton@originalportas.com.br".$quebra; //E-mail do remetente
	$headers .= "Return-Path: cleyton@originalportas.com.br".$quebra; //E-mail do remetente

	$enviarEmail = mail($destino, $assunto, $mensagem, $headers, "-r". "contato@originalportas.com.br");

if ($enviarEmail) {
	echo "<script>alert('Orçamento enviado com sucesso!');</script>";

	echo "<script language='javascript'>window.open('https://www.originalportas.com.br/', '_parent'); </script>";
} else {
	echo "<script>alert('Erro ao Enviar o Orçamento!');</script>";

	echo "<script language='javascript'>window.open('https://www.originalportas.com.br/', '_parent'); </script>";
}