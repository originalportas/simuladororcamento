<?php
require 'main.php';
require 'footer.php';
?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
        <title>Lâminas para Porta de Aço - Original Portas</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description"
	content="As lâminas para porta de aço na verdade são o que irá formar toda a estrutura da porta ou estrutura do portão utilizados na entrada dos estabelecimentos">
<meta name="keywords"
	content="Lâminas para Porta de Aço, lâminas, porta, aço, laminas, aco">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="laminas-portas-aco">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Lâminas para Porta de Aço - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/fabrica-lamina-porta-aco-automatica.png">
<meta property="og:url" content="laminas-portas-aco">
<meta property="og:description"
	content="As lâminas para porta de aço na verdade são o que irá formar toda a estrutura da porta ou estrutura do portão utilizados na entrada dos estabelecimentos">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2">
				<img src="imagens/lamina_photo-11.png"  width="100%" height="auto">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="class_aut2">
			<div class="row">
				<div class="col-md-12">
					
        <h1 style="margin-top:-20px;">Lâminas para Porta de Aço</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					
					
                <h2>Os diferentes tipos de porta de aço</h2>

                <p>Quando se fala em porta de aço é preciso pensar que existem diferentes tipos. Cada um é recomendado para um tipo de ambiente ou situação. Esse tipo de porta é feitos com <strong>lâminas para porta de aço</strong>, isso significa que é formado por pequenas estruturas que, unidas, vão resultar na peça final.</p>

                <p>O tipo de porta de aço de enrolar, por exemplo, normalmente é bastante utilizado em garagens e também na entrada de lojas e fachadas, para garantir a proteção e também bloquear a entrada. Nesse caso, as <strong>lâminas para porta de aço</strong> podem ser bem diferenciadas. A fechada é a mais comum, porém, muitos comerciantes também optam pelas <strong>lâminas para porta de aço</strong> raiada e vazada. Com isso, é possível verificar a vitrine do estabelecimento comercial e ao mesmo tempo garantir segurança.</p>

                <p>As <strong>lâminas para porta de aço</strong> Transvision também são bastante utilizadas nas vitrines porque elas possuem microperfurações que permitem observar do lado de dentro. Já as <strong>lâminas para porta de aço</strong> de modelo tijolinho impedem que o estabelecimento seja invadido por bandidos, mas ao mesmo tempo permitem uma grande visão.</p>

                <h2>O que são as lâminas para porta de aço?</h2>

                <p>As <strong>lâminas para porta de aço</strong> na verdade são o que irá formar toda a estrutura da porta ou portão utilizados na entrada dos estabelecimentos. Elas são espécies de tiras, que unidas formam as portas. Essa união das <strong>lâminas para porta de aço</strong> é feita diretamente na fábrica de portas de aço, de acordo com as dimensões solicitadas pelo cliente. Na Original Portas, por exemplo, é um trabalho feito sob encomenda, que atende as necessidades de cada segmento e cliente.</p>

                <p>A grande vantagem de trabalhar com as <strong>lâminas para porta de aço</strong> é que fica mais fácil de fazer o ajuste das medidas do que se elas fossem inteiriças.</p>

                <p>Mesmo existindo diversos modelos de <strong>lâminas para porta de aço</strong>, normalmente, não se combina mais de um, o que pode acontecer é a combinação de um determinado estilo de lâmina com vidro. Esse modelo costuma ser bastante utilizado em portas sociais porque ao mesmo tempo em que garante proteção e segurança, permite observar quem está do lado de fora com um ar mais sofisticado.</p>

 </div>
			</div>
		</div>
	</div>
	<br>
	<br>

<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('553195265215', '553171571978','553196781446'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>
</html>