<?php
$footer = '
<footer>
    <div class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3  col-md-4 col-sm-4 col-xs-3">
                    <h3> Seja Original </h3>
                    <ul>
                        <li> <a href="./index"> Pagina Inicial</a> </li>
                        <li> <a href="./quem-somos"> Quem Somos</a> </li>
                       <!-- <li> <a href="./produtos-portas"> Produtos </a> </li> -->
                        <li> <a href="https://goo.gl/photos/YTenbCXuHDhy9i97A"
							target="_blank" title="Galeria Original Portas"> Galeria</a> </li>
                        <li> <a href="./video-como-montar-instalar-porta-aco-automatica"> Videos</a> </li>
                    </ul>
                </div>
                <div class="col-lg-3  col-md-4 col-sm-4 col-xs-3">
                    <h3> Produtos </h3>
                    <ul>
                        <li> <a href="./porta-enrolar-aco-industrial"> Porta industrial </a> </li>
                        <li> <a href="./porta-enrolar-aco-comercial">Porta Comercial </a> </li>
                        <li> <a href="./porta-enrolar-aco-residencial">Porta Residencial</a> </li>
                        <li> <a href="./porta-rapida-lona"> Porta Rápida  </a></li>
                        <li> <a href="./fabricante-porta-seccionada"> Porta Seccionada </a></li>


                        <!--li> <a href="porta-vidro-automatica"> Porta de Vidro Automática</a></li -->
                        <li> <a href="./pintura-eletrostatica-porta-enrolar-aco"> Pintura Eletrostática </a></li>
                        <li> <a href="./motor-porta-enrolar-aco"> Automatizadores </a></li>
                        <li> <a href="./acessorios"> Acessórios </a></li>

                   </ul>
                </div>
                <!--  div class="col-lg-2  col-md-2 col-sm-4 col-xs-6">
                    <h3> Acessórios </h3>
                    <ul>
                        <li> <a href="./acessorios"> Trava Laminas </a> </li>
                        <li> <a href="./acessorios"> Central p/ motor  </a> </li>
                        <li> <a href="./acessorios"> Controle Remoto </a> </li>
                        <li> <a href="./acessorios"> Nobreak </a> </li>
                        <li> <a href="./acessorios"> Sensor infravermelho </a> </li>
                        <li> <a href="./acessorios"> Fechaduras </a> </li>
                    </ul>
                </div -->
                <div class="col-lg-3  col-md-4 col-sm-4 col-xs-3">
                    <h3> Atendimento </h3>
                    <ul>
                        <li> <a href="./contato-original-portas"> Contato </a> </li>
                        <li> <a href="./orcamento-porta-enrolar-automatica"> Orçamento </a> </li>
                        <li> <a href="./localizacao-fabrica-de-portas"> Localização </a> </li>
                        <li> <a href="./serralheiro-portas-aco"> Serralheiro </a> </li>
                        <!--li> <a href="./trab-con"> Trabalhe Conosco </a> </li -->
                   </ul>
                </div>
                <div class="col-lg-3  col-md-12 col-sm-12 col-xs-3 ">
                  <h3> Redes Socias </h3>
                    <ul class="social">
                        <li><a href="https://pt-br.facebook.com/originalportas/"><i class="fa fa-facebook" title="facebook"></i></a></li>
                        <!-- li><a href="./"> <i class="fa fa-twitter" title="twitter"></i></a></li-->
                        <li><a href="https://plus.google.com/103786054530062695266?hl=pt_br"><i class="fa fa-google-plus" title="google-plus"></i></a></li>
                        <li><a href="https://br.pinterest.com/originalportas/pins/"> <i class="fa fa-pinterest" title="pinterest"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCPINybml0zMMiR-c_XrsjHQ"><i class="fa fa-youtube" title="youtube"></i></a></li>
                    </ul>
                </div>
            </div>
            <!--/.row--> 
        </div>
        <!--/.container--> 
    </div>
    </footer>
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
<div id="pag_foot">
<h2 style="margin-left: 15px">Formas de pagamento</h2>
<p style="margin-left: 15px"> Cartões de crédito: (Visa, Master, 
Elo, Hipercard, Diners), Débito, Cartão Construcard 
(exceto bandeira "Elo"), Boleto Bancário, Cheque , BNDS, Deposito
e Tranferencia Bancária.</p>
</div>
</div>
</div>
<h2>links Relacionados</h2>
<div id="foot-link">
<ul><li>
<a href="./acessorios-portas-aco-automaticas" title="acessórios portas aço automáticas">acessórios portas aço automáticas</a> 
>
<a href="./acessorios-portas-enrolar" title="acessórios portas de enrolar">acessórios portas de enrolar</a> 
>
<a href="./acessorios" title="acessórios portas aço automáticas de enrolar"> acessórios portas aço automáticas de enrolar </a> 
>
<a href="./alcapao-porta-enrolar-automatica" title="alçapão porta enrolar automática">alçapão porta enrolar automática</a> 
>
<a href="./automatizacao-porta-aco-preco" title="automatização porta aço preço">automatização porta aço preço</a>
>
<a href="./automatizacao-porta-aco" title="automatização porta de aço">automatização porta de aço</a>
>
<a href="./automatizacao-porta-enrolar" title="automatização porta enrolar">automatização porta enrolar</a> 
>
<a href="./automatizadores" title="automatizadores porta enrolar">automatizadores porta enrolar</a> 
>
<a href="./certificacao" title="certificação portas enrolar">certificação portas enrolar</a> 
>
<a href="./contato-original-portas" title="contato original portas">contato original portas</a> 
>
<a href="./distribuidor-porta-enrolar" title="distribuidor porta enrolar">distribuidor porta enrolar</a> 
>
<a href="./empresa-pintura-eletrostatica-porta-aco" title="empresa pintura eletrostática porta aço">empresa pintura eletrostática porta aço</a> 
>
<a href="./fabrica-porta-aco-enrolar" title="fabrica porta aço enrolar">fabrica porta aço enrolar</a> 
>
<a href="./fabricante-laminas-porta-aco" title="fabricante laminas portas aço">fabricante laminas portas aço</a> 
>
<a href="./fabricante-porta-seccionada" title="fabricante porta seccionada">fabricante porta seccionada</a> 
>
<a href="./fabricante-portinhola-porta-aco" title=" fabricante portinhola porta aço ">fabricante portinhola porta aço</a> 
>
<a href="./laminas-portas-aco" title="laminas portas aço">laminas portas aço</a> 
>
<a href="./localizacao-fabrica-de-portas" title="localização fabrica de portas">localização fabrica de portas</a> 
>
<a href="./motor-automatizador-porta-aco-automatica" title="motor automatizador porta aço automática">motor automatizador porta aço automática</a> 
>
<a href="./motor-automatizador-porta-aco-manual" title="motor automatizador porta aço manual">motor automatizador porta aço manual</a> 
>
<a href="./motor-porta-enrolar-aco" title="motor porta de enrolar aço">motor porta de enrolar aço</a> 
>
<a href="./orcamento-porta-enrolar-automatica" title="orçamento porta enrolar automática">orçamento porta enrolar automática</a> 
>
<a href="./pintura-eletrostatica-porta-enrolar-aco" title="pintura eletrostática porta enrolar aço">pintura eletrostática porta enrolar aço</a> 
>
<a href="./pintura-eletrostatica" title="pintura eletrostática">pintura eletrostática</a> 
>
<a href="./porta-aco-automatica-preco" title="porta aço automática preço">porta aço automática preço</a> 
>
<a href="./porta-aco-automatica" title="porta aço automática">porta aço automática</a> 
>
<a href="./porta-aco" title="porta aço">porta aço</a> 
>
<a href="./porta-enrolar-aco-comercial" title="porta enrolar aço comercial">porta enrolar aço comercial</a> 
>
<a href="./porta-enrolar-aco-industrial" title="porta enrolar aço industrial">porta enrolar aço industrial</a> 
>
<a href="./porta-enrolar-aco-residencial" title="porta enrolar aço residencial">porta enrolar aço residencial</a> 
>
<a href="./porta-enrolar-automatica" title="porta enrolar automática">porta enrolar automática</a> 
>
<a href="./porta-loja-automatica" title="porta loja autoática">porta loja automática</a> 
>
<a href="./porta-rapida-dobravel" title="porta rápida dobravél">porta rápida dobravél</a> 
>
<a href="./porta-rapida-enrolar" title="porta rápida enrolar">porta rápida enrolar</a> 
>
<a href="./porta-rapida-lona" title="porta rápida lona">porta rápida lona</a> 
>
<a href="./porta-seccionada-industrial" title="porta seccionada industrial">porta seccionada industrial</a> 
>
<a href="./porta-seccionada-residencial" title="porta seccionada residencial">porta seccionada residencial</a> 
>
<a href="./serralheiro-portas-aco" title="serralheiro portas de aço">serralheiro portas de aço</a> 
>
<a href="./quem-somos" title="nossa empresa">Nossa Empresa</a> 
>
<a href="./video-como-montar-instalar-porta-aco-automatica" title="vídeo como instalar porta aço automática">vídeo como instalar porta aço automática</a> 
>



</ul>
</div>
<div class="footer-bottom">
        <div class="container">
            <p class="pull-left" style="text-align:center "> Todos dos Direitos Reservados &copy; Original Portas de Aço Automáticas . (Lei 9610 de 19/02/1998)</p>
         </div>
    </div>
    <p style="text-align: center; font-size: 12px;"><br/>created by: tworock.desenv@gmail.com</p>
    <!--/.footer-bottom--> 

<script src="js/jquery-1.9.1.min.js"></script>
';?>
</body>
</html>