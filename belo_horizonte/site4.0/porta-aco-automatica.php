
<?php
require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Porta de Aço Automática - Original Portas</title>
<base>
<meta name="description"
	content="A porta de aço automática é cada vez mais procurada por comerciantes e por pessoas que querem deixar a sua casa ou estabelecimento comercial protegido">
<meta name="keywords"
	content="Porta de Aço Automática, porta, aço, automática, enrolar, aco">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="porta-aco-automatica">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" href="imagens/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Porta de Aço Automática - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/porta-aco-automatica.png">
<meta property="og:url" content="porta-aco-automatica">
<meta property="og:description"
	content="A porta de aço automática é cada vez mais procurada por comerciantes e por pessoas que querem deixar a sua casa ou estabelecimento comercial protegido">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2">
				<img src="imagens/porta-aco-automatica.png" width="100%"
					height="auto">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="class_aut2">
			<div class="row">
				<div class="col-md-12">

					<h1 style="margin-top: -20px;">Porta de Aço Automática</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					<h2>Vantagens da porta de aço</h2>
					<p>
						A <strong>porta de aço automática</strong> é cada vez mais
						procurada por comerciantes e por pessoas que querem deixar a sua
						casa ou estabelecimento comercial protegido. Além de ser elaborada
						com material extremamente resistente, ela é super fácil de
						manusear.
					</p>
					<p>
						Antigamente, as pessoas buscavam portões manuais elaborados em
						diversos metais e isso ainda acontece, porém, a <strong>porta de
							aço automática</strong> tem ganhado espaço e muitas vezes até
						substituído antigos portões. O aço é extremamente resistente e em
						caso de uma tentativa de invasão, ele faz papel de uma barreira
						protetora, difícil de ser ultrapassada.
					</p>
					<p>
						A <strong>porta de aço automática</strong>, além de tudo, pode ser
						controlada apenas com um botão, isso faz com que a pessoa não
						precise se deslocar até ela para abrir e fechar, poupando esforço
						físico.
					</p>
					<p>
						Ela pode ser desenvolvida com lâminas bastante diversificadas. É
						possível, por exemplo, uma <strong>porta de aço automática</strong>
						totalmente fechada ou com vãos para que se tenha visão interna.
						Essa é preferida por comerciantes de shoppings e chama-se
						Transvision: consegue garantir a proteção, mas também permite que
						é vitrine seja observada quando a loja se encontra fechada.
					</p>
					<p>O custo de instalação e manutenção também são em conta. Mas é
						preciso manter cuidados com a conservação da porta. Dessa maneira,
						se torna um investimento bastante vantajoso.</p>
					<h2>A porta de aço automática tem desvantagens?</h2>
					<p>
						Se pararmos para observar bem a <strong>porta de aço automática</strong>
						não apresenta desvantagens, diferentemente da porta de aço manual,
						que é bastante pesada e exige um pouco mais de esforço para que
						possa ser movimentada.
					</p>
					<h2>Qualquer empresa pode comercializar uma porta de aço
						automática?</h2>
					<p>A venda desse tipo de equipamento deve ser realizada por
						empresas especializadas na prestação desse serviço, como a
						Original Portas, que trabalha com portas desenvolvidas nas medidas
						exatas do ambiente. A empresa ainda oferece serviço de instalação
						com técnicos capacitados.</p>
					<p>
						Na dúvida antes de contratar o serviço, vale a pena conferir o
						histórico da empresa de <strong>porta de aço automática</strong> e
						também a avaliação de outros clientes. Referencias assim permitem
						que você entenda se a empresa presta um bom serviço e se outros
						clientes já tiveram problemas com ela ou não. É a melhor forma de
						garantir que você será bem atendido em suas necessidades!
					</p>

				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
<?php echo $footer;?>
</body>
</html>