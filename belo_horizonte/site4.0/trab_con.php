<?php
require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async
	src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script>
<script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Trabalhe Conosco - Original Portas</title>
<base>
<meta name="description"
	content="Envie sua mensagem atrav&eacute;s do formulario de or&ccedil;amento e entraremos em contato com voc&ecirc;, saiba o pre&ccedil;o de portas de enrolar, portas de a&ccedil;o &eacute; caro? ,portas de enrolar &eacute; caro?">
<meta name="keywords"
	content="Trabalhe na Original Portas, emprego, Original Portas, Trabalhe">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="S&atilde;o Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="https://www.originalportas.com.br/trab_con">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" href="https://www.originalportas.com.br/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Or&ccedil;amento de portas de a&ccedil;o - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url"
	content="orcamento-porta-enrolar-automatica">
<meta property="og:description"
	content="Original Portas, or&ccedil;amento de portas e enrolar, pre&ccedil;o portas de a&ccedil;o, portas a&ccedil;o autom&aacute;ticas, portas de enrolar, portinhola, al&ccedil;ap&atilde;o, automatizadores, controle port&atilde;o. tudo para portas de a&ccedil;o">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="https://www.originalportas.com.br/bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="https://www.originalportas.com.br/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://www.originalportas.com.br/bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="https://www.originalportas.com.br/css/style2.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- -----------------JS----------------- -->
<script type="text/javascript" src="https://www.originalportas.com.br/js/jquery-1.7.2.min.js"></script>
<script src="https://www.originalportas.com.br/js/validation.js" type="text/javascript"></script>
<!-- -------------Valida&ccedil;&atilde;o input/telefone ----------------------------------->
<script type="text/javascript" src="https://www.originalportas.com.br/js/mask_number.js"></script>
<!-----------------font Google------------------>
<link href="https://fonts.googleapis.com/css?family=Play"
	rel="stylesheet">

<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<style>
.container, .row, label, h1, h2, h3, h4, h5, p {
	font-family: 'Play', sans-serif;
}
</style>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="https://www.originalportas.com.br/js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="https://www.originalportas.com.br/js/jquery.slicknav.js"></script>
<script defer src="https://www.originalportas.com.br/js/geral.js"></script>
<!--  busca cep -->
<script defer src="https://www.originalportas.com.br/js/busca_cep.js"></script>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2">
				<img alt="trabalhe na original poras" src="imagens/trabalhe-conosco.png" width="100%" height="auto">
			</div>
		</div>
	</div>
<div class="container">
		<div class="form">
			<div class="row" style="background-color: #fff;margin-top: -100px;">
				<div class="col-md-12">
					<h1>Trabalhe Conosco</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					<br>
					  <p style="font-family: arial;">Trabalhar na Original Portas Autom&aacute;ticas &eacute; fazer parte de uma empresa com crescimento Nacional j&aacute; em expans&atilde;o internacional; Nossos produtos j&aacute; se encontram presentes em mais de 3 paises. <br><br>
					  Trabalhar conosco &eacute; viver em um ambiente inspirador, cheio de desafios e oportunidades.
                      Aqui, o respeito às pessoas parte de uma cultura inclusiva, baseada na diversidade, na integra&ccedil;&atilde;o e no ambiente colaborativo, que estimula e favorece o desenvolvimento e a evolu&ccedil;&atilde;o da carreira. Aqui, a valoriza&ccedil;&atilde;o do coletivo motiva as pessoas a darem o melhor de si.
                      <br><br>A empresa que mais cresce no ramo de Portas de A&ccedil;o Automaticas traz para voc&ecirc; uma oportunidade &uacute;nica de crescimento e desenvolvimento profissional e pessoal. Uma oportunidade de estar ao lado de profissionais competentes e engajados na busca por resultados e inova&ccedil;&atilde;o.
                      Buscamos profissionais que, como nós, saibam transformar desafios em oportunidades.
<br><br>Venha fazer parte do nosso Time original Portas Autom&aacute;ticas</p><br> <br><br>
					
				</div>
				<form enctype="multipart/form-data" id="formContato" method="post" name="trab-envia" action="https://www.originalportas.com.br/trab-envia" class="form2">
					<!-- input type="hidden" name="ACAO" value="ENVIAR" / -->

					<div class="col-md-6">
						<label for="nome">Nome: <span>*</span></label><br> <input
							onkeypress="return Onlychars(event)"  onKeyUp="UcWords(this)"
							type="text" name="nome" class="nome" value="" id="nome" size="34" />
					</div>
					<div class="col-md-6">
						<label for="telefone"> DDD/Telefone: <span>*</span></label><br> <input
							type="text" name="ddd" class="ddd" value="" id="ddd" size="2"
							maxlength="2"  onkeypress="return Onlynumbers(event)" /> 
							
							<input
							type="text" name="telefone" class="telefone" value=""
							id="telefone" size="24" maxlength="9"
							onkeypress="return Onlynumbers(event)" />
					</div>
					<div class="col-md-6">
						<label for="email">E-mail: <span>*</span>
						</label><br> <input onKeyUp="minusculas(this)" type="text"
							name="email" class="email" value="" id="email" size="32" />
					</div>
					<div class="col-md-6">
						<label> Vagas de Interesse: <span>*</span>
						</label><br> <select name="vg_disp">
							<option value=""></option>
							<option value="Ajudante">Ajudante Geral</option>
							<option value="Aux-Vendas">Aux. Vendas </option>
                            <option value="Aux.Administrativo">Aux. Administrativo</option>							
							<option value="Motorista">Motorista</option>	
							<option value="Motoboy">Motoboy</option>							
							<option value="Operador">Operador de M&aacute;quina</option>
							<option value="Soldador">Soldador MIG</option>
							<option value="Vedendor(a)">Vedendor(a) Interno</option>							
						</select>
					</div><div class="col-md-12">
						<label>Cep:<a style="font-size: 15px"></a><br>
							<input name="cep" type="text" id="cep" value="" size="10"
							maxlength="9" onkeypress="return Onlynumbers(event)" /></label><br>
					</div>
					<div class="col-md-6">

						<label>Rua: <br> <input name="rua" type="text" id="rua" size="34"
							maxlength="60" />
						</label><br> <label>Bairro:<br> <input name="bairro" type="text"
							id="bairro" size="34" maxlength="50" /></label><br>
					</div>
					<div class="col-md-6">
						<label>Cidade:<br> <input name="cidade" type="text" id="cidade"
							size="34" maxlength="50" />
						</label><br> <label> UF:<br> <input name="uf" type="text" id="uf"
							size="3" maxlength="2" />
						</label><br> <br>
					</div>
					<div class="col-md-12">
					   <label for="file" >Envie o Curriculo no formato PDF</label>
					
					   <input type="file" id="anexo" style="width: 322px;" name="anexo" multiple accept=".pdf">
					</div>
					<br>
						<div class="col-md-12">
						<p>Os campos com (*) s&atilde;o obrigat&oacute;rios</p>
						<br> 
						<div class="bt-submit">
				        <button type="submit" value="Enviar" class="btn btn-primary">Enviar</button>
						</div></div>
				</form>
						
			</div>

<br>
	</div>
</div>
<?php echo $footer;?>
</body>
</html>