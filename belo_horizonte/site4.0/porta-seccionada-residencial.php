<?php
require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
        <title>Porta Seccionada residencial - Original Portas</title>
<base>
<meta name="description"
	content="As portas seccionadas residenciais são bastante utilizadas na europa e em garagens norte-americana, essas portas aparecem sempre em filmes Americanos ">
<meta name="keywords"
	content="Porta Seccionada residencial, porta, seccionada, residencial, portão garagem americano,Europa, europeu , portão, americano ">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="porta-seccionada-residencial">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" href="imagens/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Porta Seccionada residencial - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/porta_seccionada/porta-seccionada-residencial1.png">
<meta property="og:url" content="porta-seccionada-residencial">
<meta property="og:description"
	content="As portas seccionadas residenciais são bastante utilizadas na europa e em garagens norte-americana, essas portas aparecem sempre em filmes Americanos ">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2">
				<img src="imagens/porta_seccionada/porta-seccionada-residencial1.png" width="100%" height="auto" >
			</div>
		</div>
	</div>
	<div class="container">
		<div class="class_aut2">
			<div class="row">
				<div class="col-md-12">
				
				
        <h1 style="margin-top:-20px;">Porta Seccionada residencial</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
			
                     <h2></h2>
<div class="col-sm-12 col-md-12 col-xl-7" style="margin-top: 20px;">
				  <video width="100%" muted loop autoplay>
                    <source src="video/porta-seccionada.mp4" type="video/mp4">
                  </video>
                  
			</div>
                <p>A <strong>porta seccionada Residencial</strong> é elaborada em estrutura que lhe garante uma maior resistência e durabilidade. Isso também faz com que ela não consiga ser facilmente violada, sendo uma resistente barreira para possíveis invasores.</p>
                <p>Esse tipo de porta é composto com paineis de até 40mm de espessura , formado por duas chapas galvanizadas com o miolo preenchido de poliuretano expandido, e podem ser pintados com pintura eletrostática.</p>
<p>As portas seccionadas residenciais são bastante utilizadas na europa e em garagens norte-americana, essas portas aparecem sempre em filmes Americanos .
<p>A Original Portas trouxe de fora essa novidade pra você ter em sua casa o modelo de porta internacionalmente conhecido por <strong>Portas Seccionadas. </strong></p>
                <p>Ela ainda conta com um isolamento acústico, que faz com que o local fique mais silencioso, mesmo quando do lado de fora existe um grande barulho. Isolamento térmico também é uma de suas vantagens. Assim, o local conta com uma temperatura sempre agradável internamente.</p>

                <p>A porta também possui o modelo automatizada que facilita a abertura e o fechamento dela e o modelo Manual para quem quer fazer um baixo investimento.</p>
<br>
<br>
                <div class="col-md-6" style="background-color: transparent;">
				<img src="imagens/porta_seccionada/porta-seccional-residencial-interna.jpg" width="100%" height="auto" ><br>
				                <p>Modelo com guia curva superior</p>
				
			<br>
			</div>
                <div class="col-md-6" style="background-color: transparent;">
				<img src="imagens/porta_seccionada/porta-seccional-residencial-lateral.jpg" width="100%" height="auto" ><br>
						        <p>Modelo com guia curva lateral</p>
				
			<br>
			</div>
			


				</div>

			</div>
		</div>
	</div>
	<br>
	<br>

<?php echo $footer;?>
</body>
</html>