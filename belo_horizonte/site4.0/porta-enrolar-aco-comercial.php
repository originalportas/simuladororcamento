<?php require 'main.php';
require 'footer.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Porta Comercial Automática para lojas , shoppings e comércios - Original Portas</title>
<base>
<meta name="description" content="A porta comercial automática possui funções que contribui na segurança de onde ela é instala, pois não demanada tempo para abri-la, ela abre apenas com um botão">
<meta name="keywords" content="Porta Comercial Automática, porta, comercial, automática, porta de loja, porta de shopping, porta de enrolar">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="porta-enrolar-aco-comercial.php">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" href="/site4.0/imagens/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title" content="Porta Comercial de Enrolar Automática - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url" content="porta-enrolar-aco-comercial.php">
<meta property="og:description" content="A porta comercial automática possui funções que contribui na segurança de onde ela é instala, pois não demanada tempo para abri-la, ela abre apenas com um botão">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="/site4.0/bootstrap/css/bootstrap.css"type="text/css" />
<link href="/site4.0/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/site4.0/bootstrap/css/beta.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="/site4.0/css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script> 
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="/site4.0/js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="/site4.0/js/jquery.slicknav.js"></script>
<script defer src="/site4.0/js/geral.js"></script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro3">
				<img src="imagens/porta_comercial/porta-enrolar-comercial.png" width="100%" height="auto">
			</div>
		</div>
	</div>
<div class="container">
    <div class="class_port">
    	<div class="row">
    	 <div class="col-md-6" >
					<img id="port_comerc" alt="Porta comercial"
						src="imagens/porta_comercial/DSC0060677.jpg">
				</div>
			<div id="id_port" class="col-md-6" >
				<h1>PORTA DE AÇO COMERCIAL</h1>
					   	   <hr style="width: auto; height: 2px; background-color: #ccc;">
				
                    <p> Nossas portas de aço automáticas para comércios, shoppings e lojas, possuem um alto padrão de qualidade e estética, deixando a fachada do seu comércio mais apresentavél aos seus clientes. 
                    <p> Optando pelas laminas transvision é possivel divulgar seu comércio, mesmo com as portas fechadas.<br>
                     Você também pode optar pela pintura eletrostática e aumentar ainda mais a beleza de sua loja.</p>
				    <p> Nossas Portas Comerciais são indicadas em:<br>
				      - Shoppings <br>
				      - Lojas <br>
				      - Comércios <br>
				      - Galerias <br>
				      - Estádios <br>
				      - Bancas de Jornais <br>
				      - Containers <p>
				    <p> Faça um orçamento conosco, além de cumprirmos todas as normas de qualidade exigidas, damos garantia de nossos produtos, entre em contato e tire todas as suas dúvidas.</p>
             </div>
           
	      <br>
	  </div>
   </div>        
</div>
<?php echo $footer;?>
</body>
</html>