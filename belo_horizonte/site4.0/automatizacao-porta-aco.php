<?php
require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
        <title>Automatização de Porta de Aço - Original Portas</title>
<base>
<meta name="description"
	content="Com a automatização de porta de aço fica muito mais fácil de fazer a abertura de sua loja ou fazer a abertura do seu estabelecimento comercial">
<meta
	name="Automatização de Porta de Aço, automatização, porta, aço, enrolar, aco">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="automatizacao-porta-aco">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" href="imagens/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Automatização de Porta de Aço - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image"
	content="imagens/automatizador/automatizacao-porta-aco.png">
<meta property="og:url" content="automatizacao-porta-aco">
<meta property="og:description"
	content="Com a automatização de porta de aço fica muito mais fácil de fazer a abertura de sua loja ou fazer a abertura do seu estabelecimento comercial">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 
//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2">
				<img src="imagens/automatizador/automatizacao-porta-aco.png"
					width="100%" height="auto">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="class_aut2">
			<div class="row">
				<div class="col-md-12">
					<h1 style="margin-top: -20px;">Automatização de Porta de Aço</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
                <h2>Os tipos de portas automáticas</h2>
                <p>As portas automáticas podem ser de diversos tipos. Isso porque cada local pede um estilo especifico. O melhor é que a maioria delas pode passar pela <strong>automatização de porta de aço</strong>.</p>
                <p>As portas de enrolar são ideais para os locais em que não existe muito espaço para a movimentação, mas que necessitam de proteção. Com a <strong>automatização de porta de aço</strong> fica muito mais fácil fazer a abertura de sua loja ou estabelecimento comercial.</p>
                <p>A porta articulada é bastante utilizada em garagens e galpões por onde passam veículos, mas não são restritas unicamente a esses locais. Como normalmente elas são bastante grandes, sua movimentação quando manual fica difícil, por isso, a <strong>automatização de porta de aço</strong> faz com que ela seja mais facilmente manuseada.</p>
                <p>Já as portas deslizantes e basculantes normalmente são utilizadas em garagens que não sejam muito grandes, mas ter que descer do veículo para fazer a abertura pode trazer perigos ao motorista. Por isso, a <strong>automatização de porta de aço</strong> torna essa ação muito mais segura e cômoda.</p>
                <p>As portas sociais são indicadas para locais onde existe a passagem de pessoas e permite que elas sejam acionadas à distância. Mas para que isso ocorra é preciso que haja a <strong>automatização de porta de aço</strong>. Dessa maneira, com um simples botão pode-se permitir a entrada e a saída de pessoas facilmente, sem precisar ir até o local e abrir com a chave.</p>
                <h2>Como fazer a automatização de porta de aço</h2>
                <p>Em muitos casos, as empresas e residências já possuem portas de aço manuais e querem deixá-las mais dinâmicas e fáceis de serem manuseadas. Então, pode-se fazer a <strong>automatização de porta de aço</strong> com algumas adaptações e com a utilização dos equipamentos e acessórios certos.</p>
                <p>Porém, para que isso ocorra, é preciso ter conhecimentos específicos. Para quem não quer arriscar danificar a o portão, o ideal é contar com a ajuda das empresas especializadas, como a Original Portas. Essa empresa consegue analisar a sua necessidade e fazer a <strong>automatização de porta de aço</strong> em pouco tempo e, o melhor, com um serviço que incluiu garantia e equipe técnica especializada.</p>
                <p>Na hora de contratar a empresa, é preciso fornecer as medidas ou solicitar a visita de um técnico para orçamento sem compromisso.</p>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
<?php echo $footer;?>
</body>
</html>