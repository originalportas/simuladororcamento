<?php
require 'con_db/conex.php';
require 'main.php';
require 'footer.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Status Pedido - Original Portas</title>
<base>
<meta name="description"
	content="rastrear pedido de portas de aço automaticas, ratrear entrega original portas, entrega de portas de aço automaticas">
<meta name="keywords"
	content="pedido, rastrear, original portas, entregas, entrega de portas, portas automáticas ">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="pintura-eletrostatica.php">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" href="imagens/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Pintura elestrostática - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/pintura-eletrostatica-01.png">
<meta property="og:url" content="pintura-eletrostatica.php">
<meta property="og:description"
	content="A pintura eletrostática é uma tinta em pó com processo de acabamento a seco que se tornou extremamente popular desde a sua introdução. Representando mais de 15% do mercado total de acabamento industrial, o pó é usado em uma ampla gama de produtos. Mais e mais empresas buscam está técnica de tinta em pó para um acabamento durável e de alta qualidade, permitindo a produção máxima, eficiência aprimorada e conformidade ambiental simplificada. Usados ​​como acabamentos funcionais (protetores) e decorativos, revestimentos em pó estão disponíveis em uma gama de cores e texturas, os avanços tecnológicos resultaram em excelentes propriedades de desempenho.">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=iso-Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">

<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>
</head>
<body ondragstart="return false">

	<div class="container">
		<div class="busc_ped">
			<div class="row">
				<div class="col-md-12 col-xs-12">
<?php


//$count = mysqli_num_rows($query)or die( mysql_error());
$numrow = mysqli_num_rows($query);

//if ($busca and $count != NULL && $busca != ' ') {
if ($busca != NULL and $busca != ' ' and $busca >= '0' and $numrow >0) {
    
    echo '<ul>';
    while ($resultado = mysqli_fetch_array($query)) {
        $num_pedido = $resultado['NUM_PEDIDO'];
        $desc_pedido = $resultado['DESC_PEDIDO'];
        $status_pedido = $resultado['STATUS_PEDIDO'];
        $status_pag = $resultado['STATUS_PAG'];
        $status_nf = $resultado['STATUS_NF'];
        $status_producao = $resultado['STATUS_PRODUCAO'];
        $status_entrega = $resultado['STATUS_ENTREGA']; 
    }
        echo  '<h2>Nº pedido: <strong style="color:#000;font-size:32px;">#'.$num_pedido.'</style></strong></h2>
                   <table class="table table-striped">
						<thead>
							<tr>
								<th scope="col"><h4>Descrição</h4></th>
								<th scope="col"><h4>Pedido</h4></th>
								<th scope="col"><h4>Pagamento</h4></th>
								<th scope="col"><h4>Nota Fiscal</h4></th>
								<th scope="col"><h4>Produção</h4></th>
								<th scope="col"><h4>Entrega</h4></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								
                                <td><h4 style="color:rgb(50,120,180)">' .$desc_pedido.      '</h4></td>
								<td><h4 style="color:rgb(50,120,180)">' .$status_pedido.      '</h4></td>
								<td><h4 style="color:rgb(50,120,180)">' .$status_pag.      '</h4></td>
								<td><h4 style="color:rgb(50,120,180)">' .$status_nf.       '</h4></td>
								<td><h4 style="color:rgb(50,120,180)">' .$status_producao. '</h4></td>
								<td><h4 style="color:rgb(50,120,180)">' .$status_entrega.  '</h4></td>
							</tr>
						</tbody>
					</table>';
   
} else  {
    
    echo '<div class="alert alert-danger" role="alert">
  <h3> Nenhum pedido encontrado!</h3>
</div>';

  
}


	    ?>
				</div>
			</div>
			<a href="../site4.0/rastrear_pedido_porta.php" style="color:#000"><button type="button" class="btn btn-primary" style="padding:8px;font-size: 12px;">Voltar</button></a>
		</div>
	</div>
	<br>
	<br>
<?php // mysqli_free_result($resultado);
echo $footer;?>
</body>
</html>