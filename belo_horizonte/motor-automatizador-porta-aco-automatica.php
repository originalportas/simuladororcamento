<?php require 'main.php';
require 'footer.php';?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
<title>Automatizador para portas de aço - Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description" content="Nosso motor para porta de enrolar é de altíssima qualidade trazendo um produto de confiança para nossos clientes, além de possuir o selo INMETRO, também possui garantia.">
<meta name="keywords" content="Motor para portas de enrolar, motor para portas de shopping, automatizador, megatron, motor automatico,motor manual, automático, motor ac 200, motor ac 300, motor ac 400, motor ac 500, motor ac 600, motor ac 700, motor ac 800, motor ac 900, motor ac 1000,motor ac 1200">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="motor-automatizador-porta-aco-automatica">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title" content="Automatizador para portas de  aço automática - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url" content="motor-automatizador-porta-aco-automatica">
<meta property="og:description" content="Nosso motor para porta de enrolar é de altíssima qualidade trazendo um produto de confiança para nossos clientes, além de possuir o selo INMETRO, também possui garantia.">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script> 
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2" style="margin-top:60px;">
				<img src="imagens/automatizador/automatizador-portas-enrolar-aco.png" width="100%" height="auto" >
			</div>
		</div>
	</div>

<div class="container">
   <div class="class_aut">
   	<div class="row">
	<div id="id_port" class="col-md-12" >
	 <h1>Automatizadores para portas de aço automáticas</h1>

<hr style="width: auto; height: 2px; background-color: #ccc;">
<p>A automatização permite movimentar uma enorme quantidade de metal com mais facilidade, deixando de lado o trabalho de abertura e fechamento manual, com o automatizador você precisa apenas apertar um botão para fazer o motor para portas de enrolar de aço automática fazer todo o trabalho. </p>

<p>Nosso motor para porta de enrolar é de altíssima qualidade trazendo um produto de confiança para nossos clientes, além de possuir o selo INMETRO, também possui garantia.</p>

<p>Temos motores que suportam os mais diversos tipos de portas de aço, independente do seu tamanho e peso, possuímos um modelo especifico para atender a cada necessidade</p>

<h2>Qual motor automatizador para portas de enrolar automáticas devo escolher?</h2>

<p>As orientações do fabricante dizem que é relevante o tipo de porta de aço, as medidas das lâminas, soleiras e do eixo, também depende dos tipos de lâminas de aço sendo (transvision ou fechada meia-cana) sem esquecer-se da quantidade de aberturas e fechamentos que o usuário ira realizar no decorrer do dia.

Caso não siga essas orientações do fabricante, o seu motor pode ter vida útil reduzida pelo fato de estar suprindo uma carga maior que sua capacidade, também pode ser pdanificado se houver uma quantidade de acionamentos acima de seu limite diário.
Por isso, quem deseja saber sobre automatização de portas de aço é bom nos consultar, temos profissionais que podem auxilia-lo e cursos de orientações e boas praticas.</p>


<h2>Motor para portas de aço de enrolar é caro? Qual o é seu preço? Quanto custa? </h2>

<p>A automatização para portas de aço tem um preço bem acessível e que será determinado pelo tipo de porta, tamanho das laminas e quantidade de aberturas e fechamentos diários.</p>
<p>E o custo disso não costuma ser elevado, pois compensa quando se analisa o custo e benefício.</p>

</div>

</div>
</div>
</div>
<div class="container-fluid">
<div class="row" id="id_aut_row">
<div id="id_aut0" class="col-md-12"></div>
<div id="id_aut" class="col-md-12">

<h2>Especificações Técnicas </h2>
<hr style="width: auto; height: 2px; background-color: #FFF;">
<p>A capacidade de carga e o fluxo de trabalho do motor , variam  de modelo para modelo como por exemplo: O motor para porta de enrolar no Modelo AC200 tem capacidade para suportar uma porta de aço de até 200kg, já o motor para porta de enrolar modelo AC300 tem capacidade para suportar uma porta de aço de até 300Kg, conforme maior o <strong> AC </strong> maior a capacidade de carga.<br>Veja o comparativo abaixo. </p>  
</div>
<div id="id_aut3" class="col-md-6">
<table class="table">
<tbody>
    <tr>
      <th><h2>Modelo AC-200</h2></th>
            <th><h2></h2></th>
      
    </tr>
    <tr> 
      <th><p>Tensão: 220 V ~/1F</p></th>
      <th><p>Potencia: 3/4 HP - 350W</p></th>
    </tr>
    <tr>
      <th><p>Corrente: 1,9A</p></th>
      <th><p>Frequência: 60Hz</p></th>
    </tr>
    <tr>
      <th><p>RPM: 5,3</p></th>
      <th><p>Carga: 200Kg / 1.91 N</p></th>
    </tr>
    <tr>
      <th><p>Ciclo Nominal: 9.500</p></th>
      <th><p>Temp. Amb.: -20ºC ~ 45ºC</p></th>
    </tr>
    <tr>
      <th><p></p></th>
      <th><p></p></th>
    </tr>
  </tbody>
</table>
</div>
<div id="id_aut4" class="col-md-6">
<table class="table">
<tbody>
    <tr>
      <th><h2>Modelo AC-300</h2></th>
            <th><h2></h2></th>
    </tr>
    <tr> 
      <th><p>Tensão:220 V ~/1F</p></th>
      <th><p>Potencia: 1/2 HP - 350W</p></th>
    </tr>
    <tr>
      <th><p>Corrente: 3,3A</p></th>
      <th><p>Frequência: 60Hz</p></th>
    </tr>
    <tr>
      <th><p>RPM: 5,3</p></th>
      <th><p>Carga: 300Kg / 2.942 N</p></th>
    </tr>
    <tr>
      <th><p>Ciclo Nominal: 9.500</p></th>
      <th><p>Temp. Amb.: -20ºC ~ 45ºC</p></th>
    </tr>
    <tr>
      <th><p></p></th>
      <th><p></p></th>
    </tr>
  </tbody>
</table>
</div>
<div id="id_aut2" class="col-md-12">
<h2>Termos de Garantia do fabricante.</h2>
<hr style="width: auto; height: 2px; background-color: #FFF;">

<p>A Garantia consiste no reparo e substituição de peças ou acessórios fornecidos, desde
que constatados os defeitos na fabricação pelos nossos técnicos. Portanto, a contar da
data da compra do produto já entra em vigor a garantia de 2 (dois) anos para o
AUTOMATIZADOR e 1 (um) ano para KIT DE CONTROLE SEM FIO E CENTRAL DE
COMANDO. <br> 
Obs: A garantia não se aplica a consumidores finais, somente a empresas que
consomem tais produtos. (Quaisquer problemas que ocorram com o equipamento
fornecido, entre em contato diretamente com a empresa que lhe forneceu) .</p>
<h2>QUANDO OCORRE A PERDA DA GARANTIA:</h2>
<p>1 - Quando removidos os lacres de
garantia, ou constatado alterações/consertos realizados por técnicos não autorizados.
<br>2 - Acidentes involuntários, choque de veículo, incêndio, arrombamento, danos
realizados por terceiros.
<br>3 - Instalação de acessórios não fornecidos/incompatíveis c	om o equipamento.
<br>4 - Corrosão por produtos químicos, ácidos, detergentes, solventes e etc.
<br>5 - Danos causados por intempéries (ações climáticas nocivas), descarga elétrica
(raios), maresia ou água no sistema eletrônico/mecânico e etc.
<br>6 - Montagem ou utilização fora da especificação do fabricante.
<br>7 - Quando detectado uso excessivo no número de aberturas diárias ( equipamento
indicado para 4 acionamentos diários ) A substituição de peças ou consertos de nossos
produtos no período de garantia, não se prorroga.
Qualquer alteração no funcionamento do produto entre em contato com a empresa que forneceu.
<br>8 – A garantia funciona da seguinte forma:
caso o automatizador apresente defeito, o nosso cliente direto deve trazê-lo para que a manutenção seja feita.</p>
</div>
 <br>
 </div>
</div>        
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('553195265215', '553171571978','553196781446'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	

<?php echo $footer;?>
</body>
</html>