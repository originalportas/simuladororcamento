<?php
require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async
	src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script>
<script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Porta de Enrolar Automática - Original Portas</title>
<base>
<meta name="description"
	content="Um modelo de porta bastante procurado é a porta de enrolar. Isso
						porque ela não ocupa muito espaço, pois pode ser otimizada ao ser
						aberta, enrolando uma lâmina de metal sobre a outra.">
<meta name="keywords"
	content="Porta de Enrolar Automática, porta, enrolar, automática, porta de aço, porta de loja, porta de shopping, porta de rolo, portas de enrolar">
<meta property="article:publisher"
	content="https://www.facebook.com/originalportas" />
<meta property="article:tag" content="porta de enrolar automática" />
<meta property="article:published_time"
	content="2018-01-08T12:13:40+00:00" />
<meta property="article:modified_time"
	content="2018-01-08T15:17:41+00:00" />
<meta property="og:updated_time" content="2018-01-08T15:17:41+00:00" />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="porta-enrolar-automatica">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" href="imagens/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Motor automatizador para portas de aço manual- Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="porta-aco-industrial.jpg">
<meta property="og:url" content="porta-enrolar-automatica">
<meta property="og:description"
	content="Um modelo de porta bastante procurado é a porta de enrolar. Isso
						porque ela não ocupa muito espaço, pois pode ser otimizada ao ser
						aberta, enrolando uma lâmina de metal sobre a outra.">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>


<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro3">
				<img src="imagens/porta-enrolar-automatica.png" width="100%"
					height="auto">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="eletro_div">
			<div class="row">
				<div class="col-md-12">
					<h1>Porta de Enrolar Automática</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					<p>
						As <strong>Portas de Enrolar Automáticas</strong> da Original
						Portas, são a busca ideal para os comerciantes e também todos que
						querem deixar a sua Casa, Indústria ou Estabelecimento Comercial
						mais moderno e ao mesmo tempo protegido, pois além de serem
						elaboradas com materiais extremamente resistentes, elas se adaptam
						perfeitamente em qualquer design ou tipo de construção, não
						interferindo na beleza do seu projeto arquitetônico, fachada ou
						layout.
					</p>
				</div>
				
					<div class="col-md-12 col-xs-12 col-sm-12">
					<h2>Mas afinal, o que é uma Porta de Enrolar Automática?</h2>
					<p>
						Uma <strong>Porta de Enrolar Automática</strong> é basicamente uma
						cortina metálica construída por lâminas horizontais em aço, que
						são perfiladas (dobradas) em um formato especial para garantir o
						seu perfeito encaixe e travamento umas nas outras.
					</p>
					<p>
						Possuem ainda na parte inferior da Cortina de Lâminas uma Soleira
						também feita em aço, que é responsável pelo fechamento correto e
						seguro do estabelecimento, além da vedação do espaço entre a <strong>Porta
							de Enrolar Automática</strong> e o piso do local.
					</p>
					<p>Todo esse conjunto (lâminas e soleira) deslizam no interior de
						Guias (ou Trilhos Laterais) que são colocadas de ambos os lados
						para assegurar que essa Cortina de Lâminas se mantenha no lugar
						determinado.</p>
					<p>
						As <strong>Portas de Enrolar Automáticas</strong> contam ainda com
						um potente Motor Elétrico (automatizador) que é responsável por
						fazer todo o esforço de abertura e fechamento. Através de um
						movimento de rotação que é aplicado à um eixo (no qual a primeira
						lâmina da cortina de lâminas é fixada), gira esse Eixo e faz com
						que todo o conjunto de Lâminas se enrole, levantando ou abaixando
						a porta.
					</p>
					<br>
				</div>
				
				
				
				<div class="col-md-6 col-xs-12 col-sm-12">
					<br>
					<h2 style="margin-top: 20px;">E como Funciona a Porta de Enrolar
						Automática?</h2>

					<p>
						Simples! Essa é a palavra que define o seu funcionamento! A <strong>Porta
						de Aço Automática</strong> pode ser controlada apenas com um
						botão, você não precisa mais ter que se deslocar, abrir
						fechaduras, cadeados ou travas e ainda fazer um enorme esforço
						para abrir e fecha a sua porta (esforços que podem até em alguns
						casos causar lesões sérias), o que é excelente pois elimina todos
						os esforços desnecessários. </p>
						<p>
						Tecnicamente, como dissemos anteriormente, as <strong>Portas de Enrolar</strong> Automáticas
						exigem apenas os Trilhos Laterais para que possam deslizarem e um
						Automatizador especialmente desenvolvido para essa aplicação.
						</p>
						<p>
						Fizemos um Detalhamento Especial de Toda a montagem e o
						funcionamento de um <strong>Porta de Enrolar Automática</strong>
						em um Vídeo Curso Completo, que disponibilizamos de forma
						Totalmente Gratuita em nosso canal no <a href="https://www.youtube.com/watch?v=QkFo3OBff9c" Style="color:red;">Youtube</a>, veja, curta e
						compartilhe quantas vezes quiser, acabe com todas as suas dúvidas
						com esse conhecimento.
					</p>
				</div>
			
                 <div class="col-sm-6 col-md-6 col-xl-6" style="margin-top: 20px;">
					<video width="100%" muted loop autoplay><source
						src="video/porta-feicon.mp4" type="video/mp4"></video>

				</div>
				<div class="col-md-12 col-xs-12 col-sm-12">
					<br>
					<h2 style="margin-top: 20px;">Quais os Tipos de Porta de Enrolar
						Automática?</h2>
					<p>
						As <strong>Portas de Enrolar Automáticas</strong> da Original
						Portas podem ser desenvolvidas de acordo com as necessidades de
						cada Cliente, conforme o local, obra ou projeto arquitetônico. Com
						Lâminas Diversificadas, conseguimos atender desde as mais variadas
						até mesmo necessidades específicas, por exemplo, é possível fazer
						uma porta totalmente com Lâminas Fechadas (meia cana) mas possuir
						alguns vãos com Lâminas Micro Perfuradas (transvision) para
						ventilação ou para conseguir a visualização interna do
						estabelecimento, podendo também ser o inverso, uma porta
						totalmente transvision com algumas Lâminas Fechadas para Bloquear
						a Visão do Interior ou até uma determinada altura da porta.
						Antigamente as pessoas buscavam portões manuais, robustos, pesados
						e elaborados em diversos metais (e isso ainda acontece), contudo,
						a <strong>Porta de Enrolar Automática</strong> vem ganhado cada
						vez mais espaço e muitas vezes até substituindo estes antigos
						portões. </p>
						<p>
						O aço é um material extremamente resistente, e em uma
						tentativa de invasão ou arrombamento, faz o papel de uma parede
						protetora de alta resistência. </p>
						<p>
						As <strong>Portas de Enrolar</strong>
						Automáticas são comumente usadas em Comércios, Indústrias e também
						em Residências, geralmente são definidas e especificadas de acordo
						com o uso pretendido e sua aplicação. Dentre uma infinidade de
						Tipos e Modelos existentes, Automáticas ou Manuais, destacamos os
						mais comumente utilizados e conhecidos no mercado:
					</p>
				</div>

			<div class="col-md-12 col-xs-12 col-sm-4">
				<h2 style="margin-top: 40px;">Porta Automática de Enrolar – COMERCIAL</h2>
				<p>
					Nossas Portas Comerciais possuem todo o diferencial da Original
					Portas, onde aliamos toda nossa experiência com altíssima
					qualidade, garantias comprovadas através da Certificação ISO 9001 e
					InMetro. Atendem com perfeição a mais variada gama de Comércios
					como por exemplo: Lojas de Rua, Bares, Restaurantes, Lanchonetes,
					Estádios, Galerias, Bancas de Jornal, inclusive Lojas de Shoppings
					Centers, que possuem um alto padrão de qualidade e estética,
					deixando sua fachada mais apresentável aos seus clientes.
					</p>
					<p>
					A Maioria dos Estabelecimento Comerciais fazem uso de Lâminas Fechadas (Meia
					Cana), confeccionadas em Aço são laminadas em uma máquina especial
					chamada perfiladeira, o que garante a sua superfície Lisa, sem
					rugosidade, o que garante um aspecto uniforme e elegante para sua
					porta. 
					</p>
					<p>
					Outro modelo fantástico, que inclusive é o preferido dos
					Comerciantes, Lojas de Shoppings, Showroons de Vendas,
					Concessionárias e todos que necessitem de um fechamento
					eficientemente seguro e com total visibilidade para divulgação de
					produtos ou marca, chama-se Transvision. 
					<br>
					<a href="porta-enrolar-aco-comercial">
					<img  alt="porta-enrolar-aco-comercial" src="imagens\porta_comercial\porta-comercial.png" width="100%"
						height="auto" style="margin-top: 20px;"></a>
						
						<br>
					</p>	
					<p>
					As <strong>Portas de Enrolar Automáticas Transvision </strong>conseguem garantir toda
					essa proteção permitindo que sua vitrine seja observada, gerando
					infinitas possibilidades de negócios, já que pode divulgar seus
					produtos até mesmo quando está de portas fechadas! Através de muito
					estudo e tecnologia, foi criado um modelo de lâminas micro
					perfuradas que mantém toda sua resistência e segurança, permitindo
					a visualização interior dos ambientes.
					</p>
					<p>
					As <strong>Portas de Enrolar
					Automáticas </strong>são a opção mais utilizadas nos comércios,
					principalmente em grandes cidades. Isso porque ela garante mais
					praticidade e segurança, evitando também Esforços Físicos
					Desnecessários, para Abertura e Fechamento da porta (o que pode
					ocasionar Lesões Sérias), juntamente com outra vantagem essencial
					de NÃO usar Graxa em suas Guias, prevenindo que por algum acidente
					algum Cliente ou Funcionário se suje em sua Porta.
				</p>
				<br>
			</div>
				



			<div class="col-md-12 col-xs-12 col-sm-4">
				<br>
				<h2 style="margin-top: 20px;">Porta Automática de Enrolar –
					INDUSTRIAL.</h2>

				<p>
					Em nossas Portas Industriais, utilizamos o que há de mais avançado
					em qualidade e tecnologia, buscando excelência para garantir
					segurança e muita praticidade para sua empresa. 
					</p>
					<p>
					Todas as nossas <strong>Portas
					de Enrolar</strong> Automáticas são confeccionadas sob medida,
					para atender exatamente às mais específicas necessidades,
					independentemente do tamanho, pois é possível manter uma abertura
					com um vão livre de colunas ou divisões de até 15 Metros! Todas as
					Portas da linha de produção Original Portas, são confeccionadas com
					Bobinas de Aço com Alta Qualidade, tudo isso para lhe garantir
					Máxima Resistência e Total Durabilidade. 
					</p>
					<p>
					As <strong>Portas de
					Enrolar</strong> Industriais são altamente requisitadas para
					ambientes de grande porte como por exemplo podemos citar Galpões
					Industriais, Fábricas, Centros de Distribuição, Hangares, Usinas,
					Projetos de Construtoras, enfim, locais que demandam alto fluxo de
					aberturas e fechamentos, projetos especiais ou grande quantidade de
					<strong>Portas Automáticas</strong> que necessitam Alto Padrão de
					Qualidade. 
					
					<br>
					<a href="porta-enrolar-aco-industrial">
					<img  alt="porta-enrolar-aco-industrial" src="imagens\porta_industrial\ORIGINAL-industrial.png" width="100%"
						height="auto" style="margin-top: 20px;"></a>
					<br>
					</p>
					<p>
					A maior Porta Industrial Automática que produzimos e
					instalamos até o momento em toda na América Latina, foi no Galpão
					da Fábrica de Alimentos Marilan, mesmo quando diversas empresas que
					se diziam especializadas em <strong>Portas de Aço</strong>
					recusaram o trabalho, a Original Portas foi a única que aceitou
					esse desafio, e com toda nossa experiência aliada a qualidade que
					nos conferiu a Certificação ISO 9001, fabricarmos uma a uma todas
					peças e lâminas que foram utilizadas e ainda fizemos questão de
					realizar toda a instalação desta Magnífica Porta Industrial que
					possuí mais de 100 metros quadrados, o que nos deu muito Orgulho e
					Satisfação.
				</p>
			</div>

			<div class="col-md-12 col-xs-12 col-sm-4">
				<br>
				<h2 style="margin-top: 20px;">Porta Automática de Enrolar –
					RESIDENCIAL.</h2>
				<p>
					Ao contrário do que muitos pensam, as <strong>Portas de Enrolar</strong>
					Automáticas não são instaladas apenas em Comércios, Lojas, fábricas
					e Indústrias. 
					</p>
					<p>
					Com uma grande diversidade de Cores e Acabamentos
					gerando praticamente infinitos Modelos, as <strong>Portas de
					Enrolar</strong> Automáticas estão cada vez mais presentes em
					Residências e Condomínios, proporcionando mais comodidade e
					praticidade. 
					</p>
					<p>
					Por utilizar acionamento Automático por Controle
					Remoto, os usuários realizam a abertura e o fechamento sem a
					necessidade de sair do seu veículo, sabendo que sua casa ou
					condomínio ficará mais seguro, pois utilizamos Lâminas de Aço
					feitas sob medida o que garante o encaixe preciso e o fechamento
					perfeito. 
					
					<br>
					<a href="porta-enrolar-aco-residencial">
					<img alt="porta-enrolar-aco-residencial" src="imagens\porta_residencial\porta_residencial.jpg" width="100%"
						height="auto" style="margin-top: 20px;"></a>
					<br>
					</p>
					<p>
					Também é possível reduzir a temperatura do seu ambiente
					caso opte pelas Lâminas Transvision, por serem Micro Perfuradas
					possibilitam a passagem do ar, que circula livremente deixando o
					local mais arejado. Existem uma série de acessórios que podem ser
					instalados juntamente com sua <strong>Porta de Enrolar automática</strong>
					como Sensores, Controles, Travas e é justamente dos itens
					obrigatórios e opcionais que falaremos a seguir.
				</p>
			</div>
			<div class="col-md-6 col-xs-12 col-sm-12">
				<br>
				<h2 style="margin-top: 20px;">O que é Importante em uma Porta de
					Enrolar Automática?</h2>
				<p>
					A Original Portas se orgulha de compartilhar que possuímos padrões
					obrigatório de produção, o que já é a primeira garantia de um
					produto de qualidade, além da identificação de todos os itens que
					serão produzidos, eles são também separados por códigos e tipos,
					fazemos esse processo seletivo e cuidadoso de produção para
					garantir que a sua porta obtenha máxima qualidade. Após a produção
					e inspeção, todos os nossos produtos passam por um processo de
					conferência, onde são embalados conforme as especificações e
					etiquetados com o Nome do Cliente, eliminando qualquer
					possibilidade de erro. São itens Obrigatórios em uma <strong>Porta
					de Enrolar Automática:</strong><br>
					<br> - Eixo com as Testeiras;<br> 
					- Automatizador;<br> 
					- Guias ou Trilhos Laterais;<br> 
					- Lâminas; <br> 
					- Soleira.<br> <br> 
					</p>
					</div>
								
			<div class="col-md-6 col-xs-12 col-sm-6">
			   <img alt="porta-enrolar-aco-residencial" src="imagens\kit-original-portas.png" width="100%"
						height="auto" style="margin-top: 80px;">
						</div>
						<div class="col-md-6 col-xs-12 col-sm-6">
						<a href="acessorios">
			   <img alt="opcionais-original-portas" src="imagens\acessorios\acessorios.png" width="100%"
						height="auto" style="margin-top: 20px;"></a>
						</div>
					
				
			<div class="col-md-6 col-xs-12 col-sm-6">
					
					<p>Existem ainda vários itens opcionais e acessórios que podem integrar a sua
					Porta que são incluídos individualmente, esses itens visam aumentar
					o conforto, a segurança e a durabilidade de sua porta, podemos
					destacar:<br>
					<br> 
					- Trava Anti-queda;<br> 
					- Borracha de Vedação para Soleira;<br>
					- Fita Auto Lubrificante;<br> 
					- Central de Controle Remoto;<br> 
					- Controles Avulsos;<br> 
					- Aparelhos Nobreak;<br> 
					- Sensores Infravermelho;<br> 
					- Trava Lâminas, entre outros. <br>
					<br> Listamos a maioria dos itens individualmente, pois tendo uma
					grande gama de informações, você terá um parâmetro para notar
					diferenças e identificar irregularidades, afim de ter a certeza que
					estar recebendo exatamente pelo que pagou.
				</p>
			</div>	
			
					
			</div>	
			<div class="col-md-12 col-xs-12 col-sm-12">
				<br>
				<h2 style="margin-top: 20px;">Quais as Vantagens da Porta de Enrolar
					Automática?</h2>
				<p>
					Todas as <strong>Portas de Enrolar Automáticas </strong> da
					Original Portas podem ser facilmente instaladas por empresas
					especializadas em portas de aço. 
					</p>
					<p>
					Como dissemos anteriormente, seu
					funcionamento é bastante simples, não requer grande conhecimento
					técnico, e afinal, para ser acionada é preciso apenas pressionar um
					botão. 
					</p>
					<p>
					As <strong>Portas de Enrolar Automáticas </strong>são
					extremamente seguras porque mesmo sendo forçada, sua abertura não é
					facilitada – ela só abre quando o botão é pressionado, pois o
					próprio Automatizador realiza o travamento da porta. Talvez um dos
					maiores benefícios das <strong>Portas Automáticas</strong> está no
					seu sistema de rolo de abertura vertical, o que possibilita a sua
					natureza compacta. 
					</p>
					<p>
					Quando está totalmente aberta a cortina
					normalmente ocupa um espaço entre 30 e 50 cm, dependendo da altura
					total da Porta Automática. As <strong>Portas de Enrolar Automáticas</strong>
					possuem obrigatoriamente um Motor Elétrico, o que elimina todo o
					esforço físico desnecessário para abri-la ou fecha-la, e mesmo em
					casos de emergência como falta de energia, os Motores possuem um
					sistema de talha (corrente manual) que resolve o problema para
					abertura manual. 
					</p>
					<p>
					Outra grande vantagem é que o trajeto da Porta
					pode ser interrompido a qualquer momento através do acionador ou
					controle remoto, sendo útil também nos casos em que não há
					necessidade de abrir a porta totalmente por inteiro consegue-se
					fazer abertura apenas parcial.
				</p>
			</div>

			<div class="col-md-12 col-xs-12 col-sm-12">
				<br>
				<h2 style="margin-top: 20px;">A Porta de Enrolar Automática possui
					Manutenção?</h2>
				<p>
					Como todo equipamento mecânico a <strong>Porta de Enrolar
						Automática</strong>, bem como os seus componentes também
					necessitam de certos cuidados para manter o seu perfeito estado de
					funcionamento, em geral diríamos que se trata mais de zelo e
					limpeza do que manutenção propriamente dita. Digamos que para
					conservar o motor você precisará tomar alguns cuidados, o que
					inclui a limpeza regular da sua porta e o interior dos trilhos,
					para garantir que resíduos não grudem na superfície, ou prejudique
					a flexão e deslocamento de sua porta, que acabaria sobrecarregando
					o motor. 
					</p><div class="col-md-12 col-xs-12 col-sm-12">
  <img alt="manutenção-porta-enrolar" src="imagens\serra_img.png" width="100%"
						height="auto" style="margin-bottom: 10px;"></div>
					<p>
					Quando pequenos objetos aderem à porta ou o interior das
					Guias, eles podem aumentar o atrito prejudicando a abertura e
					fechamento, fazendo com que o motor de sua <strong>Porta de Enrolar</strong>
					fique sobrecarregado e quebre com frequência. 
					</p>
					<p>
					É essencial lembrar
					sempre de que as Guias deve estar APENAS LIMPAS, as <strong>Portas
					de Enrolar Automáticas</strong> NÃO USAM GRAXAS, as Lâminas devem
					correr livremente no interior das Guias (em casos específicos em
					que a dobra do perfil das lâminas tenha ficado muito justas é
					aconselhado o uso de vaselina industrial para lubrificação). Além
					desses pontos ainda indicamos a pintura eletrostática das Lâminas,
					pois muito além da beleza estética, este tipo de pintura irá elevar
					ao máximo a proteção das Lâminas de sua Porta. 
					</p>
					<p>
					A nossa Pintura
					Eletrostática age como uma barreira, evitando, por exemplo, a
					corrosão. Mas, é fundamental que essa pintura seja feita por uma
					empresa especializada como a Original Portas, empresa garantida
					pelo ISO 9001 de Qualidade. É importante ressaltar que a pintura
					ocorre apenas nas Lâminas, Testeiras e Guias, sendo que o motor de
					sua <strong>Porta de Enrolar Automática</strong> deve permanecerá
					na sua cor original. Mas não se preocupe, como ele é instalado de
					forma discreta, e todo esse cuidado serve para não afetar o design
					de sua <strong>Porta de Enrolar</strong>, muito menos o design do
					seu Negócio.
				</p>
			</div>





			<div class="col-md-12 col-xs-12 col-sm-12">
				<br>
				<h2 style="margin-top: 20px;">Qual o Custo de Porta de Enrolar
					Automática?</h2>
				<p>
					Quando falamos em custo nossa mente imagina logo o valor monetário
					das coisas, e não leva em consideração os benefícios que um produto
					de extrema qualidade pode nos gerar. 
					</p>
					<p>
					Os custos variam bastante
					conforme o modelo da porta, o local de instalação, tipo e aplicação
					para o qual ela será utilizada e após esses dados ainda é calculado
					os valores em relação a metragem de sua porta. Por todos esses
					detalhes de extrema importância para realização de uma porta de
					Alta qualidade, que nós da Original possuímos Técnicos Treinados e
					Capacitados para Realizar as Medições e Orçamentos, para garantir
					exatamente o valor real de sua <strong>Porta de Enrolar Automática</strong>.
					Mas, se levarmos em conta o quanto vale a nossa saúde através de
					todos os esforços desnecessários que não iremos mais realizar; se
					pensarmos em todos os dias de chuva que não precisaremos mais
					descer de nossos veículos para abrir cadeados, trancas e portões
					complicados; e se ainda considerarmos o aumento da segurança e
					comodidade que teremos em nosso comércio, indústria ou residência
					fará com que os custos de uma <strong>Porta de Enrolar Automática</strong>
					sejam extremamente baixos. Mas é de extrema importância termos
					consciência de investir em um produto de alta qualidade e ter a
					garantia de que será realizada uma instalação correta. Também é
					preciso seguir os cuidados com a conservação de sua porta que
					apresentamos anteriormente. 
					</p>
					<p>
					Com isso, você estará garantindo um
					investimento bastante vantajoso.
				</p>
			</div>

			<div class="col-md-6 col-xs-6 col-sm-12">
				<br>
				<h2 style="margin-top: 20px;">A Porta de Enrolar Automática possui
					Alguma Desvantagem?</h2>
				<p>
					Levando em conta todas as considerações e observações que fizemos
					ao longo desta página, chegaremos à conclusão de que a <strong>Porta
					de Enrolar Automática </strong>não apresenta “Desvantagens”
					propriamente ditas. 
					</p>
					<p>
					Ela necessita de cuidados! Isso desde a compra
					até as manutenções periódicas de limpeza que deverão ser feitas
					após a instalação. 
					</p>
					<p>
					É claro que se um automóvel em alta velocidade
					se chocar contra ela irá arrobar, mas isso não pode ser considerado
					como uma falha de segurança. Da mesma forma que se o fabricante não
					for confiável e não usar os componentes corretos, sua porta irá
					apresentar uma série de defeitos e quebras, mas que são decorrentes
					de erro de fabricação, componentes não recomendados e sem
					qualidade.
				</p></div>
<div class="col-md-6 col-xs-6 col-sm-12">
  <img alt="desvantagem-porta-enrolarl" src="imagens\boneco-interrogacao.jpg" width="50%"
						height="auto" style="margin-left: 100px;"></div>

			<div class="col-md-12 col-xs-12 col-sm-12">
				<br>
				<h2 style="margin-top: 20px;">
					Qualquer Empresa pode Comercializar Portas de Enrolar
						Automáticas?
				</h2>
				<p>NÃO! A venda desse tipo de equipamento deve ser realizada APENAS
					por empresas especializadas na prestação desse tipo de serviço. A
					Original Portas é uma empresa sólida no mercado a mais de 10 anos
					que trabalha com portas desenvolvidas nas medidas exatas dos nosso
					Clientes. Oferecemos ainda o serviço de instalação com Técnicos
					Altamente Capacitados, muitos Formados inclusive em nossa própria
					fábrica, onde damos todo treinamento necessário, para que seja
					realizado um excelente trabalho para os nossos Clientes. Desde o
					início nos mantemos em constante preocupação com a qualidade dos
					produtos e serviços prestados, garantia comprovada através das
					Certificações ISO 9001 e InMetro. 
					</p>
					
					<p>
					Trabalhamos sabendo que a
					Segurança e a Satisfação dos nossos clientes deve estar sempre em
					primeiro lugar, e acreditamos que não somos apenas fabricantes de
					portas, aqui na Original, temos a certeza de que participamos da
					concretização de projetos e da realização de sonhos. 
					</p>
					<p>
					Mas, na dúvida
					antes de contratar um serviço, vale a pena conferir o histórico da
					empresa, a avaliação de outros clientes, enfim, referencias assim
					permitem que você entenda se a empresa presta um bom serviço e se
					outros clientes já tiveram problemas com ela ou não. É a melhor
					forma de garantir que você fará um Excelente Negócio!</p>
			</div>			
			
		</div>
	</div>
	
	<br>
	<br>
<?php echo $footer;?>
</body>
</html>