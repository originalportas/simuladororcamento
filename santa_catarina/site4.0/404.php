<?php require 'main.php';
require 'footer.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>ERRO 404 - Original Portas</title>
<base>
<meta name="description"
	content="Original portas a maior fabrica de portas do Brasil">
<meta name="keywords"
	content="Original Portas, portas, portas de aço, portas aço automáticas, portas de enrolar, portinhola, alçapão, automatizadores, controle portão. ">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="404.php">
<meta name="author" content="Original Portas">
<link rel="shortcut icon" href="/site4.0/imagens/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title" content="ERRO 404 - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url" content="404.php">
<meta property="og:description" content="Original portas a maior fabrica de portas do Brasil">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="/site4.0/bootstrap/css/bootstrap.css"type="text/css" />
<link href="/site4.0/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/site4.0/bootstrap/css/beta.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="/site4.0/css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- -----------------JS----------------- -->
<script type="text/javascript" src="/site4.0/js/jquery-1.7.2.min.js" ></script>
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="/site4.0/js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="/site4.0/js/jquery.slicknav.js"></script>
<script defer src="/site4.0/js/geral.js"></script> 
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
   <div class="erro404">
     <div class="row">
		  <div id="log404" class="col-sm-12	 col-md-6">
		   <img  id="img404" alt="erro404" src="/site4.0/imagens/erro404.png"></div>
		<div id="log404" class="col-sm-12 col-md-6">
		<h1>Erro 404: Página não encontrada</h1>
		<h3>Ops! Página não encontrada. </h3>
		<p>Tente acessar novamente ou navegue em outras páginas do nosso site.</p>
        </div>
		    <script type="text/javascript">
			  var GOOG_FIXURL_LANG = 'pt-BR';
			  var GOOG_FIXURL_SITE = 'index.php'
			</script>
			<script type="text/javascript"
			  src="http://linkhelp.clients.google.com/tbproxy/lh/wm/fixurl.js;"></script>
     </div>
  </div>
</div>
<?php echo $footer;?>
</body>
</html>