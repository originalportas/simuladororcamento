<?php require 'main.php'; 
require 'footer.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Nossa Empresa - Original Portas</title>
<base>
<meta name="description"
	content="Original Portas - Há 10 anos a original portas vem conquistando a confiança e o carinho de seus clientes,nos tornamos referência no segmento de portas de aço automáticas para indústrias, comércios e residências">
<meta name="keywords"
	content="Original Portas, portas, portas de aço, portas aço automáticas, portas de enrolar, portinhola, alçapão, automatizadores, controle portão. ">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="quem_somos.php">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" href="/site4.0/imagens/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title" content="Quem Somos - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url" content="quem_somos.php">
<meta property="og:description" content="Há 10 anos a original portas vem conquistando a confiança e o carinho de seus clientes,nos tornamos referência no segmento de portas de aço automáticas para indústrias,comércios e residências">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="/site4.0/bootstrap/css/bootstrap.css"type="text/css" />
<link href="/site4.0/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/site4.0/bootstrap/css/beta.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="/site4.0/css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script> 
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="/site4.0/js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="/site4.0/js/jquery.slicknav.js"></script>
<script defer src="/site4.0/js/geral.js"></script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="embed-container"> 
  	<iframe src="https://www.youtube.com/embed/nmbJVI13cKY?autoplay=1;rel=0&amp;showinfo=0&amp;start=2" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
</div>

<div class="container">
  <div class="text_empresa">
	<div id="row_emp" class="row">
      <div id="bar_emp" class="col-md-12">
      <br>
       <h1>Original Portas de Aço automáticas</h1>
       <hr style="width: auto; height: 2px; background-color: #ccc;">
       <br></div>
	   <div class="col-md-12">
           <img alt="10anos de Original" id="logo_antigo"
						src="/site4.0/imagens/logo_antigo1.png" width="220px"
						height="100px">
						<img alt="10anos de Original" id="logo_antigo"
						src="/site4.0/imagens/logo_antigo2.png" width="220px"
						height="100px">
						<img alt="10anos de Original" id="logo_antigo"
						src="/site4.0/imagens/logo_antigo3.png" width="220px"
						height="100px"> <br> <br>
					<p>Há 10 anos a original portas vem conquistando a
						confiança e o carinho de seus clientes. Essa parceria foi
						conquistada não apenas pela nossa forma única de
						atendimento, mas pela qualidade nossos produtos, somos
						reconhecidos por distribuir produtos da mais alta qualidade e
						durabilidade. Assim, mesmo atendendo um público bem
						exigente, nos tornamos referência no segmento de portas de
						aço automáticas para indústrias,
						comércios e residências, em nível nacional e internacional.</p>
					<br />
					<p>Fomos a primeira fábrica da América Latina, há
						produzir uma porta de aço com mais de 10 metros de altura por 10 metros de largura,
						instalada na fábrica de alimentos Marilan.</p>
					<p>Somos uma empresa completa: contamos com uma ampla gama de
						portas de aço , vidro e Lona, produtos feitos sob medida para atender a cada
						necessidade.</p>
                </div>
				<br />
			</div>
			<div id="row_emp" class="row">
				<br />
				<div class="col-md-12">
					<h1>Política de Qualidade</h1>
					 <p>Atender as necessidades e expectativas dos clientes, buscando a
						melhoria contínua de nossos processos com a prática
						do sistema de gestão da qualidade e atender aos requisitos
						aplicáveis.</p>
					<p>Para assegurar esta politica, contamos com as normas do ISO 9001
						(SVG), selo que garante qualidade no nosso processo de
						fabricação.</p>
					<br />
				</div>
                <div class="col-md-12">
						<h2>
							<img alt="" src="/site4.0/imagens/icones/arrow-right.png"
								style="width: 12px;"> Nossa Missão
						</h2>
						<p>Ser uma referência nacional no mercado industrial,
							comercial e residencial. Conquistar nossos clientes com foco na
							qualidade e inovação de atendimento, pessoas e
							conhecimento.</p>
						<br />
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
						<h2>
							<img alt="" src="/site4.0/imagens/icones/arrow-right.png"
								style="width: 12px;"> Nossos
							Objtivos
						</h2>
						<p>Prestar toda assistência técnica e comercial aos
							nossos clientes, analisando individualmente as suas necessidades,
							oferecendo-lhes sempre as melhores opções e
							alternativas nas soluções de seus problemas.
							Acompanhar sempre o desenvolvimento de novos produtos e de novas
							soluções em nosso campo de atuação
							através da melhoria de nossos sistemas produtivos aumento
							da tecnologia e bem-estar de nossos colaboradores.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
							<h2>
								<img alt="" src="/site4.0/imagens/icones/arrow-right.png"
									style="width: 12px;"> Nossos
								Valores
							</h2>
							<p>Seriedade, transparência e respeito no trato com nossos
								parceiros, clientes e provedores externos, valorizando suas
								opiniões, e buscando continuamente o
								aperfeiçoamento em todas as fases de nossas atividades.</p>
							<br />
				</div>
			</div>
		</div>
</div>
<?php echo $footer;?>
</body>
</html>