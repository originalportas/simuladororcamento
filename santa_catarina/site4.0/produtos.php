<?php require 'main.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async
	src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script>
<script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');
</script>
<title>Contato - Original Portas</title>
<base>
<meta name="description"
	content="Na Original Portas você encontra tudo para Portas de Aço automática,portas de enrolar, porta para comércio,porta para loja , acessórios, automatizadores com nobreaks, botoeiras, portas de aço transvision, entre outros">
<meta name="keywords"
	content="Original Portas, portas, portas de aço, portas aço automáticas">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="index.php">
<meta name="author" content="Original Portas">
<link rel="shortcut icon" href="/site4.0/imagens/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title" content="Home - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url" content="index.php">
<meta property="og:description"
	content="Na Original Portas você encontra tudo para Portas de Aço automática,portas de enrolar, porta para comércio,porta para loja , acessórios, automatizadores com nobreaks, botoeiras, portas de aço transvision, entre outros">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 

</script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>

	<div class="container-fluid">
		<div class="prod_class">
			<div class="row">
				<div class="col-md-12">
					<h1>CONHEÇA NOSSOS PRODUTOS</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					<p>A Original Portas trabalha com diversos tipos de portas
						automáticas ou manuais.
					
					
					<p>Nossos produtos são fabricados com a mais alta tecnologia,
						garantindo a segurança de seu estabelecimento</p>
				</div>
			</div>
			<div class="col-md-12">
				<h2>PORTAS DE AÇO AUTOMÁTICAS E MANUAIS</h2>
				<hr>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<a href="porta-enrolar-aco-industrial.php"><img id="img_prod"
						src="imagens/produtos/Industrial_311x145.png"
						alt="PORTAS INDÚSTRIAIS"></a>
					<div class="caption">
						<h3>PORTA DE ENROLAR INDUSTRIAL</h3>
						<p>Industrias, Docas, Hangares, Galpões.</p>
						<br>
						<p>
							<a href="orcamento-porta-enrolar-automatica.php"
								class="btn btn-primary" role="button">Consulte</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<a href="porta-enrolar-aco-comercial.php"> <img id="img_prod"
						src="imagens/produtos/porta_comercial_311x145.png"
						alt="PORTAS PARA COMERCIOS"></a>
					<div class="caption">
						<h3>PORTA DE ENROLAR COMERCIAL</h3>
						<p>Shoppings, Lojas, Containers, Galerias, Quiosques.</p>
						<br>
						<p>
							<a href="orcamento-porta-enrolar-automatica.php"
								class="btn btn-primary" role="button">Consulte</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<a href="porta-enrolar-aco-residencial.php"><img id="img_prod"
						src="imagens/produtos/porta_residencial_311x145.png"
						alt="PORTAS PARA RESIDÊNCIAS"></a>
					<div class="caption">
						<h3>PORTA DE ENROLAR RESIDENCIAL</h3>
						<p>Residências e condominios.</p>
						<br>
						<p>
							<a href="orcamento-porta-enrolar-automatica.php"
								class="btn btn-primary" role="button">Consulte</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<h2>PORTAS SECCIONADAS</h2>
				<hr>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<a href="porta-seccionada-residencial.php"><img id="img_prod"
						src="imagens/produtos/porta-seccionada_residencial_311x145.png"
						alt="PORTA AMERICANA SECCIONAL"></a>
					<div class="caption">
						<h3>PORTA SECCIONADA RESIDENCIAL</h3>
						<p>Utilizada em Residencias e Condominios ,pode ser usada como
							porta de Garagem ou Social</p>
						<br>
						<p>
							<a href="orcamento-porta-enrolar-automatica.php"
								class="btn btn-primary" role="button">Consulte</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<a href="porta-seccionada-industrial.php"><img id="img_prod"
						src="imagens/produtos/porta-seccionada_industrial_311x145.png"
						alt="PORTA AMERICANA SECCIONAL"></a>
					<div class="caption">
						<h3>PORTA SECCIONADA INDUSTRIAL</h3>
						<p>Industrias, Fabricas, Hangares, Docas.</p>
						<br> <br>
						<p>
							<a href="orcamento-porta-enrolar-automatica.php"
								class="btn btn-primary" role="button">Consulte</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<h2>PORTAS RÁPIDAS DE LONA</h2>
				<hr>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<a href="porta-rapida-enrolar.php"><img id="img_prod"
						src="imagens/produtos/porta_rapida_enrolar_311x145.jpg"
						alt="PORTA RAPIDA DE ENROLAR"></a>
					<div class="caption">
						<h3>PORTA RÁPIDA ENROLAVEL</h3>
						<p>Utilizada em areas que necessitem de pouco contato externo com
							isolação contra contaminações e pragas ,por ser lavavél o
							material ele facilita a esterização</p>
						<br>
						<p>
							<a href="orcamento-porta-enrolar-automatica.php"
								class="btn btn-primary" role="button">Consulte</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<a href="porta-rapida-dobravel.php"><img id="img_prod"
						src="imagens/produtos/porta_rapida_dobravel_311x145.png"
						alt="PORTA RÁPIDA DOBRAVEL"></a>
					<div class="caption">
						<h3>PORTA RÁPIDA DOBRAVÉL</h3>
						<p>A porta rapida dobravél contém os mesmos beneficios que a porta
							rápida enrolavél, a unica diferença é a forma de abertura.</p>
						<br>
						<p>
							<a href="orcamento-porta-enrolar-automatica.php"
								class="btn btn-primary" role="button">Consulte</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<h2>Portas de Vidro Autmáticas</h2>
				<hr>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<a href="porta-rapida-enrolar.php"><img id="img_prod"
						src="imagens/produtos/porta_rapida_enrolar_311x145.jpg"
						alt="PORTA RAPIDA DE ENROLAR"></a>
					<div class="caption">
						<h3>PORTA DE VIDRO</h3>
						<p>Este tipo de porta é muito comum em locais com o fluxo grande
							de pessoas, como por exemplo: farmácias, mercados, shoppings</p>
						<br>
						<p>
							<a href="orcamento-porta-enrolar-automatica.php"
								class="btn btn-primary" role="button">Consulte</a>
						</p>
					</div>
				</div>
			</div>










		</div>
	</div>
	<!-- MENU  MOBILE -->
	<script defer type="text/javascript"
		src="/site4.0/js/jquery.slicknav.js"></script>
	<!-- /MENU  MOBILE -->

	<div class="linha_vertical-2">
		<hr>
	</div>
	<footer>
	<div class="footer" id="footer">
		<div class="container">
			<div class="row" style="background-color:;">
				<div class="col-lg-3  col-md-4 col-sm-4 col-xs-3">
					<h3>Seja Original</h3>
					<ul>
						<li><a href="index.php"> Pagina Inicial</a></li>
						<li><a href="quem_somos.php"> Quem Somos</a></li>
						<li><a href="produtos.php"> Produtos </a></li>
						<li><a href="https://goo.gl/photos/YTenbCXuHDhy9i97A"
							target="_blank" title="Galeria Original Portas"> Galeia</a></li>
						<li><a href="video.php"> Videos</a></li>
						<li><a href="contatos.php">Atendimento</a></li>
					</ul>
				</div>
				<div class="col-lg-3  col-md-4 col-sm-4 col-xs-3">
					<h3>Produtos</h3>
					<ul>
						<li><a href="porta_industrial.php"> Porta industrial </a></li>
						<li><a href="porta_comercial.php">Porta Comercial </a></li>
						<li><a href="porta_residencial.php">Porta Residencial</a></li>
						<li><a href="porta_rapida.php"> Porta Rápida </a></li>
						<li><a href="porta_vidro.php"> Porta de Vidro </a></li>
						<li><a href="acessorios.php"> Acessórios </a></li>

					</ul>
				</div>
				<!--  div class="col-lg-2  col-md-2 col-sm-4 col-xs-6">
                    <h3> Acessórios </h3>
                    <ul>
                        <li> <a href="acessorios.php"> Trava Laminas </a> </li>
                        <li> <a href="acessorios.php"> Central p/ motor  </a> </li>
                        <li> <a href="acessorios.php"> Controle Remoto </a> </li>
                        <li> <a href="acessorios.php"> Nobreak </a> </li>
                        <li> <a href="acessorios.php"> Sensor infravermelho </a> </li>
                        <li> <a href="acessorios.php"> Fechaduras </a> </li>
                    </ul>
                </div -->
				<div class="col-lg-3  col-md-4 col-sm-4 col-xs-3">
					<h3>Atendimento</h3>
					<ul>
						<li><a href="contatos.php"> Contato </a></li>
						<li><a href="orcamento.php"> Orçamento </a></li>
						<li><a href="localizacao.php"> Localização </a></li>
						<!-- li> <a href="sej_rev.php"> Seja Revendedor </a> </li>
                        <li> <a href="trab_con.php"> Trabalhe Conosco </a> </li -->
					</ul>
				</div>
				<div class="col-lg-3  col-md-12 col-sm-12 col-xs-3 ">
					<h3>Redes Socias</h3>
					<ul class="social">
						<li><a href="https://pt-br.facebook.com/originalportas/"><i
								class="fa fa-facebook" title="facebook"></i></a></li>
						<!-- li><a href="#"> <i class="fa fa-twitter" title="twitter"></i></a></li-->
						<li><a
							href="https://plus.google.com/103786054530062695266?hl=pt_br"><i
								class="fa fa-google-plus" title="google-plus"></i></a></li>
						<!-- li><a href="#"> <i class="fa fa-pinterest" title="pinterest"></i></a></li-->
						<li><a
							href="https://www.youtube.com/channel/UCPINybml0zMMiR-c_XrsjHQ"><i
								class="fa fa-youtube" title="youtube"></i></a></li>
					</ul>
				</div>
			</div>
			<!--/.row-->
		</div>
		<!--/.container-->
	</div>
	<!--/.footer-->
	<h3 style="margin-left: 15px">Formas de pagamento</h3>
	<p style="margin: 0 15 0 15;">Cartões de crédito: (Visa, Master, Elo,
		Hipercard, Diners), Débito, Cartão Construcard (exceto bandeira
		"Elo"), Boleto Bancário, Cheque , BNDS, Deposito e Tranferencia
		Bancária.</p>
	<div class="footer-bottom">
		<div class="container">
			<p class="pull-left" style="text-align: center">Todos dos Direitos
				Reservados &copy; Original Portas de Aço Automáticas . (Lei 9610 de
				19/02/1998)</p>
		</div>
	</div>
	<p style="text-align: center; font-size: 12px;">
		<br />created by: tworock.desenv@gmail.com
	</p>
	<!--/.footer-bottom--> </footer>
	<script src="js/jquery-1.9.1.min.js"></script>
	<!-- -------------------Menu mobile------------------------------- -->
	<script type="text/javascript">$(window).load(function() {
	  $('.flexslider').flexslider({ 
		    animation: "slide"
		  });
		});</script>
	<script defer src="/site4.0/js/vendor/modernizr-2.6.2.min.js"></script>
	<!-- MENU  MOBILE -->
	<script defer type="text/javascript"
		src="/site4.0/js/jquery.slicknav.js"></script>
	<!-- /MENU  MOBILE -->
	<script defer src="../apis.google.com/js/plusone.js"></script>
	<script defer src="/site4.0/js/geral.js"></script>
</body>
</html>
