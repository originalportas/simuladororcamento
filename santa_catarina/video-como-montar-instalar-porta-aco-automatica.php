<?php require 'main.php';
require 'footer.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
<title>Videos de Portas de Enrolar - Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description"
	content="Assista aos nossos videos e tire a maioria de suas dúvidas, veja abaixo o passo a passo de como instalar uma porta de enrolar de aço automática">
<meta name="keywords"
	content="video portas de aço de enrolar, como montar uma porta de aço, instalação de laminas de aço, medidas de tubos e guias de afastamento, regulagem fim de curso, portinhola, alçapão, automatizadores,">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="video.php">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>

<meta name="author" content="TwoRock">
<meta property="og:region" content="Brasil">
<meta property="og:title" content="Videos Portas de aço - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url" content="video.php">
<meta property="og:description"
	content="Assista aos nossos videos e tire a maioria de suas dúvidas, veja abaixo o passo a passo de como instalar uma porta de enrolar de aço automática">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta-vid.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>



<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container">
		<div class="vid_class">
			<div class="row">
				<div class="col-md-12">

					<h1>VÍDEO - ORIGINAL PORTAS</h1>
					<p id="vid_p">ASSISTA AOS NOSSOS VIDEOS E TIRE A MAIORIA DE SUAS
						DÚVIDAS, VEJA ABAIXO O PASSO A PASSO DE COMO INSTALAR UMA PORTA DE
						AÇO AUTOMÁTICA</p>
					<br>
				</div>
				<br>
				<div>
					<h2>0.1 - Video Completo com 10 passos lhe ensinando a montar e instalar, portas de aço automática.</h2>
				</div>
				<div class="col-md-12">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="1060" height="300" class="embed-responsive-item"
							src="https://www.youtube-nocookie.com/embed/QkFo3OBff9c?rel=0&amp;showinfo=0&amp;start=11"></iframe>
					</div>
				</div>
			</div>
			
			<br>
			<div class="row">
			<div class="col-md-12">
			<div><h2>também é possível assistir a todos os videos por modúlos.</h2>
				</div>
			</div>
			</div>
			<div class="row">
			<div class="col-sm-4">
					<h3>1.1 - Nivelamento Portas Automáticas.</h3>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="auto" height="auto" class="embed-responsive-item"
							src="https://www.youtube.com/embed/c_eZTJwf_ZU?rel=0&amp;showinfo=0&amp;start=11"" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-4">
					<h3>1.2 - INSTALAÇÃO EIXO PORTAS AUTOMÁTICAS.</h3>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="auto" height="auto" class="embed-responsive-item"
							src="https://www.youtube.com/embed/MKzhD37PBhc?rel=0&amp;showinfo=0"
							frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-4">
					<h3>1.3 - INSTALAÇÃO DO AUTOMATIZADOR (MOTOR).</h3>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="auto" height="auto" class="embed-responsive-item"
							src="https://www.youtube.com/embed/kfXZUtNieuw?rel=0&amp;showinfo=0&amp;start=9"
							frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
			<div class="col-sm-4">
					<h3>1.4 - MEDIDAS DE GUIAS E TUBOS DE AFASTAMENTO.</h3>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="auto" height="auto" class="embed-responsive-item"
							src="https://www.youtube.com/embed/pQFOits6qcw?rel=0&amp;showinfo=0&amp;start=9"
							frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-4">
					<h3>1.5 - MONTAGEM DAS LAMINAS PORTAS DE AÇO.</h3>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="auto" height="auto" class="embed-responsive-item"
							src="https://www.youtube.com/embed/sh8clED-4NQ?rel=0&amp;showinfo=0&amp;start=9"
							frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-4">
					<h3>1.6 - ENCAIXE DO TRAVA LAMINAS.</h3>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="auto" height="auto" class="embed-responsive-item"
							src="https://www.youtube.com/embed/j7Mlk5smqTU?rel=0&amp;showinfo=0&amp;start=9"
							frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
			<div class="col-sm-4">
					<h3>1.7 - REGULAGEM DO FIM DE CURSO.</h3>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="auto" height="auto" class="embed-responsive-item"
							src="https://www.youtube.com/embed/wewg7NjzBBk?rel=0&amp;showinfo=0&amp;start=9"
							frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-4">
					<h3>1.8 - INSTALAÇÃO PORTINHOLA PORTA DE AÇO.</h3>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="auto" height="auto" class="embed-responsive-item"
							src="https://www.youtube.com/embed/d00KqMrus2Q?rel=0&amp;showinfo=0&amp;start=55"
							frameborder="0" allowfullscreen> frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-4">
					<h3>1.9 - INSTALAÇÃO ALÇAPÃO PORTA DE AÇO.</h3>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="auto" height="auto" class="embed-responsive-item"
							src="https://www.youtube.com/embed/-pznq9zt6mw?rel=0&amp;showinfo=0&amp;start=55"
							frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-4">
					
				</div>
			</div>
			<br>
			<hr>
			<br>
			<div class="row">
				<div class="col-sm-6">
					<h3>2.1 - CONHEÇA NOSSO KIT PARA PORTAS DE AÇO AUTOMÁTICAS</h3>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="auto" height="auto" class="embed-responsive-item"
							src="https://www.youtube.com/embed/BKI2Jejx4Dk?rel=0&amp;showinfo=0&amp;start=10"
							" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-6">
					<h3>3.1 - ASSISTA AO NOSSO VIDEO DE APRESENTÇÃO</h3>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="auto" height="auto" class="embed-responsive-item"
							src="https://www.youtube.com/embed/nmbJVI13cKY?rel=0&amp;showinfo=0&amp;start=10"
							" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
			<br>
			<hr>
			<br>
			<div class="row">
				<div class="col-sm-12" id="canal">
					<h2>
						Inscreva-se em nosso canal <a
							href="https://www.youtube.com/channel/UCPINybml0zMMiR-c_XrsjHQ">
							<i class="fa fa-youtube fa-2x" style="color: red;"
							aria-hidden="true"></i>
						</a>
					</h2>
				</div>
			</div>
		</div>
	</div>
	
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('554799663772', '554791235132'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	

<?php echo $footer;?>
</body>
</html>