<?php
require 'main.php';
require 'footer.php';
?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
<title>Portas Rápidas de Enrolar - Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description"
	content="A eficiência da produção, velocidade de processamento, higiene e
						armazenamento do frio são cruciais em todos os ambientes de
						processamento de alimentos. As portas rápidas de enrolar da Original
						Portas ajudam a atender esses requisitos. A cortina da porta de
						PVC é flexível e uniformemente texturizada, uma porta de alta
						velocidade e muito fácil de limpar. A porta de enrolar pode ser
						instalada com uma estrutura de aço galvanizado e postes laterais.
						As impurezas não se acumulam e torna-se possível uma boa higiene.">
<meta name="keywords"
	content="Porta Rápida e Enrolar, porta, rapida, enrolar,alta, velocidade, abertura, automática, sensor de movimento, aproximação, PVC, lona, plastico, fast rolling door">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="porta-rapida-enrolar">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Portas Rápidas de Enrolar - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image"
	content="imagens/porta_rapida/porta-rapida-enrolar.png">
<meta property="og:url" content="porta-rapida-enrolar">
<meta property="og:description"
	content="A eficiência da produção, velocidade de processamento, higiene e
						armazenamento do frio são cruciais em todos os ambientes de
						processamento de alimentos. As portas rápidas de enrolar da Original
						Portas ajudam a atender esses requisitos. A cortina da porta de
						PVC é flexível e uniformemente texturizada, uma porta de alta
						velocidade e muito fácil de limpar. A porta de enrolar pode ser
						instalada com uma estrutura de aço galvanizado e postes laterais.
						As impurezas não se acumulam e torna-se possível uma boa higiene.">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2">
				<img src="imagens/porta_rapida/porta-enrolar-11.png"
					width="100%" height="auto">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="class_aut2">
			<div class="row">
				<div class="col-md-12 col-xs-12">


					<h1 style="margin-top: -20px;">Portas Rápidas de Enrolar</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
<br>
							<div class="col-md-5 col-xs-12">

					<H2>RESPEITA OS REQUISITOS DE HIGIENE</H2>
					<p>A eficiência da produção, velocidade de processamento, higiene e
						armazenamento do frio são cruciais em todos os ambientes de
						processamento de alimentos. As portas rápidas de enrolar da Original
						Portas ajudam a atender esses requisitos. A cortina da porta de
						PVC é flexível e uniformemente texturizada, uma porta de alta
						velocidade e muito fácil de limpar. A porta de enrolar pode ser
						instalada com uma estrutura de aço galvanizado e postes laterais.
						As impurezas não se acumulam e torna-se possível uma boa higiene.</p>
<br>

					<H2>MANTÉM A BAIXAS TEMPERATURAS.</H2>
					<p>Porta rápida de enrolar abre e fecha muito rápido. O tempo que
						uma porta permanece aberta é limitado a poucos segundos. Isso
						evita flutuações de temperatura na corrente de ar frio,
						protegendo a qualidade dos alimentos. Uma vedação superior em
						torno do perímetro total da porta rápida de enrolar mantém a
						poeira, sujeira e insetos fora de suas áreas de armazenamento e
						processamento. Isso ajuda a evitar contaminações.</p>
				<br><br>
				</div>
					<div class="col-md-7 col-xs-12 col-xs-12">
						
						<div id="ban_eletro2">
				          <img src="imagens/porta_rapida/porta-rapida-enrolar-02.jpg" width="100%" height="auto" style="margin-top: 20px;">
			           </div>
			       </div>			
						<div class="div_rapid">
														<div class="col-md-12 col-xs-12">
													<hr style="width: auto; height: 2px; background-color: green;">
			
					<h2><i class="fa fa-check" style="color:green; font-size:30px;"></i> Benefícios</h2>
					
											</div>	
													<div class="col-md-4 col-xs-4">
							
							<p><br><Strong>• Respeita os requisitos de higiene.</strong><br> O tecido
								da porta rápida de enrolar é fácil de limpar e pode ser
								combinada com uma estrutura de aço inoxidável (opcional).
							</p><br>
							
							<p><br>
								<Strong>• Evita contaminação.</strong><br> ciclo rápido de
								funcionamento e com boa vedação, protegem da poeira, sujeira e
								parasitas.
							</p><br>
							</div>
														<div class="col-md-4 col-xs-4">
							
							<p><br>
								<Strong>• Economize energia.</strong><br> Temperaturas estáveis
								graças à alta velocidade de abertura e fechamento e vedação
								superior.
							</p><br>
							
							<p><br>
								<Strong>• Aumenta a eficiência da produção</strong><br> Otimização
								do fluxo de produção e aceleração no trafego.
							</p><br>
							</div>
														<div class="col-md-4 col-xs-4">
							
							<p><br>
								<Strong>• Redução de Custo</strong><br> Os desgastes e manutenção
								são limitados.
							</p><br>
							<p><br>
								<Strong>• Segurança para pessoas e equipamentos</strong><br>
								Possui tecido flexível sem elementos rígidos evita lesões e
								danos.
							</p><br>
				
				</div>
				
</div>
</div>
</div>
			</div>
													<hr style="width: auto; height: 2px; background-color: green;">
			
		</div>
	<br>
	<br>
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('554799663772', '554791235132'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>		

<?php echo $footer;?>
</body>
</html>