<?php
require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async
	src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script>
<script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Inscrição Curso - Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description"
	content="Envie sua mensagem através do formulario de orçamento e entraremos em contato com você, saiba o preço de portas de enrolar, portas de aço é caro? ,portas de enrolar é caro?">
<meta name="keywords"
	content="Original Portas, orçamento de portas e enrolar, preço portas de aço, portas aço automáticas, portas de enrolar, portinhola, alçapão, automatizadores, controle portão. tudo para portas de aço ">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="inscricao-curso">
<meta name="author" content="TwoRock">
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Inscrição para Cursos - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url" content="inscricao-curso">
<meta property="og:description"
	content="Original Portas, orçamento de portas e enrolar, preço portas de aço, portas aço automáticas, portas de enrolar, portinhola, alçapão, automatizadores, controle portão. tudo para portas de aço">
<meta property="og:site_name" content="Original Portas">

<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- -----------------JS----------------- -->
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script src="js/validation.js" type="text/javascript"></script>
<!-- -------------Validação input/telefone ----------------------------------->
<script type="text/javascript" src="js/mask_number.js"></script>
<!-----------------font Google------------------>
<link href="https://fonts.googleapis.com/css?family=Play"
	rel="stylesheet">

<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>


<style>
.container, .row, label, h1, h2, h3, h4, h5, p {
	font-family: 'Play', sans-serif;
}
</style>


<script>
function mascara_data(dt_nasc){ 
    var mydata = ''; 
    mydata = mydata + dt_nasc; 
    if (mydata.length == 2){ 
        mydata = mydata + '/'; 
        document.forms[0].dt_nasc.value = mydata; 
    } 
    if (mydata.length == 5){ 
        mydata = mydata + '/'; 
        document.forms[0].dt_nasc.value = mydata; 
    } 
    if (mydata.length == 10){ 
        verifica_data(); 
    } 
} 
 
function verifica_data () { 

  dia = (document.forms[0].dt_nasc.value.substring(0,2)); 
  mes = (document.forms[0].dt_nasc.value.substring(3,5)); 
  ano = (document.forms[0].dt_nasc.value.substring(6,10)); 

  situacao = ""; 
  // verifica o dia valido para cada mes 
  if ((dia < 01)||(dia < 01 || dia > 30) && (  mes == 04 || mes == 06 || mes == 09 || mes == 11 ) || dia > 31) { 
      situacao = "falsa"; 
  } 

  // verifica se o mes e valido 
  if (mes < 01 || mes > 12 ) { 
      situacao = "falsa"; 
  } 

  

  if (document.forms[0].data.value == "") { 
      situacao = "falsa"; 
  } 

  if (situacao == "falsa") { 
      alert("Data inválida!"); 
      document.forms[0].dt_nasc.focus(); 
  } 
} 
</script>
<script type="text/javascript">
</script>


<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>
<!--  busca cep -->
<script defer src="js/busca_cep.js"></script>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container">
		<div class="form">
			<div class="row" style="background-color: #fff;">
				<div class="col-md-12">
					<h1>Preencha os campos abaixo para realizar a pré-inscrição.</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					<br>
				</div>
				<form enctype="multipart/form-data" id="formInscricao" method="post"
					name="contato-envia" action="inscricao-envia.php" class="form2">
					<!-- input type="hidden" name="ACAO" value="ENVIAR" / -->

					<div class="col-md-6 col-sm-3">
						<label for="nome">Nome: <span>*</span></label><br> <input
							onkeypress="return Onlychars(event)" onKeyUp="UcWords(this)"
							type="text" name="nome" class="nome" value="" id="nome"
							style="width: 80%;" size="45" maxlength="60" />
					</div>
					<div class="col-md-6">
						<label for="dt_nasc">Data Nasc.: <span>*</span></label><br> <input
							type="text" name="dt_nasc" id="dt_nasc" OnKeyUp="mascara_data(this.value)"
							maxlength="10"><br>

					</div>
					<div class="col-md-6">
						<label for="telefone"> DDD/Telefone: <span>*</span></label><br> <input
							type="text" name="ddd" class="ddd" value="" id="ddd" size="2"
							maxlength="2" onkeypress="return Onlynumbers(event)" /> <input
							type="text" name="telefone" class="telefone" value=""
							id="telefone" size="9" maxlength="9"
							onkeypress="return Onlynumbers(event)" />
					</div>

					<div class="col-md-6">
						<label for="email">E-mail: <span>*</span>
						</label><br> <input onKeyUp="minusculas(this)" type="text"
							name="email" class="email" value="" id="email"
							style="width: 80%;" size="45" maxlength="60" />
					</div>

					<div class="col-md-6">
						<label> Curso Desejado ?: <span>*</span>
						</label><br> <select name="tp_curso" style="width: 80%;">
							<option value="">Selecione</option>
							<!--option  value="Instalador Porta Automatica 23/08/2018">Instalador
								de Portas de aço Aut. 17/10/2018</option-->

							<!--option value="Instalador Porta Automatica 28/11/2018" >Instalador
								de Portas de aço Aut. 28/11/2018</option-->
						</select>
					</div>
					<br>
					<div class="col-md-12">
						<label>Cep: <span>*</span><a style="font-size: 15px"></a><br> <input name="cep"
							type="text" id="cep" value="" size="10" maxlength="8"
							onkeypress="return Onlynumbers(event)" /></label><br>
					</div>
					<div class="col-md-6">

						<label>Rua: <span>*</span><br> <input name="rua" type="text" id="rua"
							style="width: 80%;" size="45" maxlength="60" />
						</label><br> <label>Bairro: <span>*</span><br> <input name="bairro" type="text"
							id="bairro" style="width: 80%;" size="45" maxlength="60" /></label><br>
					</div>
					<div class="col-md-6">
						<label>Cidade: <span>*</span><br> <input name="cidade" type="text" id="cidade"
							style="width: 80%;" size="45" maxlength="60" />
						</label><br> <label> UF: <span>*</span><br> <input name="uf" type="text" id="uf"
							size="2" maxlength="2" />
						</label><br> <br>

					</div>
					<div class="col-md-12">
						<p>Os campos com (*) são obrigatórios</p>
					</div>
					<div class="col-md-12">
						<label><strong>Atenção:</strong> A realização da pré-inscrição não
							garante vaga aos cursos inscritos. A vaga só é garantida
							mediante a comprovação de pagamento</label>
					</div>
					<div class="col-md-12" id="img_vao">

						<br> <span class="bt-submit">
							<button type="submit" value="Enviar" class="btn btn-primary">Enviar</button><br>
						</span><br>
					</div>

				</form>

			</div>
		</div>


	</div>
		
		
	
	
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('554799663772', '554791235132'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	

<?php echo $footer;?>
</body>
</html>

