<?php require 'main.php';
require 'footer.php';?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
<title>Portas rapidas de PVC tipo Lona - Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description"
	content="As portas rápidas de Lona PVC, podem ser ativadas diversas vezes durante o dia , estas portas por serem leves forçam menos o trabalho do motor aumentando a sua vida útil e velocidade.">
<meta name="keywords"
	content="Original Portas, portas de lona, portas de PVC, portas rápidas automáticas,Portas Indústriais de lona, portas para frigorificos, portas para insdustrias cosméticas, farmacêutias, alimenticias, porta de enrolar de PVC, portas de toldo, portas de plastico.">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="porta_rapida.php">
<meta name="author" content="tworock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title" content="Portas Rápidas de Lona - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url" content="porta_rapida.php">
<meta property="og:description" content="As portas rápidas de Lona PVC, podem ser ativadas diversas vezes durante o dia , estas portas por serem leves forçam menos o trabalho do motor aumentando a sua vida útil e velocidade.">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">
function disableselect(e){ 
return false 
} 
function reEnable(){ 
return true 
} 
//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script> 

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container">
    <div class="class_port">
    	<div class="row">
    	 <div class="col-md-6">
		   <img id="port_comerc" alt="Porta comercial" src="imagens/porta_rapida/615rev.png">
		 </div>
		 <div id="id_port" class="col-md-6" >
			<h1>PORTA RÁPIDA DE LONA</h1>
		    <hr style="width: auto; height: 2px; background-color: #ccc;">
			    <p> Nossas portas de lona possuem um sistema de recolhimento rápido e automático, sendo ativado por sensores de movimento, tirando totalmente a necessidade de pressionar algum tipo de botão de acionamento, sendo por controle remoto ou manual. </p>
                <p>O funcionamento das portas rápidas é bem simples, ao passar pelo sensor que será ativado pelo movimento, acionando o recolhimento da porta em 2 segundos, após alguns segundos ,se n&atilde;o houver movimento nos sensores, a porta se fechará automaticamente.</p>
                <p>As portas rápidas são compostas de PVC, material leve e resistente.<br>
                   Em seu Centro contém um tipo de PVC incolor, que possibilita enxergar o outro lado mesmo com a porta fechada, diminuindo os riscos de acidentes.</p>
                 <p>
        		 <p> Nossas Portas Rápidas de Lona são indicadas em:<br>
				      - Indústrias farmacêuticas. <br>
				      - Indústrias de alimentos. <br>
                      - Indústrias de cosméticos. <br>
                      - Indústrias de produtos químicos. <br>
				      - Frigoríficos. <br>
                                      - Supermercados.<br> 
                      - E para todos os lugares que necessitem de tr&aacute;fego r&aacute;pido e de pouco contato com o ambiente externo, e evitar contaminações e pragas.
                 <p> Faça um orçamento conosco, além de cumprirmos todas as normas de qualidade exigidas, damos garantia de nossos produtos, entre em contato e tire todas as suas dúvidas.</p>
          </div>
          <br>
	  </div>
   </div>        
</div>
<br>	
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('554799663772', '554791235132'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>
</html>