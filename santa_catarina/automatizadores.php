<?php
require 'main.php';
require 'footer.php';
?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
        <title>Automatizadores - Original Portas</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description"
	content="Os automatizadores que trabalhamos, possuem a melhor qualidade para sermos os melhores em fabrica  o e instala  o de portas de a o autom tica">
<meta
	name="Automatizadores, automatizador, nobreak, portas, enrolar, aco">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="automatizadores">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Automatizadores - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image"
	content="imagens/automatizador/automatizador.png">
<meta property="og:url" content="automatizadores">
<meta property="og:description"
	content="Os automatizadores que trabalhamos, possuem a melhor qualidade para sermos os melhores em fabrica  o e instala  o de portas de a o autom tica">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 
//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2">
				<img src="imagens/automatizador/automatizador.png"
					width="100%" height="auto">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="class_aut2">
			<div class="row">
				<div class="col-md-12">
					<h1 style="margin-top: -20px;">Automatizadores</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
  <p>
          Trabalhamos com Automatizadores da Megatron, conhecida empresa de importação do ramo, existente há mais de cinco anos.
          <br><br>
          Todos os nossos automatizadores possuem Certificado ISO 9001 2000, sendo garantida sua procedência e qualidade.
          <br><br>
          São produtos fáceis de manusear, que requerem pouquíssima manutenção. E o melhor: com baixo custo.
          <br><br>
          Precisa de automatizador para porta de aço automática? <a href="contato.php">Entre em Contato.</a>

         </p>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>

<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('554799663772', '554791235132'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>		

<?php echo $footer;?>
</body>
</html>