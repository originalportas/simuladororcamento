<?php
require 'main.php';
require 'footer.php';
?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
<title>Porta Rápida Dobrável - Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description"
	content="A porta rápida dobrável são preparadas pra áreas
							de trafego muito intenso , por esse meio a sua empresa consegue
							alcançar o objetivo na produtividade logística com qualidade
							total.">
<meta name="keywords"
	content="Porta rápida Dobravél, porta, rapida, dobravel, PV, porta enrolar, porta de plastico, porta de toldo, toldo, automática">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="porta-rapida-dobravel">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Porta Rápida Dobrável - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image"
	content="imagens/porta_rapida/porta-rapida-dobravel-01.png">
<meta property="og:url" content="porta-rapida-dobravel">
<meta property="og:description"
	content="A porta rápida dobrável são preparadas pra áreas
							de trafego muito intenso , por esse meio a sua empresa consegue
							alcançar o objetivo na produtividade logística com qualidade
							total.">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2">
				<img src="imagens/porta_rapida/porta-enrolar-dobravel.png"
					width="100%" height="auto">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="class_aut2">
			<div class="row">
				<div class="col-md-12">
					<h1 style="margin-top: -20px;">Portas Rápidas Dobráveis</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					<div class="col-md-12">
						<p>
							A <strong>porta rápida dobrável</strong> são preparadas pra áreas
							de trafego muito intenso , por esse meio a sua empresa consegue
							alcançar o objetivo na produtividade logística com qualidade
							total.
						</p>
					</div>
					<div class="col-md-4" style=" margin-top: 30px;">

						<h2>alta Velocidade</h2>
						<p>Esta porta rápida dobrável leva 1 metro
							por 1,5 segundos para abrir e fechar , é a segunda porta mais
							rápida que temos , ficando atrás da porta rápida de enrolar que
							leva 1 metro por segundo para abrir e fechar.</p>
						<h2>Composição estrutural</h2>
						<p>Composata por dois elementos e uma viga
							superior, onde se oculta o mecanismo da porta , esta estrutura é
							confeccionada com chapa galvanizada ou aço inox, possui escovas de
							vedação em todo o perímetro.</p>
					</div>
					<div class="col-md-8">
						<br> <img src="imagens/porta_rapida/porta-rapida-dobravel-07.jpg"
							width="100%" height="auto" style="margin-top: 20px;">
					</div>
					
					<div class="col-md-8" style="margin-top: 50px;">
						<img src="imagens/porta_rapida/porta-rapida-dobravel-10.jpg"
							width="100%" height="auto">
					</div>
					<div class="col-md-4" style="margin-top: 20px;">
						<h2>Composição do Tecido</h2>
						<p>
							Confeccionado com um tecido mutifilamento de poliéster revestido
							de <strong>PVC</strong> em ambos os lados, tornando -se
							impermeavél e anti-chama, a porta rápida dobrável também possui proteção contra Raios UV
							e nevoas salinas; com reforços tranversais de barras de aço
							galvanizado.
						</p>

					<h2>Indicações</h2>
						<p>Está porta rápida dobrável é ideal para montadoras, supermercados, laboratórios
							Farmacêuticos ,industrias de alimentos e em qualquer lugar de
							alto fluxo de trafego e com passagens controladas.</p>
					</div>
				</div>

			</div>
		</div>
	</div>
	<br>
	<br>	
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('553195265215', '553171571978','553196781446'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>
</html>