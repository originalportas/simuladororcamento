<?php require 'main.php';
require 'footer.php';?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
<title>Certificação em Portas de Aço - Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description"
	content="Na Original Portas, temos certificação ISO 9001 e Selo inmetro de todos os nossos produtos , além do nosso selo de qualidade original">
<meta name="keywords"
	content="Portas Certificadas iso 9001, produtos inmetro, qualidade original portas, portas aço automáticas">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="certificacao.php">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title" content="Certificação em Portas de Aço - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url" content="certificacao.php">
<meta property="og:description" content="Na Original Portas, temos certificação ISO 9001 e Selo inmetro de todos os nossos produtos , além do nosso selo de qualidade original">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">

<!-- ----------------logo rede social----------------->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script> 
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script> 

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
  <div class="row">
    <div id="ban_iso"><img src="imagens/banner_iso.png" width="100%"height="auto">
    </div>
  </div>
</div>
<div class="container">
	<div class="certif">
		<div class="row">
		   <div class="col-md-12">
					<h1>Certificação ISO 9001</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					<br>
					<p>
						A ISO 9001 É a norma mais conhecida, quando o assunto é padrão de
						qualidade. Muitas empresas adotam as práticas desta norma, porque
						isso passa mais credibilidade e fortalece a imagem e a reputaçao
						do empreendimento. <br>O que muitos empreendedores ainda não sabem
						Qual a importancia da certificação e qual o verdadeiro significado
						da norma?
					</p>
                    <h2>O que é a ISO 9001?</h2>
					<p>É a norma de padronização da International Standard
						Organization (ISO), voltada a produtos e serviços, sendo utilizada
						em todo o mundo por empresas de quaisquer segmentos e portes. A
						finalidade de aplicar boas práticas e aprimorar a gestão
						organizacional. Essa regra também pode ser aliada a outras regras
						relacionadas, por exemplo, meio ambiente, saúde ocupacional e
						segurança. Por isso, um elemento estratégico, que determina as
						obrigatoriedades necessárias para fazer uma gestão da qualidade e
						de que forma a gestão deve ser realizada. Um dos focos dessa regra
						é compreender as demandas e necessidades dos clientes. Para isso
						segue uma hierarquia que começa no âmbito estratégico e vai até o
						operacional, além de passar pelo tático</p>
					<h2>Quais as vantagens da ISO 9001?</h2>
					<p>
						A implantação da norma é difícil e exige o emprego de esforço e
						colaboração por parte de toda a empresa. No entanto, á vários
						benefícios materiais e imateriais conquistados com essa atitude.
						Confira alguns Beneficos do o ISO 9001:<br> - &nbsp; Aumento da
						produtividade.<br> - &nbsp; Agregação de valor aos produtos e
						serviços.<br> - &nbsp; Padronização de processos.<br>

					</p>
			</div>
            <div class="col-md-12">
                  <h2>Como seguir os requisitos da norma ISO 9001?</h2>
					<p>
						As regras internacionais são adaptadas para o contexto brasileiro
						pela Associação Brasileira de Normas Técnicas (ABNT). As
						recomendaçõees gerais têm a finalidade de padronizar as
						instalações, linha de produão, gestão de pessoas, logística e
						outras atividades realizadas pela empresa. De modo indireto, esses
						quesitos impactam a satisfaçao dos consumidores, que se beneficiam
						dos produtos e serviços. Qualquer empreendimento pode adotar as
						práticas especificadas na norma. No entanto, só será reconhecidas
						aquelas que obtiverem a certificação. Para isso é preciso passar
						por um processo de auditoria para garantir que todas as exigências
						são cumpridas. O primeiro passo é ter um planejamento estratégico
						que seja documentado, aplicado e mantido. Esse plano deve
						apresentar os processos exigidos para a efetivação do sistema de
						qualidade, que passa pela definição de:<br> - &nbsp; pontos de
						intercessão entre os processos;<br> - &nbsp; recursos físicos,
						humanos e econômicos necessários para a implantação;<br> - &nbsp;
						mêtodos de monitoramento e controle dos processos;<br> - &nbsp;
						ações corretivas para assegurar o aprimoramento constante.<br> A
						auditoria externa fará a avaliação dos requisitos e indicará se a
						empresa cumpre a norma ou não. No segundo caso, pode-se ajustar o
						que for indicado e pedir outra análise.
					</p>
					<p>Depois de certificado, o empreendimento passa por avaliações
						constantes para garantir a suscetibilidade e autenticidade dos
						processos.</p>
					<h2>
						<a target="_blank" href="dcs/certificado_iso.pdf"
							class="btn btn-primary btn-lg active" role="button"
							aria-pressed="true" style="font-size: 20px;">Certificado ISO-9001
							<i class="fa fa-file-pdf-o"></i>
					  </a>
		    </h2>
	    </div>
     </div>
  </div>
</div>
<br>
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('553195265215', '553171571978','553196781446'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>
</html>