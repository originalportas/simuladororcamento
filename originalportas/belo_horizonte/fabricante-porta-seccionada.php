<?php
require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async
	src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script>
<script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Fabricante de Porta Seccionada - Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description"
	content="O fabricante de porta seccionada consegue fabricá-las sob medida, com isso, elas não deixam um espaço no vão e se encaixam perfeitamente no local">
<meta name="keywords"
	content="Fabricante de Porta Seccionada, fabricante, porta, seccionada, portão garagem americano, portão, americano ">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="fabricante-porta-seccionada">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Fabricante de Porta Seccionada - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image"
	content="imagens/porta_seccionada/fabricante-porta-seccionada.png">
<meta property="og:url" content="fabricante-porta-seccionada">
<meta property="og:description"
	content="O fabricante de porta seccionada consegue fabricá-las sob medida, com isso, elas não deixam um espaço no vão e se encaixam perfeitamente no local">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2">
				<img src="imagens/porta_seccionada/fabricante-porta-seccionada-1.png"
					width="100%" height="auto">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="class_aut2">
			<div class="row">
				<div class="col-md-12">
					<h1>Fabricante de Portas Seccionadas</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					<p>Há tantos ótimos motivos para escolher uma<strong>Porta Seccional</strong>, em
						primeiro lugar, porque as <strong>Portas Seccionadas</strong> da Original Portas
						oferecem o mais Moderno Design com Elegância e Praticidade, sendo
						ao mesmo tempo Altamente Confiável, por oferecer um Nível de
						Segurança Elevado.</p>
				</div>
				<div class="col-sm-12 col-md-12 col-xl-12" style="margin-top: 20px;">
					<video width="100%" muted loop autoplay> <source
						src="video/port-sectional.mp4" type="video/mp4"></video>

				</div>
				<br>

				<div class="col-md-6 col-xl-6" style="background-color:transparent;">
					<h1>Quais os Tipo de Portas Seccionadas?</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">


					<h2>PORTAS SECCIONADAS RESIDENCIAIS.</h2>
					<p>A <strong>Porta Seccionada Residencial</strong> é elaborada em estrutura que lhe
						garante uma maior resistência e durabilidade. Isso também faz com
						que ela não consiga ser facilmente violada, sendo uma resistente
						barreira para possíveis invasores. Sendo também composta com
						painéis de até 40mm de espessura, formadas por duas chapas
						galvanizadas com o miolo de poliuretano que podem ser pintados com
						pintura eletrostática.</p>
						
						<p>As <strong>Portas Seccionadas Residenciais</strong> são
						bastante utilizadas na Europa e em Garagens na América do Norte,
						essas portas aparecem sempre nos filmes de Hollywood. </p>
						
						<p>A Original Portas traz essa novidade para que você tenha em sua casa o modelo
						de porta internacionalmente conhecido por <strong>Portas Seccionadas</strong>. Ela
						ainda conta com um Isolamento Acústico, que faz com que o local
						fique mais silencioso, mesmo quando do lado de fora existe um
						grande barulho e o mesmo Isolamento Térmico das Portas
						Industriais, para que o local conte com uma temperatura sempre
						agradável internamente.</p>
				</div>
				<div class="col-md-6 col-xl-6" style="background-color:transparent;"><a href="porta-seccionada-residencial">
					<img src="imagens/porta_seccionada/secional-residen.png"
						width="100%" height="auto" style="margin-top: 90px;"></a>
				</div>

				<div class="col-md-6 col-xl-6" style="background-color:transparent;"><a href="porta-seccionada-industrial">
					<img src="imagens/porta_seccionada/seccional-interior.jpg"
						width="100%" height="auto" style="margin-top: 100px;"></a>
				</div>


				<div class="col-md-12 col-xl-6" style="margin-top: 60px;">
					<h2>PORTAS SECCIONADAS INDUSTRIAIS</h2>

					<p>A <strong>porta seccionada industrial</strong> é elaborada em estrutura que lhe
						garante uma maior resistência e durabilidade. Isso também faz com
						que ela não consiga ser facilmente violada, sendo uma barreira
						extremamente resistente para possíveis invasores. </p>
						
						<p>Esse tipo de porta é composto com painéis de 40mm, formados por duas chapas
						galvanizadas com o miolo preenchido de poliuretano expandido, e
						podem ser pintadas com pintura eletrostática. </p>
						
						<h4>Quanto custa uma porta seccionada?</h4>
						
						<p>Os valores da <strong>Porta Seccionada Industrial</strong> irão variar conforme o modelo dela, porém,
						todas contam com um sistema de trilhos laterais, que ficam
						discreto e não atrapalha o seu design. Além disso, garantem um
						espaço mínimo nas laterais do vão, o que faz com que seu
						fechamento seja perfeito, impedindo que o ambiente seja observado.
						Ainda contam com um Isolamento Acústico, que faz com que o local
						fique mais silencioso, mesmo quando do lado de fora existe um
						grande barulho. </p>
						
						<p>O Isolamento térmico também é outra de suas
						vantagens. Assim, o local conta com uma temperatura sempre
						agradável internamente. A porta também é automatizada, o que
						facilita a sua abertura e fechamento.</p>
				</div>



				<div class="col-md-12 col-xl-12">
					<h2>Quais as Vantagens das Portas Seccionadas?</h2>

					<p>As <strong>portas seccionadas</strong> são baseadas nas tendências da Europa. São
						muito bonitas e práticas, tanto que acabaram ganhando todo o
						mundo. </p>
						
						<p>Aqui no Brasil, estamos constantemente estudando e buscando
						nos especializar em novas tecnologias e evoluindo cada vez mais,
						para levarmos aos nossos clientes sempre a melhor Porta
						Seccionada. </p>
						
					<p>	As <strong>Portas Seccionadas</strong> são fabricadas sob medida. Com
						isso, elas não deixam um espaço no vão e se encaixam perfeitamente
						no local para o qual ela foi planejada. Além disso, ela é formada
						por painéis articuláveis que se encaixam perfeitamente e garantem
						uma maior mobilidade sem danificar.</p> 
						
						<p>Esse modelo vem ganhando espaço no Brasil porque não precisa de rolos, como acontece com as
						<strong>Portas de Enrolar</strong>. Isso garante maior durabilidade a ela. Mas a
						vantagem de não precisar de muito espaço para ser aberta
						permanece, isso porque ela pode ser aberta na mesma linha do teto.</p>
						
						<p>Elas também já saem da Fábrica com Isolamento Térmico e Acústico,
						garantindo mais comodidade e privacidade nos ambientes em que está
						instalada.</p>
				</div>
				<div class="col-md-12 col-xl-12">
					<h2>Escolhendo o Fabricante de Porta Seccionada</h2>

					<p>A<strong> porta seccionada </strong>conta com uma infinidade de vantagens e
						consegue deixar a entrada dos ambientes muito mais bonitas, porém,
						é preciso escolher com cuidado o <strong>Fabricante da sua Porta
						Seccionada</strong>. Isso porque é preciso materiais de alta qualidade e um
						acabamento impecável. Contrate um Fabricante que possua
						conhecimento do produto. </p>
						
						<p>Elas devem deslizar perfeitamente nos
						trilhos, para que não fiquem travando e acabem quebrando, gerando
						um alto custo de manutenção.</p>
						
						 <p>O mais importante de tudo é poder
						contar com uma empresa séria e que realmente possa cumprir com o
						prometido. Isso porque como esses portões costumam ser feitos sob
						encomenda podem demorar um pouco para serem entregues e se não
						vierem como o estipulado atrasam as obras, causando bastante dor
						de cabeça.</p>
				
				</div>



			</div>
		</div>
	</div>
	<br>
	<br>
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array('553195265215', '553171571978','553196781446'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>
</html>