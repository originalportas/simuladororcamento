<?php
require 'main.php';
require 'footer.php';
?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
<title>Porta de Loja Automática - Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description"
	content="Uma porta de loja automática é a opções mais utilizada no comércio, principalmente em grandes cidades. Isso porque ela garante mais praticidade e segurança">
<meta name="keywords"
	content="Porta de Loja Automática, porta, loja, automática, enrolar, aco">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="porta-loja-automatica">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Porta de Loja Automática - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/porta_comercial/distribuidor-portas-enrolar.png">
<meta property="og:url" content="porta-loja-automatica">
<meta property="og:description"
	content="Uma porta de loja automática é a opções mais utilizada no comércio, principalmente em grandes cidades. Isso porque ela garante mais praticidade e segurança">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 
//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro2">
				<img src="imagens/porta_comercial/distribuidor-portas-enrolar.png" width="100%"
					height="auto">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="class_aut2">
			<div class="row">
				<div class="col-md-12">
					<h1 style="margin-top: -20px;">Porta de Loja Automática</h1>
					<hr style="width: auto; height: 2px; background-color: #ccc;">
					<h2>Conheça os tipos de portas para lojas</h2>

					<p>
						Para garantir a segurança, os estabelecimentos comerciais já
						contam com sistemas de alarmes, câmeras de segurança e <strong>porta
							de loja automática</strong>. Isso tudo precisa estar alinhado com
						a estrutura e identidade visual da marca, para que o ambiente não
						fique com a aparência destoando.
					</p>

					<p>
						Por isso, na hora de escolher a <strong>porta de loja automática</strong>
						que será utilizada, pesquise todos tipos disponíveis no mercado:
						verifique os modelos, tamanhos e cores, para que combine
						perfeitamente com a fachada da sua loja e, assim, reforce ainda
						mais a sua marca.
					</p>

					<h2>Porta de loja automática</h2>

					<p>
						Uma <strong>porta de loja automática</strong> é a opções mais
						utilizadas no comércio, principalmente em grandes cidades. Isso
						porque ela garante mais praticidade e segurança durante o manuseio
						e também evita o esforço físico – e que o funcionário se suje de
						graxa.
					</p>

					<p>
						A <strong>porta de loja automática</strong> pode ser facilmente
						instalada por empresas especializadas em portas de aço. Seu
						funcionamento é bastante simples e não requer grande conhecimento,
						afinal, para ser acionada é preciso apenas pressionar um botão.
					</p>

					<p>
						Outra grande vantagem é que o trajeto pode ser parado a qualquer
						momento e quando não há necessidade de abrir a <strong>porta de
							loja automática</strong> por inteiro consegue-se fazer abertura
						apenas parcial.
					</p>

					<p>
						Com uma <strong>porta de loja automática</strong> a segurança do
						estabelecimento é total. Isso porque mesmo sendo forçada, sua
						abertura não é facilitada – ela só abre quando o botão é
						pressionado.
					</p>

					<h2>Porta de loja manual</h2>

					<p>
						Além da <strong>porta de loja automática</strong>, existe também o
						modelo manual. Bastante utilizado em comércios pequenos, que tem
						dimensões menores, fazendo com que a porta não seja muito pesada.
						Assim, um funcionário consegue empurrar e puxar com facilidade.
					</p>

					<p>Apesar de trabalhar de forma manual, ela também consegue
						garantir a segurança do estabelecimento, uma vez que conta com
						fechadura bastante resistente.</p>

					<h2>Tipos de lâminas para porta de loja automática</h2>

					<p>
						Seja a sua <strong>porta de loja automática</strong> ou manual, é
						possível contar com diversos tipos de lâminas. Quem deseja ter
						mais privacidade dentro do ambiente pode escolher modelos
						totalmente fechados. Já quem quer que a vitrine fique à mostra
						mesmo enquanto a loja não está aberta pode escolher por modelos
						vazados, como a Transvision.
					</p>

				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array( '5511959681162', '5511959065418','5511949523408','5511953536427','5511953874598'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>
</html>