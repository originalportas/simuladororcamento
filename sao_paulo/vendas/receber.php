<?php
require('./fpdf/fpdf.php');
include('./class.ipdetails.php');
include('./banco.php');
#$ip = $_SERVER['REMOTE_ADDR'];
$ip = "189.73.71.160";
$ipdetails = new ipdetails($ip);
$ipdetails->scan();
$localOrcamento = $ipdetails->get_region()." - ".$ipdetails->get_city();

$checkPintura = isset($_POST['checkPintura']) ? $_POST['checkPintura'] : 0;
$CheckPortinhola = isset($_POST['CheckPortinhola']) ? $_POST['CheckPortinhola'] : 0;
$CheckalcaEnrolar = isset($_POST['CheckalcaEnrolar']) ? $_POST['CheckalcaEnrolar'] : 0;
$CheckBorraVedacao = isset($_POST['CheckBorraVedacao']) ? $_POST['CheckBorraVedacao'] : 0;
$CheckPvcGuias = isset($_POST['CheckPvcGuias']) ? $_POST['CheckPvcGuias'] : 0;
$CheckMkit = isset($_POST['CheckMkit']) ? $_POST['CheckMkit'] : 0;
$CheckCentral = isset($_POST['CheckCentral']) ? $_POST['CheckCentral'] : 0;
$nome =  utf8_decode('NOME: ' . strtoupper($_POST['nome'])); 
$endereco = utf8_decode('ENDEREÇO: ' . strtoupper($_POST['endereco']));
$cidade = $_POST['cidade'];
$telefone = $_POST['telefone'];
$porta = $_POST['porta'];
$resChapa = $_POST['resChapa'];
$totalChapa = $_POST['totalChapa'];
$guia = $_POST['guia'];
$tamGuia = $_POST['tamGuia'];
$resulGuia = $_POST['resulGuia'];
$nomeEixo = $_POST['nomeEixo'];
$lgr = $_POST['lgr'];
$resulEixo = $_POST['resulEixo'];
$valorSoleira = $_POST['valorSoleira'];
$motor = $_POST['motor'];
$motorValor = $_POST['motorValor'];
$pvcGuias = $_POST['pvcGuias'];
$resultPinEletros = $_POST['resultPinEletros'];
$valPortinhola = $_POST['valPortinhola'];
$valAlcapao = $_POST['valAlcapao'];
$centDoisContr = $_POST['centDoisContr'];
$valorMontKit = $_POST['valorMontKit'];
$valBorraVedacao = $_POST['valBorraVedacao'];
$totalPvcGuias = $_POST['totalPvcGuias'];
$totalOrcam = $_POST['totalOrcam'];

$data = date('Y/m/d');


$linha1 = "ORÇAMENTO SERRALHEIRO";
$linha2 = "NOME: " . $nome;
$linha3 = "ENDEREÇO: " . $endereco;
$linha12 = "CIDADE: " . $cidade;
$linha4 = "TELEFONE: " . $telefone;
$linha5 = $porta . " - M² " . $resChapa . " - R$ " . number_format($totalChapa, 2, ',', '.');
$linha6 = $guia . " - ML. " . $tamGuia . " - R$ " . number_format($resulGuia, 2, ',', '.');
$linha7 = $nomeEixo . " - ML. " . str_replace('.', ',', $lgr) . " - R$ " . number_format($resulEixo, 2, ',', '.');
$linha8 = "Soleira em T - ML. " . str_replace('.', ',', $lgr) . " - R$ " . number_format($valorSoleira, 2, ',', '.');
$linha9 = $motor . " - PC. 1 - R$ " . number_format($motorValor, 2, ',', '.');
$linha13 = "ITENS ADICIONAIS";
if ($checkPintura != '0') {
    $linha14 = "Pintura Eletrostatica - M² " . $resChapa . " - R$ " . number_format($resultPinEletros, 2, ',', '.');
    $totalOrcam = $totalOrcam += $resultPinEletros;
} else {
    $linha14 = "Pintura Eletrostatica - ======= ";
}
if ($CheckPortinhola != '0') {
    $linha15 = "Portinhola - PC. 1 - R$ " . number_format($valPortinhola, 2, ',', '.');
    $totalOrcam = $totalOrcam += $valPortinhola;
} else {
    $linha15 = "Portinhola - ======= ";
}
if ($CheckalcaEnrolar != '0') {
    $linha16 = "Alçapão de Enrolar - PC. 1 - R$ " . number_format($valAlcapao, 2, ',', '.');
    $totalOrcam = $totalOrcam += $valAlcapao;
} else {
    $linha16 = "Alçapão de Enrolar - ======= ";
}
if ($CheckCentral != '0') {
    $linha17 = "Central com 2 controles - PC. 1 - R$ " . number_format($centDoisContr, 2, ',', '.');
    $totalOrcam = $totalOrcam += $centDoisContr;
} else {
    $linha17 = "Central com 2 controles - ======= ";
}
if ($CheckBorraVedacao != '0') {
    $linha10 = "Borracha vedação - ML. " . str_replace('.', ',', $lgr) . " - R$ " . number_format($valBorraVedacao, 2, ',', '.');
    $totalOrcam = $totalOrcam += $valBorraVedacao;
} else {
    $linha10 = "Borracha vedação - ======= ";
}
if ($CheckPvcGuias != '0') {
    $linha11 = "PVC Guias - ML. " . $pvcGuias . " - R$ " . number_format($totalPvcGuias, 2, ',', '.');
    $totalOrcam = $totalOrcam += $totalPvcGuias;
} else {
    $linha11 = "PVC Guias - ======= ";
}
if ($CheckMkit != '0') {
    $linha18 = "Montagem de Kit - PC. 1 - R$ " . number_format($valorMontKit, 2, ',', '.');
    $totalOrcam = $totalOrcam += $valorMontKit;
} else {
    $linha18 = "Montagem de Kit - ======= ";
}
$Ultimalinha = "TOTAL ORÇAMENTO";
$totalAdic = $totalOrcam;
$total = "R$ " . number_format($totalAdic, 2, ',', '.');
$espaco = " ";

$sql = $pdo->prepare("INSERT INTO orcamento_vendas VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
$sql->execute(array($nome, $endereco, $cidade, $telefone, $data, $localOrcamento, $resChapa, $porta, $tamGuia, $lgr, $guia, $nomeEixo, $motor, $checkPintura, $CheckPortinhola, $CheckalcaEnrolar, $CheckCentral, $CheckBorraVedacao, $CheckPvcGuias, $pvcGuias, $CheckMkit));


class PDF extends FPDF
{
    // Page header
    function Header()
    {
        // Logo
        $this->Image('./images/Logo.jpg', 5, 10, -900);
        $this->Image('./images/INMETRO.jpg', 85, 10, -4800);
        $this->Image('./images/ISO.jpg', 100, 8, -3600);
        $this->Image('./images/QUALIDADE.jpg', 120, 10, -4800);
        // Arial bold 15
        $this->SetFont('Arial', 'B', 10);
        // Move to the right

        // Title
        $this->Cell(350, 10, utf8_decode('ORÇAMENTO VENDAS'), 0, 1, 'C');
        $this->Ln(20);
    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial', 'B', 11);
        $this->Line(220, 240, 0, 240);
        $this->Cell(0, -70, utf8_decode('ATENÇÃO:(*)'), 0, 1, 'L');
        $this->SetFont('Arial', '', 8);
        $this->Cell(0, 80, utf8_decode('(*) O transporte do motor até a assistência técnica, é de total responsabilidade do cliente.'), 0, 1, 'L');
        $this->Cell(0, -70, utf8_decode('(*) As entregas só podem ser recebidas pelo cliente ou por seu representante.'), 0, 1, 'L');
        $this->Cell(0, 80, utf8_decode('(*) Caro cliente, confira sua mercadoria no ato da entrega, não aceitaremos reclamações posteriores.'), 0, 1, 'L');
        $this->Cell(0, -70, utf8_decode('(*) Prazo de entrega passará a contar somente após a confirmação do pagamento da entrega.'), 0, 1, 'L');
        $this->Line(220, 275, 0, 275);
        $this->Cell(0, 110, utf8_decode('Rua Luis Delgado, 42 - Jd. Modelo - São Paulo/Tel.:(11) 4386-1001'), 0, 1, 'C');
        $this->Cell(0, -100, utf8_decode('www.originalportas.com.br - contato@originalportas.com.br'), 0, 1, 'C');
        // Page number
        //$this->Cell(0, 120, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial', '', 11);
$pdf->Cell(190, 10, $nome, 'T,L,R', 1, 'L');
$pdf->Cell(190, 5, $endereco, 'L,R', 1, 'L');
$pdf->Cell(190, 10, utf8_decode('CIDADE : ' . strtoupper(strval($cidade))), 'L,R', 1, 'L');
$pdf->Cell(190, 5, utf8_decode('TELEFONE : ' . strtoupper(strval($telefone))), 'L,R', 1, 'L');
$pdf->Cell(190, 10, utf8_decode('DATA: '.date('d/m/Y')), 'L,R,B', 1, 'L');
//$pdf->Cell(190, 5, utf8_decode('LOCAL ORÇAMENTO : '.strtoupper($ipdetails->get_region().' - '.$ipdetails->get_country())), 'L,R,B', 0, 'L');
$pdf->Ln(15);
$pdf->SetFont('Arial', '', 8);
//$pdf->Line(220,100,0,100);
$pdf->Cell(190, 5, utf8_decode('QUANTIDADE   |   UN.           |  DESCRIÇÃO'), 'T,L,R', 1, 'L');
if(strlen($resChapa) < 2){
    $pdf->Cell(190, 5, $resChapa . '                      ' . utf8_decode('M²') . '                 ' . utf8_decode(strtoupper(strval($porta))), 'T,L,R', 1, 'L');
}else{
    $pdf->Cell(190, 5, $resChapa . '                      ' . utf8_decode('M²') . '                 ' . utf8_decode(strtoupper(strval($porta))), 'T,L,R', 1, 'L');
}
if(strlen($tamGuia) < 2){
    $pdf->Cell(190, 5, $tamGuia . '                           ' . 'ML.               ' . utf8_decode(strtoupper(strval($guia))), 'T,L,R', 1, 'L');
}else{
    $pdf->Cell(190, 5, $tamGuia . '                         ' . 'ML.               ' . utf8_decode(strtoupper(strval($guia))), 'T,L,R', 1, 'L');
}
if(strlen($lgr) < 2){
    $pdf->Cell(190, 5, str_replace('.', ',', $lgr) . '                           ' . 'ML.               ' . utf8_decode(strtoupper(strval($nomeEixo))), 'T,L,R', 1, 'L');
}else{
    $pdf->Cell(190, 5, str_replace('.', ',', $lgr) . '                        ' . 'ML.               ' . utf8_decode(strtoupper(strval($nomeEixo))), 'T,L,R', 1, 'L');
}
if(strlen($lgr) < 2){
    $pdf->Cell(190, 5, str_replace('.', ',', $lgr) . '                           ' . 'ML.               ' . utf8_decode('SOLEIRA EM T'), 'T,L,R', 1, 'L');
}else{
    $pdf->Cell(190, 5, str_replace('.', ',', $lgr) . '                        ' . 'ML.               ' . utf8_decode('SOLEIRA EM T'), 'T,L,R', 1, 'L');
}
$pdf->Cell(190, 5, '1                           ' . 'PC                ' . utf8_decode(strtoupper(strval($motor))), 'T,L,R,B', 1, 'L');

if ($checkPintura != '0') {
    if(strlen($resChapa) < 2){
        $pdf->Cell(190, 5, $resChapa . '                      ' . utf8_decode('M²') . '                 ' . utf8_decode('PINTURA ELETROSTATICA'), 'L,R,B', 1, 'L');
    }else{
        $pdf->Cell(190, 5, $resChapa . '                      ' . utf8_decode('M²') . '                 ' . utf8_decode('PINTURA ELETROSTATICA'), 'L,R,B', 1, 'L');
    }
}
if ($CheckPortinhola != '0') {
    $pdf->Cell(190, 5, '1                           ' . 'PC.               ' . utf8_decode('PORTINHOLA'), 'L,R,B', 1, 'L');
}
if ($CheckalcaEnrolar != '0') {
    $pdf->Cell(190, 5, '1                           ' . 'PC.               ' . utf8_decode('ALÇAPÃO DE ENROLAR'), 'L,R,B', 1, 'L');
}
if ($CheckCentral != '0') {
    $pdf->Cell(190, 5, '1                           ' . 'PC.               ' . utf8_decode('CENTRAL COM 2 CONTROLES'), 'L,R,B', 1, 'L');
}
if ($CheckBorraVedacao != '0') {
    if(strlen($lgr) < 2){
        $pdf->Cell(190, 5, $lgr . '                           ' . 'ML.               ' . utf8_decode('BORRACHA VEDAÇÃO'), 'L,R,B', 1, 'L');
    }else{
        $pdf->Cell(190, 5, $lgr . '                        ' . 'ML.               ' . utf8_decode('BORRACHA VEDAÇÃO'), 'L,R,B', 1, 'L');
    }
}
if ($CheckPvcGuias != '0') {
    if(strlen($pvcGuias) < 2){
        $pdf->Cell(190, 5, $pvcGuias . '                           ' . 'ML.               ' . utf8_decode('PVC GUIAS'), 'L,R,B', 1, 'L');
    }else{
        $pdf->Cell(190, 5, $pvcGuias . '                         ' . 'ML.               ' . utf8_decode('PVC GUIAS'), 'L,R,B', 1, 'L');
    }
}
if ($CheckMkit != '0') {
    $pdf->Cell(190, 5, '1                           ' . 'PC.               ' . utf8_decode('MONTAGEM DE KIT'), 'L,R,B', 1, 'L');
}
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(190, 5, utf8_decode('VALOR DOS PRODUTOS:........................' . utf8_decode(strtoupper(strval($total)))), 0, 1, 'R');
$pdf->Output();
