<?php

require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async
	src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script>
<script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Kit Porta de Aço Automáticas - Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description" content="A Original Portas tem o orgulho de oferecer aos seus cliente,
							parceiros e colaboradores, sempre o que há de melhor no mercado
							de portas de enrolar, tanto manuais quanto automáticas. Somente
							uma empresa parceira, sólida no mercado a mais de 10 anos e com
							sede própria, ousaria defender e respeitar os seus direitos de
							consumidor. Desde o início nos mantemos em constante preocupação
							com a qualidade dos produtos e serviços prestados, garantia
							comprovada através das Certificações ISO 9001 e InMetro.
							Trabalhamos sabendo que a Segurança e a Satisfação dos nossos
							clientes deve estar sempre em primeiro lugar, e acreditamos que
							não somos apenas fabricantes de portas, aqui na Original, temos a
							certeza de que participamos da concretização de projetos e da
							realização de sonhos.">
<meta name="keywords"
	content="Original Portas, kit porta de aço automatica de enrolar, porta, portas,kit,automaticas.">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="kit-original-portas.php">
<meta name="author" content="tworock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Kit Porta de Aço Automáticas - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/kit-porta-automatica.png">
<meta property="og:url" content="kit-original-portas.php">
<meta property="og:description"
	content="A Original Portas tem o orgulho de oferecer aos seus cliente,
							parceiros e colaboradores, sempre o que há de melhor no mercado
							de portas de enrolar, tanto manuais quanto automáticas. Somente
							uma empresa parceira, sólida no mercado a mais de 10 anos e com
							sede própria, ousaria defender e respeitar os seus direitos de
							consumidor. Desde o início nos mantemos em constante preocupação
							com a qualidade dos produtos e serviços prestados, garantia
							comprovada através das Certificações ISO 9001 e InMetro.
							Trabalhamos sabendo que a Segurança e a Satisfação dos nossos
							clientes deve estar sempre em primeiro lugar, e acreditamos que
							não somos apenas fabricantes de portas, aqui na Original, temos a
							certeza de que participamos da concretização de projetos e da
							realização de sonhos.">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">
function disableselect(e){ 
return false 
} 
function reEnable(){ 
return true 
} 
//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="/site4.0/js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript"
	src="/site4.0/js/jquery.slicknav.js"></script>
<script defer src="/site4.0/js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row" style="margin-top:45px;">
			<div id="ban_eletro4">
				<img src="imagens/kit-original-portas-11.png" width="100%">
			</div>
		</div>
		<div class="container">
			<div class="eletro_div">
				<div class="row">
					<div class="col-md-12" style="background-color:transparent;">
						<h1>Kit Porta de Enrolar Automática</h1>
						<hr style="width: auto; height: 2px; background-color: #ccc;">
						<p>A Original Portas tem o orgulho de oferecer aos seus cliente,
							parceiros e colaboradores, sempre o que há de melhor no mercado
							de portas de enrolar, tanto manuais quanto automáticas. Somente
							uma empresa parceira, sólida no mercado a mais de 10 anos e com
							sede própria, ousaria defender e respeitar os seus direitos de
							consumidor. Desde o início nos mantemos em constante preocupação
							com a qualidade dos produtos e serviços prestados, garantia
							comprovada através das Certificações ISO 9001 e InMetro.
							Trabalhamos sabendo que a Segurança e a Satisfação dos nossos
							clientes deve estar sempre em primeiro lugar, e acreditamos que
							não somos apenas fabricantes de portas, aqui na Original, temos a
							certeza de que participamos da concretização de projetos e da
							realização de sonhos.</p>
					</div>

					<div class="col-md-12 col-xs-12 col-sm-12" style="background-color:transparent;">
						<h2>ITENS QUE FAZEM PARTE DO KIT ORIGINAL PORTAS</h2>
						<p>
							Toda Porta de Aço Automática possui uma série de itens
							Obrigatórios para ser considerada uma Porta de Aço Automática, o
							nosso Kit Original Portas foi desenvolvido a partir desses itens,
							para facilitar a sua compra e também evitar que por descuido ou
							esquecimento sua porta fique incompleta por conta da falta de
							algum item essencial. O Kit Padrão da Original Portas é composto
							por:<br>
							<br> - Eixo e Testeiras;<br> - Guias ou Trilhos Laterais;<br> -
							Lâminas da porta;<br> - Soleira;<br> - Automatizador.<br>
						</p>
						<p>Abaixo listamos esses itens individualmente, para que possa
							entender a função e o mecanismo de funcionamento de sua porta de
							cada um deles, tendo a certeza de saber exatamente porque você
							necessita comprar este item e para que ele serve.</p>
						<br>
					</div>



					<div class="col-md-6 col-xs-12 col-sm-12" style="background-color:transparent;">
						<br>
						<h2 style="margin-top: 20px;">EIXO COMPLETO</h2>

						<p>Buscando melhorar a qualidade de trabalho doso nossos parceiros
							e colaboradores, nós da Original Portas disponibilizamos a
							possibilidade de adquirir o eixo completamente montado, com as
							testeiras soldadas, furação e tudo já alinhado no esquadro com
							travas de segurança contra desalinhamento. Com isso você terá uma
							economia significativa do seu tempo instalação, agilizando seus
							processos, pois será necessário apenas nivela-lo e fixa-lo no
							lugar. E isso lhe trará a possibilidade de realizar mais
							instalações por dia, aumentando bastante a sua produtividade, o
							que resultará em maiores lucros no final do mês.</p>
					</div>

					<div class="col-sm-6 col-md-6 col-xl-6" style="margin-top: 20px; background-color:transparent;">
						<br><img
							alt="eixo completo porta de enrolar"
							src="imagens\acessorios\eixo-completo.png" width="100%"
							height="auto" style="margin-top: 20px;"></a> <br>
					</div>
					

					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<br>
						<h2 style="margin-top: 50px;">GUIAS ou TRILHOS LATERAIS</h2>
						<p>As guias acomodam e fazem o travamento das lâminas nas
							laterais, possuem espessuras padronizadas e a sua escolha é
							determinada através do comprimento total da porta. Trabalhamos
							com bobinas em Aço temperado, o que lhe garante mais segurança e
							confiabilidade na hora de fabricarmos as suas guias, onde
							executamos um processo rigoroso de qualidade, para que suas guias
							sejam feitas sob medida, para lhe garantir o máximo de precisão.
						</p>
					</div>

					<div class="col-sm-6 col-md-6 col-xl-6" style="margin-top: 50px; background-color:transparent;">
						<br> <img alt="guia telescopica"
							src="imagens\acessorios\guia-telescopica.png" width="40%"> <img
							alt="porta-enrolar-aco-comercial"
							src="imagens\acessorios\guia-sobreposta.png" width="30%"> <img
							alt="porta-enrolar-aco-comercial"
							src="imagens\acessorios\guia-comum.png" width="20%">

					</div>

					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<h2 style="margin-top: 40px;">LÂMINAS</h2>
						<p>Totalmente de fabricação própria, seguimos todas as normas de
							segurança, altos padrões de qualidade e acabamento. As Lâminas
							são classificadas por espessuras, e sendo responsáveis por mais
							de 60% da sua segurança de uma porta de enrolar automática,
							devamos levar em conta a sua máxima importância, para produzirmos
							corretamente e com alta qualidade, dimensionando corretamente
							este item.
					
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">

						<img
							alt="lamina porta de aço" src="imagens\lamina404.png"
							width="100%" height="auto" style="margin-top: 20px;">
					</div>




					<div class="col-md-12 col-xs-12 col-sm-12" style="background-color:transparent;">
						<br>
						<h2 style="margin-top: 20px;">SOLEIRA</h2>

						<p>
							A soleira é aquela última peça colocada depois das lâminas
							próxima ao chão para fazer o fechamento de sua porta. Existem
							vários tipos de Soleiras no mercado, algumas mais simples outras
							já mais elaboradas. As Soleiras são feitas com Chapa dobrada em
							“L”, dobra Reforçada em “T” ou “Construção Tubular”, são
							responsáveis pelo fechamento correto da porta é mais um
							importante item de segurança. Cada uma delas possui uma função e
							uma determinada resistência também, a seguir relacionamos os
							tipos que trabalhamos e suas especificações para que possa
							escolher com segurança. <br>
							</div>
							
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<br>
						<h2 style="margin-top: 20px;">Soleiras em Chapa L</h2>
						<p>As Soleiras dobradas em “L” são as mais simples pois foram a
							primeira a ser inventada, consequentemente são também as mais
							frágeis, tecnicamente não são apropriadas para suportar esforços,
							e assim, não há um dimensionamento que garanta seu uso com
							segurança.</p>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
                                <img
								alt="Soleira em L"
								src="imagens\acessorios\soleira-l.png"
								width="100%" height="auto" style="margin-top: 20px;">
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12" style="background-color:transparent;">
						<br>
						<h2 style="margin-top: 20px;">Soleiras com dobra Reforçada em “T”</h2>
						<p>As Soleiras com dobra Reforçada em “T” foram desenvolvidas a
							partir do modelo com tradicional em “L” visando aumentar sua
							segurança e durabilidade, onde através de estudos de esforços e
							cargas, chegou-se à um modelo em “T”, que possuem maior
							segurança. Seu formato duplo aplica uma força de resistência
							contra alavancas e esforços, atendendo portas de até 6M sem
							necessitar do reforço inferior, que ainda pode ser feito através
							de um tubo metalon ou cantoneira soldada na parte inferior da
							Soleira.</p>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<img alt="soleira em T"
							src="imagens\acessorios\soleira-t.png" width="100%" height="auto"
							style="margin-top: 20px;">
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<h2 style="margin-top: 20px;">Soleira em Construção Tubular</h2>

						<p>As Soleiras Tubulares são de fato superiores e não há como
							duvidar disto! Sua estrutura com tubos de aço, são dimensionadas
							para garantir alta resistência, possuem ainda um excelente
							acabamento, unindo beleza, segurança e qualidade. A Soleira
							Tubular é constituída por uma Lâmina na parte superior, um tubo
							metalon soldado em sua parte inferior e ainda por traz é colocado
							mais um tubo de metalon de aço para dar um reforça extra contra
							alavancas ou tentativas de arrombamentos, sendo assim o modelo
							mais resistente e eficiente disponível atualmente.</p>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						 <img alt="soleira tubular"
							src="imagens\acessorios\soleira-tubular.png" width="100%"
							height="auto" style="margin-top: 20px;">
					</div>
				
				<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<br>
					<h2 style="margin-top: 20px;">AUTOMATIZADOR</h2>
					<p>O Automatizador é o coração de uma Porta de Aço Automática, pois
						é responsável por sua abertura e fechamento, sendo assim o item
						principal, pois sem ele a porta se tornaria manual, necessitando
						de molas para auxiliar sua abertura. O Automatizador é calculado
						de acordo com o peso total da porta e o fluxo de trabalho, levando
						em conta a quantidade de aberturas e fechamentos, são separados em
						baixo, médio e auto fluxo e possuem um fluxo de trabalho
						especificado pelo fabricante. Forçar o Automatizador além do
						especificado pelo fabricante reduz gravemente o tempo de vida
						útil, esse desgaste excessivo das peças e dos componentes
						eletrônicos, geram diversos danos que provocam a queima de sua
						máquina em curto prazo, por isso é muito importante seleciona-lo
						corretamente com a certeza de estar exagerando, o que gera custos
						desnecessários. Trabalhamos com o melhor do mercado, somos
						distribuidores autorizados dos AUTOMATIZADORES MEGATRON, são
						motores robustos e fortes, o que lhe garante muito mais qualidade
						para suas instalações, levando em consideração de que todos os
						seus componentes de tração são feitos em Aço de alta resistência,
						contando ainda com sistema de freio embutido para fazer o
						travamento de sua porta e um duplo sistema de fim de curso,
						pensados cuidadosamente na prevenção de possíveis acidentes em
						caso de falhas elétricas.</p>
				</div>
                <div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<img alt="motor automatizador"
							src="imagens\acessorios\motor-automatizador.png" width="100%"
							height="auto" style="margin-top: 80px;">
				</div>
				
				
								<hr style="width: 100%; height: 2px; background-color: #ccc;">
				
				<div class="col-md-12 col-xs-12 col-sm-12" style="background-color:transparent;">									
				
					<br>
					<h2 style="margin-top: 20px;">ITENS OPCIONAIS E ACESSÓRIOS.</h2>
					<p>Existem ainda vários itens opcionais e acessórios que podem
						fazer parte de sua Porta sendo incluídos individualmente, esses
						itens visam aumentar o conforto, a segurança e a durabilidade de
						sua porta.</p>
				</div>
												<hr style="width: 100%; height: 2px; background-color: #ccc;">
				
				<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<br>
					<h2 style="margin-top: 20px;">Tubos de Afastamento.</h2>
					<p>QOs tubos de afastamento são utilizados para alinhar a direção
						de descida da porta com as guias, e também visa aliviar o esforço
						de carga que o Automatizador terá de realizar para fazer a
						abertura de sua porta. Geralmente são usados tubos de metalon de
						aço resistente que podem ser soldados diretamente nas guias ou
						parafusados através de “orelhinhas” furadas. Os Tubos de
						Afastamento também podem ser personalizados com dupla furação para
						embutir os parafusos ou com cantoneiras laterais para fixação, que
						poderá ser feita através de solda ou parafusos fixadores
						“parabolt”.</p>

				</div>
    <div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
						<img alt="tubo de afastamento"
							src="imagens\acessorios\tubo-afastamento.png" width="100%"
							height="auto" style="margin-top: 20px;">
				</div>
				
				
				<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<br>
					<h2 style="margin-top: 20px;">Trava Lâminas.</h2>
					<p>Os Trava Lâminas da Original Portas são confeccionados em
						Polímero de Alta Resistência, são utilizados para fazer o
						travamento de sua Porta de Enrolar Automática no sentido
						horizontal, impedindo o deslocamento das Lâminas, o que causaria o
						seu desalinhamento, possível entrave das Lâminas no interior das
						Guias Laterais e consequente quebra de sua Porta. Além do
						Travamento das Lâminas, outra função importantíssima dos Trava
						Lâminas é reduzir ao máximo os ruídos provenientes atrito entre a
						ponta das Lâminas e o interior das Guias Laterais, proporcionando
						um deslizar macio e suave, o que gera a redução do ruído, e
						automaticamente mais uma forma de Lubrificação para sua porta ter
						mais eficiência, qualidade e consequentemente Maior Durabilidade.

					</p>
				</div>
				<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<img alt="trava laminas"
						src="imagens\acessorios\trava-lamina.png" width="100%" height="auto"
						style="margin-top: 20px;">
				</div>

				<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<br>
					<h2 style="margin-top: 20px;">Fita Auto Lubrificante.</h2>
					<p>As Fitas Auto Lubrificantes são colocadas nas Guias Laterais e
						servem para aliviar o atrito entre as Lâminas e o interior das
						Guias, levando em consideração que as Portas de Enrolar
						Automáticas não usam graxa nem óleo dentro de suas Guias, elas
						também funcionam como lubrificantes que proporcionam o perfeito
						deslizamento das Lâminas.</p>
				</div>

					<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<img alt="fita auto lubrificante"
						src="imagens\acessorios\fita-lubrificante.png" width="70%" height="auto"
						style="margin-top: 20px;">
				</div>
				
				
				<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<br>
					<h2 style="margin-top: 20px;">Borracha de Vedação para Soleira</h2>
					<p>É um acessório muito útil que realiza a vedação daquele espaço
						que pode existir entre o chão e a Soleira, devido a desníveis e
						ondulações no piso. Com esta borracha, além do fechamento perfeito
						de sua porta, também irá impedir a passagem de pó, sujeira,
						insetos e outras objetos indesejáveis.</p>
				</div>
               <div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<img alt="borracha vedacao soleira"
						src="imagens\acessorios\borracha-vedacao.png" width="70%" height="auto"
						style="margin-top: 20px;">
				</div>
				
				<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<br>
					<h2 style="margin-top: 20px;">Central de Controle Remoto</h2>
					<p>As Centrais de Controle Remoto para Portas de Aço Automáticas
						estão se tornando o item quase obrigatório devido a importância de
						sua utilidade, que gera muita praticidade e total conforto.
						Acionar sua porta a distância elimina diversos ações e operações
						que normalmente deveriam ser executadas mesmo com uma Porta
						Automática, pois seu acionamento comum é através de uma botoeira
						que é fixada na lateral da porta, e desta forma este item é
						praticamente indispensável, possibilitando a abertura de dentro de
						seu veículo, do interior de uma loja, possuindo tecnologia
						inclusive de se criar um acionamento remoto via internet, onde o
						proprietário não precisará nem ao menos estar no local para
						realizar a abertura da mesma, isso não é Incrível!</p>
				</div>
               <div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<img alt="central com controle"
						src="imagens\acessorios\central-controle.png" width="80%" height="auto"
						style="margin-top: 20px;">
				</div>
				


				<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<br>
					<h2 style="margin-top: 20px;">Aparelhos No-Break</h2>
					<p>Como tudo que é elétrico as Portas de Enrolar Automáticas também
						sofrem com as quedas de energia, e isso gera uma certa preocupação
						nas pessoas na hora de optar por comprar uma Porta de Aço
						Automática, porém esta preocupação não existe com os Aparelhos
						No-Break, que dão autonomia de várias aberturas e fechamento em
						caso de pane ou queda de energia no local. Trata-se de um aparelho
						equipado com excelentes baterias capazes de suprir as necessidades
						no Automatizador até que a energia do local seja reestabelecida.</p>
				</div>
      <div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<img alt="bateria nobreak"
						src="imagens\acessorios\bateria-nobreak.png" width="50%" height="auto"
						style="margin-top: 20px;margin-left: 20px;">
				</div>
				
				<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<br>
					<h2 style="margin-top: 20px;">Trava Anti-Queda.</h2>


					<p>Trava feita especialmente para segurar a porta em caso de quebra
						da corrente ou qualquer dano que possa comprometer as funções do
						sistema de transmissão das engrenagens do motor. Este equipamento
						impede a queda da porta evitando possíveis acidentes, protegendo
						pessoas, carros ou animais que possam estar sob a porta. Este
						sistema mecânico reconhece qualquer variação brusca na rotação do
						eixo que enrolador a porta e faz o travamento imediato de uma
						forma segura, impedindo que a porta desça violentamente em direção
						ao chão.</p>
				</div>

      <div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<img alt="anti-quedas"
						src="imagens\acessorios\anti-queda.png" width="90%" height="auto"
						style="margin-top: 20px;margin-left: 20px;">
				</div>
				

				<div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<br>
					<h2 style="margin-top: 20px;">Sensores Infravermelho</h2>
					<p>Os Sensores Infravermelhos são amplamente utilizados em várias
						aplicações, sejam elas industriais, comerciais ou até mesmo
						residenciais, e com as Portas de Enrolar Automáticas não podia ser
						diferente. Visando o bem-estar e a segurança dos clientes, os
						Sensores Infravermelho foram desenvolvidos para interromper o
						funcionamento da porta no exato momento que detectar qualquer
						objeto que esteja sob a direção da porta. São instalados um de
						frente para o outro criando uma espécie de linha invisível no vão
						de abertura da porta, que é reconhecida por esses aparelhos,
						emitindo um pulso que interfere na corrente elétrica do
						Automatizador, forçando sua parada instantânea, lembrando que
						existem alguns modelos que além de pararem o funcionamento, ainda
						invertem a direção da porta, fazendo com que ela volte a enrolar
						caso esteja descendo.</p>
				</div>
      <div class="col-md-6 col-xs-6 col-sm-12" style="background-color:transparent;">
					<img alt="infra-vermelho"
						src="imagens\acessorios\infravermelho.png" width="80%" height="auto"
						style="margin-top: 20px;margin-left: 20px;">
				</div>
			</div>
		</div>
		</div>
		<br> <br>
	</div>
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array( '5511959681162', '5511959065418','5511949523408','5511953536427','5511953874598'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>