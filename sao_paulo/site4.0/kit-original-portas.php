<?php

require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script src="https://www.googletagmanager.com/gtag/js?id=UA-116556154-1"></script>
<script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-116556154-1');</script>
<title>Kit Porta de A&ccedil;o Autom&aacute;ticas - Original Portas</title>
<base>
<meta name="description"
	content="A Original Portas tem o orgulho de oferecer aos seus cliente,
							parceiros e colaboradores, sempre o que h&aacute; de melhor no mercado
							de portas de enrolar, tanto manuais quanto autom&aacute;ticas. Somente
							uma empresa parceira, s&oacute;lida no mercado a mais de 10 anos e com
							sede pr&oacute;pria, ousaria defender e respeitar os seus direitos de
							consumidor. Desde o in&iacute;cio nos mantemos em constante preocupa&ccedil;&atilde;o
							com a qualidade dos produtos e servi&ccedil;os prestados, garantia
							comprovada atrav&eacute;s das Certifica&ccedil;&otilde;es ISO 9001 e InMetro.
							Trabalhamos sabendo que a Seguran&ccedil;a e a Satisfa&ccedil;&atilde;o dos nossos
							clientes deve estar sempre em primeiro lugar, e acreditamos que
							n&atilde;o somos apenas fabricantes de portas, aqui na Original, temos a
							certeza de que participamos da concretiza&ccedil;&atilde;o de projetos e da
							realiza&ccedil;&atilde;o de sonhos.">
<meta name="keywords"
	content="Original Portas, kit porta de a&ccedil;o automatica de enrolar, porta, portas,kit,automaticas.">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="S&atilde;o Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="kit-original-portas.php">
<meta name="author" content="tworock">
<link rel="shortcut icon" href="/site4.0/imagens/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Kit Porta de A&ccedil;o Autom&aacute;ticas - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/kit-porta-automatica.png">
<meta property="og:url" content="kit-original-portas.php">
<meta property="og:description"
	content="A Original Portas tem o orgulho de oferecer aos seus cliente,
							parceiros e colaboradores, sempre o que h&aacute; de melhor no mercado
							de portas de enrolar, tanto manuais quanto autom&aacute;ticas. Somente
							uma empresa parceira, s&oacute;lida no mercado a mais de 10 anos e com
							sede pr&oacute;pria, ousaria defender e respeitar os seus direitos de
							consumidor. Desde o in&iacute;cio nos mantemos em constante preocupa&ccedil;&atilde;o
							com a qualidade dos produtos e servi&ccedil;os prestados, garantia
							comprovada atrav&eacute;s das Certifica&ccedil;&otilde;es ISO 9001 e InMetro.
							Trabalhamos sabendo que a Seguran&ccedil;a e a Satisfa&ccedil;&atilde;o dos nossos
							clientes deve estar sempre em primeiro lugar, e acreditamos que
							n&atilde;o somos apenas fabricantes de portas, aqui na Original, temos a
							certeza de que participamos da concretiza&ccedil;&atilde;o de projetos e da
							realiza&ccedil;&atilde;o de sonhos.">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="/site4.0/bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="/site4.0/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/site4.0/bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type='text/css' href="/site4.0/css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">
function disableselect(e){ 
return false 
} 
function reEnable(){ 
return true 
} 
//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="/site4.0/js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript"
	src="/site4.0/js/jquery.slicknav.js"></script>
<script defer src="/site4.0/js/geral.js"></script>
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro4">
				<img src="imagens/kit-original-portas-11.png" width="100%">
			</div>
		</div>
		<div class="container">
			<div class="eletro_div">
				<div class="row">
					<div class="col-md-12">
						<h1>Kit Porta de Enrolar Autom&aacute;tica</h1>
						<hr style="width: auto; height: 2px; background-color: #ccc;">
						<p>É A Original Portas tem o orgulho de oferecer aos seus cliente,
							parceiros e colaboradores, sempre o que h&aacute; de melhor no mercado
							de portas de enrolar, tanto manuais quanto autom&aacute;ticas. 
							</p><p>
							Somente uma empresa parceira, s&oacute;lida no mercado a mais de 10 anos e com
							sede pr&oacute;pria, ousaria defender e respeitar os seus direitos de
							consumidor. 
						    Desde o in&iacute;cio nos mantemos em constante preocupa&ccedil;&atilde;o
							com a qualidade dos produtos e servi&ccedil;os prestados, garantia
							comprovada atrav&eacute;s das Certifica&ccedil;&otilde;es ISO 9001 e InMetro.
							</p><p>
							Trabalhamos sabendo que a Seguran&ccedil;a e a Satisfa&ccedil;&atilde;o dos nossos
							clientes deve estar sempre em primeiro lugar, e acreditamos que
							n&atilde;o somos apenas fabricantes de portas, aqui na Original, temos a
							certeza de que participamos da concretiza&ccedil;&atilde;o de projetos e da
							realiza&ccedil;&atilde;o de sonhos.</p>
					</div>

					<div class="col-md-12 col-xs-12 col-sm-12">
						<h2>ITENS QUE FAZEM PARTE DO KIT ORIGINAL PORTAS</h2>
						<p>
							Toda Porta de A&ccedil;o Autom&aacute;tica possui uma s&eacute;rie de itens
							Obrigat&oacute;rios para ser considerada uma Porta de A&ccedil;o Autom&aacute;tica, o
							nosso Kit Original Portas foi desenvolvido a partir desses itens,
							para facilitar a sua compra e tamb&eacute;m evitar que por descuido ou
							esquecimento sua porta fique incompleta por conta da falta de
							algum item essencial.</p><p> O Kit Padr&atilde;o da Original Portas &eacute; composto
							por:<br>
							<br> - Eixo e Testeiras;<br> - Guias ou Trilhos Laterais;<br> -
							L&acirc;minas da porta;<br> - Soleira;<br> - Automatizador.<br>
						</p>
						<p>Abaixo listamos esses itens individualmente, para que possa
							entender a fun&ccedil;&atilde;o e o mecanismo de funcionamento de sua porta de
							cada um deles, tendo a certeza de saber exatamente porque voc&ecirc;
							necessita comprar este item e para que ele serve.</p>
						<br>
					</div>



					<div class="col-md-6 col-xs-12 col-sm-12">
						<br>
						<h2 style="margin-top: 20px;">EIXO COMPLETO</h2>

						<p>Buscando melhorar a qualidade de trabalho doso nossos parceiros
							e colaboradores, n&oacute;s da Original Portas disponibilizamos a
							possibilidade de adquirir o eixo completamente montado, com as
							testeiras soldadas, fura&ccedil;&atilde;o e tudo j&aacute; alinhado no esquadro com
							travas de seguran&ccedil;a contra desalinhamento. Com isso voc&ecirc; ter&aacute; uma
							economia significativa do seu tempo instala&ccedil;&atilde;o, agilizando seus
							processos, pois ser&aacute; necess&aacute;rio apenas nivela-lo e fixa-lo no
							lugar. E isso lhe trar&aacute; a possibilidade de realizar mais
							instala&ccedil;&otilde;es por dia, aumentando bastante a sua produtividade, o
							que resultar&aacute; em maiores lucros no final do m&ecirc;s.</p>
					</div>

					<div class="col-sm-6 col-md-6 col-xl-6" style="margin-top: 20px;">
						<br><img
							alt="eixo completo porta de enrolar"
							src="imagens\acessorios\eixo-completo.png" width="100%"
							height="auto" style="margin-top: 20px;"> <br>
					</div>
					

					<div class="col-md-6 col-xs-6 col-sm-12">
						<br>
						<h2 style="margin-top: 50px;">GUIAS ou TRILHOS LATERAIS</h2>
						<p>As guias acomodam e fazem o travamento das l&acirc;minas nas
							laterais, possuem espessuras padronizadas e a sua escolha &eacute;
							determinada atrav&eacute;s do comprimento total da porta. Trabalhamos
							com bobinas em A&ccedil;o temperado, o que lhe garante mais seguran&ccedil;a e
							confiabilidade na hora de fabricarmos as suas guias, onde
							executamos um processo rigoroso de qualidade, para que suas guias
							sejam feitas sob medida, para lhe garantir o m&aacute;ximo de precis&atilde;o.
						</p>
					</div>

					<div class="col-sm-6 col-md-6 col-xl-6" style="margin-top: 50px;">
						<br> <img alt="guia telescopica"
							src="imagens\acessorios\guia-telescopica.png" width="40%"> <img
							alt="porta-enrolar-aco-comercial"
							src="imagens\acessorios\guia-sobreposta.png" width="30%"> <img
							alt="porta-enrolar-aco-comercial"
							src="imagens\acessorios\guia-comum.png" width="20%">

					</div>

					<div class="col-md-6 col-xs-6 col-sm-12">
						<h2 style="margin-top: 40px;">L&Acirc;MINAS</h2>
						<p>Totalmente de fabrica&ccedil;&atilde;o pr&oacute;pria, seguimos todas as normas de
							seguran&ccedil;a, altos padr&otilde;es de qualidade e acabamento. As L&acirc;minas
							s&atilde;o classificadas por espessuras, e sendo respons&aacute;veis por mais
							de 60% da sua seguran&ccedil;a de uma porta de enrolar autom&aacute;tica,
							devamos levar em conta a sua m&aacute;xima import&acirc;ncia, para produzirmos
							corretamente e com alta qualidade, dimensionando corretamente
							este item.
					
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12">

						<img
							alt="lamina porta de a&ccedil;o" src="imagens\lamina404.png"
							width="100%" height="auto" style="margin-top: 20px;">
					</div>




					<div class="col-md-12 col-xs-12 col-sm-12">
						<br>
						<h2 style="margin-top: 20px;">SOLEIRA</h2>

						<p>
							A soleira &eacute; aquela &uacute;ltima pe&ccedil;a colocada depois das l&acirc;minas
							pr&oacute;xima ao ch&atilde;o para fazer o fechamento de sua porta. Existem
							v&aacute;rios tipos de Soleiras no mercado, algumas mais simples outras
							j&aacute; mais elaboradas. As Soleiras s&atilde;o feitas com Chapa dobrada em
							âLâ, dobra Refor&ccedil;ada em âTâ ou âConstru&ccedil;&atilde;o Tubularâ, s&atilde;o
							respons&aacute;veis pelo fechamento correto da porta &eacute; mais um
							importante item de seguran&ccedil;a. Cada uma delas possui uma fun&ccedil;&atilde;o e
							uma determinada resist&ecirc;ncia tamb&eacute;m, a seguir relacionamos os
							tipos que trabalhamos e suas especifica&ccedil;&otilde;es para que possa
							escolher com seguran&ccedil;a. <br>
							</div>
							
					<div class="col-md-6 col-xs-6 col-sm-12">
						<br>
						<h2 style="margin-top: 20px;">Soleiras em Chapa L</h2>
						<p>As Soleiras dobradas em âLâ s&atilde;o as mais simples pois foram a
							primeira a ser inventada, consequentemente s&atilde;o tamb&eacute;m as mais
							fr&aacute;geis, tecnicamente n&atilde;o s&atilde;o apropriadas para suportar esfor&ccedil;os,
							e assim, n&atilde;o h&aacute; um dimensionamento que garanta seu uso com
							seguran&ccedil;a.</p>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12">
                                <img
								alt="Soleira em L"
								src="imagens\acessorios\soleira-l.png"
								width="100%" height="auto" style="margin-top: 20px;">
					</div>
					<div class="col-md-6 col-xs-12 col-sm-12">
						<br>
						<h2 style="margin-top: 20px;">Soleiras com dobra Refor&ccedil;ada em âTâ</h2>
						<p>As Soleiras com dobra Refor&ccedil;ada em âTâ foram desenvolvidas a
							partir do modelo com tradicional em âLâ visando aumentar sua
							seguran&ccedil;a e durabilidade, onde atrav&eacute;s de estudos de esfor&ccedil;os e
							cargas, chegou-se Ã  um modelo em âTâ, que possuem maior
							seguran&ccedil;a. Seu formato duplo aplica uma for&ccedil;a de resist&ecirc;ncia
							contra alavancas e esfor&ccedil;os, atendendo portas de at&eacute; 6M sem
							necessitar do refor&ccedil;o inferior, que ainda pode ser feito atrav&eacute;s
							de um tubo metalon ou cantoneira soldada na parte inferior da
							Soleira.</p>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12">
						<img alt="soleira em T"
							src="imagens\acessorios\soleira-t.png" width="100%" height="auto"
							style="margin-top: 20px;">
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12">
						<h2 style="margin-top: 20px;">Soleira em Constru&ccedil;&atilde;o Tubular</h2>

						<p>As Soleiras Tubulares s&atilde;o de fato superiores e n&atilde;o h&aacute; como
							duvidar disto! Sua estrutura com tubos de a&ccedil;o, s&atilde;o dimensionadas
							para garantir alta resist&ecirc;ncia, possuem ainda um excelente
							acabamento, unindo beleza, seguran&ccedil;a e qualidade. A Soleira
							Tubular &eacute; constitu&iacute;da por uma L&acirc;mina na parte superior, um tubo
							metalon soldado em sua parte inferior e ainda por traz &eacute; colocado
							mais um tubo de metalon de a&ccedil;o para dar um refor&ccedil;a extra contra
							alavancas ou tentativas de arrombamentos, sendo assim o modelo
							mais resistente e eficiente dispon&iacute;vel atualmente.</p>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-12">
						 <img alt="soleira tubular"
							src="imagens\acessorios\soleira-tubular.png" width="100%"
							height="auto" style="margin-top: 20px;">
					</div>
				
				<div class="col-md-6 col-xs-6 col-sm-12">
					<br>
					<h2 style="margin-top: 20px;">AUTOMATIZADOR</h2>
					<p>O Automatizador &eacute; o cora&ccedil;&atilde;o de uma Porta de A&ccedil;o Autom&aacute;tica, pois
						&eacute; respons&aacute;vel por sua abertura e fechamento, sendo assim o item
						principal, pois sem ele a porta se tornaria manual, necessitando
						de molas para auxiliar sua abertura. O Automatizador &eacute; calculado
						de acordo com o peso total da porta e o fluxo de trabalho, levando
						em conta a quantidade de aberturas e fechamentos, s&atilde;o separados em
						baixo, m&eacute;dio e auto fluxo e possuem um fluxo de trabalho
						especificado pelo fabricante. For&ccedil;ar o Automatizador al&eacute;m do
						especificado pelo fabricante reduz gravemente o tempo de vida
						&uacute;til, esse desgaste excessivo das pe&ccedil;as e dos componentes
						eletrÃ´nicos, geram diversos danos que provocam a queima de sua
						m&aacute;quina em curto prazo, por isso &eacute; muito importante seleciona-lo
						corretamente com a certeza de estar exagerando, o que gera custos
						desnecess&aacute;rios. Trabalhamos com o melhor do mercado, somos
						distribuidores autorizados dos AUTOMATIZADORES MEGATRON, s&atilde;o
						motores robustos e fortes, o que lhe garante muito mais qualidade
						para suas instala&ccedil;&otilde;es, levando em considera&ccedil;&atilde;o de que todos os
						seus componentes de tra&ccedil;&atilde;o s&atilde;o feitos em A&ccedil;o de alta resist&ecirc;ncia,
						contando ainda com sistema de freio embutido para fazer o
						travamento de sua porta e um duplo sistema de fim de curso,
						pensados cuidadosamente na preven&ccedil;&atilde;o de poss&iacute;veis acidentes em
						caso de falhas el&eacute;tricas.</p>
				</div>
                <div class="col-md-6 col-xs-6 col-sm-12">
						<img alt="motor automatizador"
							src="imagens\acessorios\motor-automatizador.png" width="100%"
							height="auto" style="margin-top: 80px;">
				</div>
				
				
								<hr style="width: 100%; height: 2px; background-color: #ccc;">
				
				<div class="col-md-12 col-xs-12 col-sm-12">									
				
					<br>
					<h2 style="margin-top: 20px;">ITENS OPCIONAIS E ACESSÃRIOS.</h2>
					<p>Existem ainda v&aacute;rios itens opcionais e acess&oacute;rios que podem
						fazer parte de sua Porta sendo inclu&iacute;dos individualmente, esses
						itens visam aumentar o conforto, a seguran&ccedil;a e a durabilidade de
						sua porta.</p>
				</div>
												<hr style="width: 100%; height: 2px; background-color: #ccc;">
				
				<div class="col-md-6 col-xs-6 col-sm-12">
					<br>
					<h2 style="margin-top: 20px;">Tubos de Afastamento.</h2>
					<p>QOs tubos de afastamento s&atilde;o utilizados para alinhar a dire&ccedil;&atilde;o
						de descida da porta com as guias, e tamb&eacute;m visa aliviar o esfor&ccedil;o
						de carga que o Automatizador ter&aacute; de realizar para fazer a
						abertura de sua porta. Geralmente s&atilde;o usados tubos de metalon de
						a&ccedil;o resistente que podem ser soldados diretamente nas guias ou
						parafusados atrav&eacute;s de âorelhinhasâ furadas. Os Tubos de
						Afastamento tamb&eacute;m podem ser personalizados com dupla fura&ccedil;&atilde;o para
						embutir os parafusos ou com cantoneiras laterais para fixa&ccedil;&atilde;o, que
						poder&aacute; ser feita atrav&eacute;s de solda ou parafusos fixadores
						âparaboltâ.</p>

				</div>
    <div class="col-md-6 col-xs-6 col-sm-12">
						<img alt="tubo de afastamento"
							src="imagens\acessorios\tubo-afastamento.png" width="100%"
							height="auto" style="margin-top: 20px;">
				</div>
				
				
				<div class="col-md-6 col-xs-6 col-sm-12">
					<br>
					<h2 style="margin-top: 20px;">Trava L&acirc;minas.</h2>
					<p>Os Trava L&acirc;minas da Original Portas s&atilde;o confeccionados em
						Pol&iacute;mero de Alta Resist&ecirc;ncia, s&atilde;o utilizados para fazer o
						travamento de sua Porta de Enrolar Autom&aacute;tica no sentido
						horizontal, impedindo o deslocamento das L&acirc;minas, o que causaria o
						seu desalinhamento, poss&iacute;vel entrave das L&acirc;minas no interior das
						Guias Laterais e consequente quebra de sua Porta. Al&eacute;m do
						Travamento das L&acirc;minas, outra fun&ccedil;&atilde;o important&iacute;ssima dos Trava
						L&acirc;minas &eacute; reduzir ao m&aacute;ximo os ru&iacute;dos provenientes atrito entre a
						ponta das L&acirc;minas e o interior das Guias Laterais, proporcionando
						um deslizar macio e suave, o que gera a redu&ccedil;&atilde;o do ru&iacute;do, e
						automaticamente mais uma forma de Lubrifica&ccedil;&atilde;o para sua porta ter
						mais efici&ecirc;ncia, qualidade e consequentemente Maior Durabilidade.

					</p>
				</div>
				<div class="col-md-6 col-xs-6 col-sm-12">
					<img alt="trava laminas"
						src="imagens\acessorios\trava-lamina.png" width="100%" height="auto"
						style="margin-top: 20px;">
				</div>

				<div class="col-md-6 col-xs-6 col-sm-12">
					<br>
					<h2 style="margin-top: 20px;">Fita Auto Lubrificante.</h2>
					<p>As Fitas Auto Lubrificantes s&atilde;o colocadas nas Guias Laterais e
						servem para aliviar o atrito entre as L&acirc;minas e o interior das
						Guias, levando em considera&ccedil;&atilde;o que as Portas de Enrolar
						Autom&aacute;ticas n&atilde;o usam graxa nem &oacute;leo dentro de suas Guias, elas
						tamb&eacute;m funcionam como lubrificantes que proporcionam o perfeito
						deslizamento das L&acirc;minas.</p>
				</div>

					<div class="col-md-6 col-xs-6 col-sm-12">
					<img alt="fita auto lubrificante"
						src="imagens\acessorios\fita-lubrificante.png" width="70%" height="auto"
						style="margin-top: 20px;">
				</div>
				
				
				<div class="col-md-6 col-xs-6 col-sm-12">
					<br>
					<h2 style="margin-top: 20px;">Borracha de Veda&ccedil;&atilde;o para Soleira</h2>
					<p>&Eacute; um acess&oacute;rio muito &uacute;til que realiza a veda&ccedil;&atilde;o daquele espa&ccedil;o
						que pode existir entre o ch&atilde;o e a Soleira, devido a desn&iacute;veis e
						ondula&ccedil;&otilde;es no piso. Com esta borracha, al&eacute;m do fechamento perfeito
						de sua porta, tamb&eacute;m ir&aacute; impedir a passagem de p&oacute;, sujeira,
						insetos e outras objetos indesej&aacute;veis.</p>
				</div>
               <div class="col-md-6 col-xs-6 col-sm-12">
					<img alt="borracha vedacao soleira"
						src="imagens\acessorios\borracha-vedacao.png" width="70%" height="auto"
						style="margin-top: 20px;">
				</div>
				
				<div class="col-md-6 col-xs-6 col-sm-12">
					<br>
					<h2 style="margin-top: 20px;">Central de Controle Remoto</h2>
					<p>As Centrais de Controle Remoto para Portas de A&ccedil;o Autom&aacute;ticas
						est&atilde;o se tornando o item quase obrigat&oacute;rio devido a import&acirc;ncia de
						sua utilidade, que gera muita praticidade e total conforto.
						Acionar sua porta a dist&acirc;ncia elimina diversos a&ccedil;&otilde;es e opera&ccedil;&otilde;es
						que normalmente deveriam ser executadas mesmo com uma Porta
						Autom&aacute;tica, pois seu acionamento comum &eacute; atrav&eacute;s de uma botoeira
						que &eacute; fixada na lateral da porta, e desta forma este item &eacute;
						praticamente indispens&aacute;vel, possibilitando a abertura de dentro de
						seu ve&iacute;culo, do interior de uma loja, possuindo tecnologia
						inclusive de se criar um acionamento remoto via internet, onde o
						propriet&aacute;rio n&atilde;o precisar&aacute; nem ao menos estar no local para
						realizar a abertura da mesma, isso n&atilde;o &eacute; Incr&iacute;vel!</p>
				</div>
               <div class="col-md-6 col-xs-6 col-sm-12">
					<img alt="central com controle"
						src="imagens\acessorios\central-controle.png" width="80%" height="auto"
						style="margin-top: 20px;">
				</div>
				


				<div class="col-md-6 col-xs-6 col-sm-12">
					<br>
					<h2 style="margin-top: 20px;">Aparelhos No-Break</h2>
					<p>Como tudo que &eacute; el&eacute;trico as Portas de Enrolar Autom&aacute;ticas tamb&eacute;m
						sofrem com as quedas de energia, e isso gera uma certa preocupa&ccedil;&atilde;o
						nas pessoas na hora de optar por comprar uma Porta de A&ccedil;o
						Autom&aacute;tica, por&eacute;m esta preocupa&ccedil;&atilde;o n&atilde;o existe com os Aparelhos
						No-Break, que d&atilde;o autonomia de v&aacute;rias aberturas e fechamento em
						caso de pane ou queda de energia no local. Trata-se de um aparelho
						equipado com excelentes baterias capazes de suprir as necessidades
						no Automatizador at&eacute; que a energia do local seja reestabelecida.</p>
				</div>
      <div class="col-md-6 col-xs-6 col-sm-12">
					<img alt="bateria nobreak"
						src="imagens\acessorios\bateria-nobreak.png" width="50%" height="auto"
						style="margin-top: 20px;margin-left: 20px;">
				</div>
				
				<div class="col-md-6 col-xs-6 col-sm-12">
					<br>
					<h2 style="margin-top: 20px;">Trava Anti-Queda.</h2>


					<p>Trava feita especialmente para segurar a porta em caso de quebra
						da corrente ou qualquer dano que possa comprometer as fun&ccedil;&otilde;es do
						sistema de transmiss&atilde;o das engrenagens do motor. Este equipamento
						impede a queda da porta evitando poss&iacute;veis acidentes, protegendo
						pessoas, carros ou animais que possam estar sob a porta. Este
						sistema mec&acirc;nico reconhece qualquer varia&ccedil;&atilde;o brusca na rota&ccedil;&atilde;o do
						eixo que enrolador a porta e faz o travamento imediato de uma
						forma segura, impedindo que a porta des&ccedil;a violentamente em dire&ccedil;&atilde;o
						ao ch&atilde;o.</p>
				</div>

      <div class="col-md-6 col-xs-6 col-sm-12">
					<img alt="anti-quedas"
						src="imagens\acessorios\anti-queda.png" width="90%" height="auto"
						style="margin-top: 20px;margin-left: 20px;">
				</div>
				

				<div class="col-md-6 col-xs-6 col-sm-12">
					<br>
					<h2 style="margin-top: 20px;">Sensores Infravermelho</h2>
					<p>Os Sensores Infravermelhos s&atilde;o amplamente utilizados em v&aacute;rias
						aplica&ccedil;&otilde;es, sejam elas industriais, comerciais ou at&eacute; mesmo
						residenciais, e com as Portas de Enrolar Autom&aacute;ticas n&atilde;o podia ser
						diferente. Visando o bem-estar e a seguran&ccedil;a dos clientes, os
						Sensores Infravermelho foram desenvolvidos para interromper o
						funcionamento da porta no exato momento que detectar qualquer
						objeto que esteja sob a dire&ccedil;&atilde;o da porta. S&atilde;o instalados um de
						frente para o outro criando uma esp&eacute;cie de linha invis&iacute;vel no v&atilde;o
						de abertura da porta, que &eacute; reconhecida por esses aparelhos,
						emitindo um pulso que interfere na corrente el&eacute;trica do
						Automatizador, for&ccedil;ando sua parada instant&acirc;nea, lembrando que
						existem alguns modelos que al&eacute;m de pararem o funcionamento, ainda
						invertem a dire&ccedil;&atilde;o da porta, fazendo com que ela volte a enrolar
						caso esteja descendo.</p>
				</div>
      <div class="col-md-6 col-xs-6 col-sm-12">
					<img alt="infra-vermelho"
						src="imagens\acessorios\infravermelho.png" width="80%" height="auto"
						style="margin-top: 20px;margin-left: 20px;">
				</div>
			</div>
		</div>
		</div>
		<br> <br>
	</div>
<?php echo $footer;?>
</body>