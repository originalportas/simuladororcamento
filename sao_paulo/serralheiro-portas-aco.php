<?php require 'main.php';
require 'footer.php';?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
<title>Serralheiro descontos especiais, porta, portas de aço - Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description"
	content="Descontos em Portas de aço automáticas e manuais, kit para portas de aço, soleiras, protetor de soleira, guias e tubos de afastamento, portas transvison, portas meia cana, portinholas, alçapões, trava lâminas , produtos da marca Megatron (automatizadores para portas de aço automaticas e manuais, Nobreak, Central para automatizadores, controle remoto, testeiras, botoeiras), lâminas perfuradas, lâminas fechadas ">
<meta name="keywords"
	content="Preços baixos, baixo, serralheiro, desconto, descontos,, preço, parceiro, preço porta de enrolar, porta de aço, portas, vantagem, beneficios, beneficios, a original portas é confiavel?, ">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="serralheiro.php">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title" content=">Serralheiro descontos especiais, porta, portas de aço - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url" content="serralheiro.php">
<meta property="og:description" content="Descontos em Portas de aço automáticas e manuais, kit para portas de aço, soleiras, protetor de soleira, guias e tubos de afastamento, portas transvison, portas meia cana, portinholas, alçapões, trava lâminas , produtos da marca Megatron (automatizadores para portas de aço automaticas e manuais, Nobreak, Central para automatizadores, controle remoto, testeiras, botoeiras), lâminas perfuradas, lâminas fechadas ">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">

<!-- ----------------logo rede social----------------->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script> 
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script> 

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
  <div class="row">
    <div id="ban_serra"><img src="imagens/cump_maos.png" width="100%"height="auto">
    </div>
  </div>
</div>
<div class="container">
	<div class="serra_div">
		<div class="row">
		 <div class="col-md-12">
		 <h1>Serralheiro seja nosso parceiro e adquira descontos especiais</h1>
		 <p>Amigo serralheiro , não perca tempo e se cadastre como nosso parceiro e tenha muitos beneficios, beneficios que só a <strong>Original Portas</strong> dará a você.<br>
		 Seja nosso parceiro e conte sempre com preços diferenciados para portas de enrolar automáticas ,portas de aço manuais, automatizadores e acessórios</p>
     </div>
     <div class="col-md-12">
		 <h2><i class="fa fa-address-book-o"></i>&nbsp; requesitos para obter descontos</h2>
		 <p>Para contar com os todos beneficios de um parceiro <strong>Original Portas</strong>, você precisa:
		 <p>
		 1º Possuir Cadastro Nacional de Pessoas Juridicas (CNPJ) que esteja ativo.<br>
		 2º Realizar o cadastro  via telefone, email ou pessoalmente em nossa fábrica.<br>
		 3º Adquirir sempre os nossos produtos.</p>
	</div><br>
		 <div class="col-md-12">
		 <h2><i class="fa fa-gift"></i>&nbsp; Beneficos da parceria</h2>
		 <p><strong>- Descontos em:</strong> Portas de aço automáticas e manuais, kit para portas de aço, soleiras, protetor de soleira, guias e tubos de afastamento, portas transvison, portas meia cana, portinholas, alçapões, trava lâminas , produtos da marca Megatron (automatizadores para portas de aço automaticas e manuais, Nobreak, Central para automatizadores, controle remoto, testeiras, botoeiras), lâminas perfuradas, lâminas fechadas </p>
		 <p><strong>- Indicações:</strong> Indicamos os parceiros aos nossos clientes para prestação de serviços, indicamos sempre o parceiro que está mais próximo ao cliente</p> 
		 <p><strong>- Brindes sortidos:</strong> Mini-Lâminas, Camisas, Canetas, Sacolas (Todos os brindes possuem a logo marca Original Portas sendo que a sua aquisição vai depender dos itens que estão disponiveis em estoque).</p>
		 <p><strong>- E-mails promocionais:</strong> Sempre que houver promoções realizadas pela <strong>Original Portas</strong>, enviaremos e-mails a todos os nossos parceiros.  
		   </p>
		   </div>
		</div>
    </div>
</div>
<br>
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array( '5511959681162', '5511959065418','5511949523408','5511953536427','5511953874598'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>		

<?php echo $footer;?>
</body>
</html>