<?php

require 'main.php';
require 'footer.php';
?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
<title>Motores para postas de Aço de enrolar- Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description"
	content="De simples funcionamento, esse tipo de porta exige apenas um
						trilho para que possa correr, além de um motor para porta de
						enrolar. Para conservar seu motor para porta de enrolar, você
						precisará tomar alguns cuidados, o que inclui uma limpeza
						frequente tanto na porta quando nos trilhos e no motor para porta
						de enrolar. Isso garante que resíduos não grudem nas superfícies,
						fazendo que a ação da porta fique prejudicada – e sobrecarregando
						o motor. Entenda: quando pequenos objetos aderem à porta, eles
						podem piorar o atrito e prejudicar a abertura e fechamento. Isso
						faz com que o motor para porta de enrolar fique sobrecarregado e
						quebre com frequência.">
<meta name="keywords"
	content="Motores para portas de enrolar manual,motores para portas de enrolar automáticas, motor para portas de shopping, lojas e industrias automatizador para portas de enrolar manuais e automáticas,automatizador megatron, motor ac 200, motor ac 300, motor ac 400, motor ac 500, motor ac 600, motor ac 700, motor ac 800, motor ac 900, motor ac 1000,motor ac 1200">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="motor-automatizador-porta-aco-manual">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="Motor automatizador para portas de aço manual- Original Portas">
<meta property="og:type" content="article">
<meta property="og:image"
	content="imagens/automatizador/automatizador-portas-enrolar-manual.png">
<meta property="og:url" content="motor-automatizador-porta-aco-manual">
<meta property="og:description"
	content="De simples funcionamento, esse tipo de porta exige apenas um
						trilho para que possa correr, além de um motor para porta de
						enrolar. Para conservar seu motor para porta de enrolar, você
						precisará tomar alguns cuidados, o que inclui uma limpeza
						frequente tanto na porta quando nos trilhos e no motor para porta
						de enrolar. Isso garante que resíduos não grudem nas superfícies,
						fazendo que a ação da porta fique prejudicada – e sobrecarregando
						o motor. Entenda: quando pequenos objetos aderem à porta, eles
						podem piorar o atrito e prejudicar a abertura e fechamento. Isso
						faz com que o motor para porta de enrolar fique sobrecarregado e
						quebre com frequência.">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"
	type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
} 

function reEnable(){ 
return true 
} 

//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>


	<div class="container">
		<div class="class_aut2">
			<div class="row">
				<div class="col-md-12" style="margin-top:50px;">
					<h1>Motor para Porta de Enrolar de Aço</h1>
					<p>Um modelo de porta bastante procurado é a porta de enrolar. Isso
						porque ela não ocupa muito espaço, pois pode ser otimizada ao ser
						aberta, enrolando uma lâmina de metal sobre a outra.</p>
					<p>De simples funcionamento, esse tipo de porta exige apenas um
						trilho para que possa correr, além de um motor para porta de
						enrolar. Para conservar seu motor para porta de enrolar, você
						precisará tomar alguns cuidados, o que inclui uma limpeza
						frequente tanto na porta quando nos trilhos e no motor para porta
						de enrolar. Isso garante que resíduos não grudem nas superfícies,
						fazendo que a ação da porta fique prejudicada – e sobrecarregando
						o motor. Entenda: quando pequenos objetos aderem à porta, eles
						podem piorar o atrito e prejudicar a abertura e fechamento. Isso
						faz com que o motor para porta de enrolar fique sobrecarregado e
						quebre com frequência.</p>
					<p>Além disso, uma pintura eletrostática vai deixar sua porta de
						aço mais bonita e protegida. A tinta eletrostática age como uma
						barreira, evitando, por exemplo, a corrosão. Vale lembrar que é
						fundamental que essa pintura seja feita por empresas
						especializadas.</p>
					<p>É importante ressaltar que a pintura ocorre apenas no portão,
						sendo que o motor para porta de enrolar permanecerá na sua cor
						original. Mas não se preocupe: como ele fica instalado de forma
						discreta, isso não afeta o design de sua porta de enrolar.</p>
					<h2>O motor para porta de enrolar pode tornar a sua porta mais
						eficiente?</h2>
					<p>A porta automática só é funcional quando o motor para porta de
						enrolar é adequado a ela. Isso porque ele precisa suportar o seu
						peso e conseguir fazer com que ela suba e desça com facilidade.
						Quando ele não é adequado, o que ocorre é que abertura e
						fechamento da porta podem ficar mais lentos, fazendo com que o
						motor para porta de enrolar fique sobrecarregado e ele chegue até
						a parar de funcionar. Por isso, para garantir o funcionamento
						perfeito do seu portão eletrônico e fazer com que o motor para
						porta de enrolar fique mais eficiente, você deverá escolhê-lo com
						cuidado e optar sempre por empresas fabricantes especializadas,
						como a Original Portas.</p>

					<p>Existem dois tipos de automatizadores para portas de enrolar.</p>
					<div class="container">
						<div class="row">
							<div id="class_aut">
								<h2>Motores para portas de aço Manuais</h2>
								<a href="motor-automatizador-porta-aco-manual.php"><img
									src="imagens/automatizador/automatizador-portas-enrolar-manual.png"
									title="automatizador porta de enrolar manual" width="100%"
									height="auto"></a>
								<h2>Motores para portas aço Automáticas</h2>
								<a href="motor-automatizador-porta-aco-automatica.php"><img
									src="imagens/automatizador/automatizador-portas-enrolar-aco.png"
									title="automatizador porta de enrolar automática" width="100%"
									height="auto"></a>
							</div>
						</div>
					</div>

<br/><br/>

					<p><strong>ATENÇÃO:</strong> Para saber qual tipo de motor atende a sua necessidade, entre
						em contato na nossa central de atendimento e fale com um de nossos
						representantes.</p>
<br/><br/>
				</div>

			</div>
		</div>
	</div>
<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array( '5511959681162', '5511959065418','5511949523408','5511953536427','5511953874598'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>
</html>