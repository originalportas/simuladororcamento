<?php
$main = '
<div class="parallelogram" id="barra_paralela">
<a href="tel:+551143861001"><i class="fa fa-phone" id="phone"></i></a>
</div>
<header>
 <div class="div_back">
    <div id="img_nav">
      <a href="index"><img alt="Original Portas" src="./imagens/logotipo_original_medium.png" style="width:300px"></a>
     </div>			
		<p><a href="tel:+551143861001">(11) 4386-1001</a></p>
       </div>
     <div class="wrapper">
		<nav id="menu">
         <ul>
		  <li><a href="index.php" title="Página inicial"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
			<li class="dropdown"><a href="./quem-somos.php" title="Sobre a Empresa Original Portas"><i class="fa fa-industry"></i> Empresa</a>
		      <ul class="sub-menu2">
			    <li><a href="./quem-somos.php" title="Quem Somos"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> Quem Somos</a></li>
			    <li><a href="./certificacao.php" title="Certificaç&atilde;o"><i class="fa fa-certificate"></i> Certificaç&atilde;o</a></li>
		      </ul>
			</li>
		   <li class="dropdown"><a href="./#busca1" title="Produtos Original Portas"><i class="fa fa-shopping-basket"></i> Produtos</a>
					<ul class="sub-menu2">
                    <li class="dropdown"><a href="./porta-enrolar-automatica.php" title="porta enrolar autom&aacute;tica"><i class="fa fa-plus-circle" aria-hidden="true"></i> Portas de Aço</a>
                              <ul class="sub-menu3">
                            <li><a href="./porta-enrolar-aco-industrial.php" title="portas de enrolar industrial"> Portas de aço Industrial</a></li>
							<li><a href="./porta-enrolar-aco-comercial.php" title="portas de enrolar comercial"> Portas de aço Comercial</a></li>
							<li><a href="./porta-enrolar-aco-residencial.php" title="portas de enrolar residencial"> Portas de aço Residencial</a></li>
                              </ul>
                            </li>
							

                            <li><a href="./kit-original-portas.php" title="kit portas de a&ccedil;o"><i class="fa fa-plus-circle" aria-hidden="true"></i> Kit portas autom&aacute;ticas</a></li>

                          <li class="dropdown"><a href="./fabricante-portas-rapidas.php" title="Portas R&aacute;pidas de Lona"><i class="fa fa-plus-circle" aria-hidden="true"></i> Portas R&aacute;pidas</a>
                             <ul class="sub-menu3">
                            <li><a href="./porta-rapida-enrolar.php" title="Porta R&aacute;pida Enrolar"> Modelo de Enrolar</a></li>
							<li><a href="./porta-rapida-dobravel.php" title="Porta R&aacute;pida Dobravél"> Modelo Dobr&aacute;vel</a></li>
                              </ul>
                            </li>
                          <li  class="dropdown"><a href="./fabricante-porta-seccionada.php" title="Fabricante Porta Seccionada"><i class="fa fa-plus-circle" aria-hidden="true"></i> Portas Seccionadas</a>
							     <ul class="sub-menu3">
                            <li><a href="./porta-seccionada-industrial.php" title="Porta Seccionada Industrial"> Modelo Industrial</a></li>
							<li><a href="./fabricante-porta-seccionada.php" title="Porta Seccionada Residencial"> Modelo Residencial</a></li>
                              </ul>
                            </li>
                      <li><a href="./pintura-eletrostatica.php" title="Pintura para portas de aço"><i class="fa fa-tint"  aria-hidden="true"></i> Pintura Eletrostática</a></li>
                          

                            <li class="dropdown"><a href="./motor-porta-enrolar-aco.php" title="Motores para portas de enrolar de aço"><i class="fa fa-gear"  aria-hidden="true"></i> Automatizadores</a>
                              <ul class="sub-menu3">
                                <li><a href="./motor-automatizador-porta-aco-automatica.php" title="Motor automatizador porta enrolar autom&aacute;tica">Motor p/ Porta de Aço Autom&aacute;tica</a></li>
                                <li><a href="./motor-automatizador-porta-aco-manual.php" title="Motor automatizador porta enrolar manual">Motor p/ Porta de A&ccedil;o Manual</a></li>
                                <li><a href="./automatizacao-porta-aco-preco.php" title="automatiza&ccedil;&atilde;o porta a&ccedil;o pre&ccedil;o">automatiza&ccedil;&atilde;o porta a&ccedil;o pre&ccedil;o</a></li>
                                <li><a href="./automatizacao-porta-aco.php" title="automatiza&ccedil;&atilde;o porta a&ccedil;o">automatiza&ccedil;&atilde;o porta a&ccedil;o</a></li>
                              </ul>
                            </li>
                            <li><a href="./fabricante-laminas-porta-aco.php" title="Fabricante l&acirc;minas portas de A&ccedil;o"><i class="fa fa-navicon"></i> L&acirc;minas p/ portas de a&ccedil;o</a></li>

                            <li><a href="./acessorios.php" title="Acessórios para portas aço"><i class="fa fa-cogs"  aria-hidden="true"></i> Acessórios para portas aço</a></li>
                            <li><a href="./niveladora-docas.php" title="Niveladora para docas"><i class="fa fa-cubes"></i> Niveladora p/ Docas</a></li>

                     </ul>


					<!--	
					<ul class="sub-menu2">
							<li><a href="./portas.php" title="portas de aço automatica"><img src="/site4.0/imagens/icone_porta_de_aco_p.png"> Portas de aço automática</a></li>
							<li><a href="./itens_opc.php" title="Itens Opcionais"><img src="/site4.0/imagens/icone_bandeira_p.png"> Itens Opcionais</a></li>
							<li><a href="./automatizadores.php" title="Automatizadores"><i class="fa fa-gear"></i> Automatizadores</a></li>
							<li><a href="./pintura-eletrostatica-porta-enrolar-aco.php" title="Pintura Eletrostática"><img src="/site4.0/imagens/icone_spray_gun_p.png">Pintura Eletrostática</a></li>
							<li><a href="./acessorios.php" title="Acessórios"><i class="fa fa-gears" "></i> Acessórios</a></li>
							<li><a href="./pecas_rep.php" title="Peças para reposiç&atilde;o"><i class="fa fa-gears" ></i> Peças para reposiç&atilde;o</a></li>
					  </ul>
                    </li> -->
					<li class="dropdown"><a href="https://goo.gl/photos/YTenbCXuHDhy9i97A"
						target="_blank" title="Galeria de Fotos - Original Portas"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Galeria</a>
						<ul class="sub-menu2">
						<li><a href="https://goo.gl/photos/YTenbCXuHDhy9i97A" title="Fotos de portas de aço"><i class="fa fa-camera"></i> Fotos</a></li>
						<li><a href="./video-como-montar-instalar-porta-aco-automatica.php" title="Video como montar e instalar portas de aço de enrolar"><span class="glyphicon glyphicon-facetime-video"  aria-hidden="true"></span> Videos</a></li>
						</ul>
					</li>
					<li class="dropdown"><a href="./contato-original-portas.php" title="Atendimento Original Portas" style="width: 120px;"><i class="fa fa-headphones" ></i> Atendimento</a>
					  <ul class="sub-menu2">
					      <li><a href="./contato-original-portas.php" title="Fale com a Original Portas" style="width: 180px;"><i class="fa fa-phone"></i> Contato</a></li>
					        <li><a href="./orcamento-porta-enrolar-automatica.php" title="Orçamentos  Original Portas" style="width: 180px;"><i class="fa fa-money"></i> Orçamento</a></li>
					       <li><a href="./localizacao-fabrica-de-portas.php" title="Localizaç&atilde;o Original Portas" style="width: 180px;"><i class="fa fa-map-marker"></i> Localizaç&atilde;o</a></li>
					     <li><a href="./serralheiro-portas-aco.php" title="Serralheiro seja um parceiro" style="width: 180px;"><i class="fa fa-handshake-o"></i> Serralheiro</a></li> 
					    <li><a href="./inscricao-curso.php" title="Inscrição Para Cursos Original Portas" style="width: 180px;"><i class="fa fa-graduation-cap"></i> Cursos</a></li>
					 <li><a href="./trab_con.php" title="Trabalhe com a Original Portas" style="width: 180px;"><i class="fa fa-group"></i> Trabalhe Conosco</a></li>

 </ul>
				</ul>
			</nav>
		</div>
    <hr id="hr">
</header>';
?>