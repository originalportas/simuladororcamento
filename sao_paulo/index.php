<?php
require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//PT_BR" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<title>Home - Portas de enrolar automáticas - Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="description"
	content="Na Original Portas você encontra tudo para Portas de enrolar de aço automáticas com sede em  São Paulo - Guarulhos, fabrica de portas,temos portas de loja, para, portas de shopping, portas de rolo, preço baixo, descontos para serralheiro portas de aço,  automatizadores com nobreaks, motor para portões, botoeiras, portas transvision, portas perfuradas, portas para hangar, hangares, aeroportos, portas para trailer, banca de jornal, porta para container, lanchonete, meia cana, portas industriais, controle para portão, lâminas, pintura eletrostatica, contamos com serralhria própria">
<meta name="keywords"
	content="Original Portas, porta, portas de aço, aco, enrola, rolo, bandeira, cobre rolo, serralheria, serralheiro, portas aço automáticas, portas de enrolar , porta de loja, porta de shopping,porta de rolo, preço, descontos, motor portão, megatron, botoeiras,porta perfurada,furo, meia cana,transvision, frete, entrega de portas,pintura eletrostatica, portinhola, alçapão, Guarulhos, São Paulo, SP, seja revendedor">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximun-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General"><?php
require 'main.php';
require 'footer.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<title>Home - Portas de enrolar automáticas - Original Portas</title>
<meta name="description"
	content="Na Original Portas você encontra tudo para Portas de enrolar de aço automáticas com sede em  São Paulo - Guarulhos, fabrica de portas,temos portas de loja, para, portas de shopping, portas de rolo, preço baixo, descontos para serralheiro portas de aço,  automatizadores com nobreaks, motor para portões, botoeiras, portas transvision, portas perfuradas, portas para hangar, hangares, aeroportos, portas para trailer, banca de jornal, porta para container, lanchonete, meia cana, portas industriais, controle para portão, lâminas, pintura eletrostatica, contamos com serralhria própria">
<meta name="keywords"
	content="Original Portas, porta, portas de aço, aco, enrola, rolo, bandeira, cobre rolo, serralheria, serralheiro, portas aço automáticas, portas de enrolar , porta de loja, porta de shopping,porta de rolo, preço, descontos, motor portão, megatron, botoeiras,porta perfurada,furo, meia cana,transvision, frete, entrega de portas,pintura eletrostatica, portinhola, alçapão, Guarulhos, São Paulo, SP, seja revendedor">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximun-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<meta name="author" content="TwoRock.Desenv">
<link rel="canonical" href="./index">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>

<meta property="og:region" content="Brasil">
<meta property="og:title"
	content="HOME - PORTAS DE ENROLAR AUTOMÁTICAS - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="./imagens/logotipo.png">
<meta property="og:url" content="./index">
<meta property="og:description"
	content="Na Original Portas você encontra tudo para Portas de enrolar de aço automáticas com sede em  São Paulo - Guarulhos, fabrica de portas,temos portas de loja, para, portas de shopping, portas de rolo, preço baixo, descontos para serralheiro portas de aço,  automatizadores com nobreaks,motor para portões, botoeiras, portas transvision, portas perfuradas,portas para hangar, hangares, aeroportos, portas para trailer, banca de jornal, porta para container, lanchonete, meia cana, portas industriais, controle para portão, lâminas, pintura eletrostatica, contamos com serralhria própria">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="./bootstrap/css/bootstrap.css" type="text/css" />
<link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="./bootstrap/css/beta.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="./css/style.css">
<link rel="stylesheet" type='text/css' href="./css/span.css">

<!-- ----------------logo rede social----------------->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="//fonts.googleapis.com/css?family=Fjalla+One:regular&subset=latin-ext,latin" rel="stylesheet" type="text/css" />
    <link href="//fonts.googleapis.com/css?family=Poiret+One:regular&subset=cyrillic,latin-ext,latin" rel="stylesheet" type="text/css" />
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">
function disableselect(e){ 
return false 
} 
function reEnable(){ 
return true 
} 
//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script>
<!-- ---------------------Slide Cliente -------------- -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="./js/jquery-1.9.1.min.js"></script>
<!-- jssor slider scripts-->
<script type="text/javascript" src="./js/jssor.slider.min.js"></script>
<!-- -------------------Menu mobile------------------------------- -->
<script defer src="./js/vendor/modernizr-2.6.2.min.js"></script>
<!-- MENU  MOBILE -->
<script defer type="text/javascript" src="./js/jquery.slicknav.js"></script>
<!-- /MENU  MOBILE -->
<script defer src="./js/geral.js"></script>
<script src="./js/jquery-1.9.1.min.js"></script>


<script type="text/javascript">windows.location(false);</script>



<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->

</head>
<body ondragstart="return false">
<?php echo $main;



// caso a data seja igual a atual , ela vai aparecer o folder
/*
$data = date("d-m-Y");
$data2 = ('17-10-2018');
$data3 = ('28-11-2018');
if ($data <= $data2) {
    echo '<div id="home" class="overlay">
	<div class="popup">
	<a target="_blank" href="inscricao-curso"><img alt="" src="imagens/comunicado-covd-19-original.png"></a>
		<a class="close" href="#home">&times;</a>
		<div class="content">
		</div>
	</div>	
</div>';
} else{
    echo '<div id="home" class="overlay">
	<div class="popup">
	<a target="_blank" href="inscricao-curso"><img alt="" src="imagens/comunicado-covd-19-original.png"></a>
		<a class="close" href="#home">&times;</a>
		<div class="content">
		</div>
	</div>	
</div>';
    
}

; */
?>




<!-- --------------------------banner----------------------------------->
<script src="./js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="./js/jssor.slider-27.4.0.min.js" type="text/javascript"></script>
        <script src="./js/baner.js" type="text/javascript"></script>
        
 <div id="baner_anima" ><div id="jssor_1" class="container-fluid">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-004-double-tail-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="./img/double-tail-spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1440px;height:720px;overflow:hidden;" >
           <a href="./certificacao"> <div data-p="0">
                <img data-u="image" src="./img/bgiso.jpg" />
              <img data-u="caption" data-t="0" style="position:absolute;top:91px;left:-512px;width:497px;height:629px;max-width:497px;" src="./img/manwork.png" />
                <img data-u="caption" data-t="1" style="position:absolute;top:208px;left:1449px;width:677px;height:532px;max-width:677px;" src="./img/framedireito.png" />
                <img data-u="caption" data-t="2" style="position:absolute;top:559px;left:1442px;width:354px;height:31px;max-width:354px;" src="./img/texto11.png" />
                <img data-u="caption" data-t="3" style="position:absolute;top:603px;left:1441px;width:354px;height:29px;max-width:354px;" src="./img/texto12.png" />
                <img data-u="caption" data-t="4" style="position:absolute;top:640px;left:1440px;width:354px;height:33px;max-width:354px;" src="./img/texto03.png" />
                <img data-u="caption" data-t="5" style="position:absolute;top:-13px;left:-7px;width:1452px;height:746px;max-width:1452px;" src="./img/trans-amarelo.png" />
                <img data-u="caption" data-t="6" style="position:absolute;top:-328px;left:513px;width:328px;height:320px;max-width:328px;" src="./img/selo-iso.png" />
            </div></a>
       <a href="./motor-porta-enrolar-aco"><div>
                <img data-u="image" src="./img/bgq.jpg" />
                <img data-u="caption" data-t="7" style="position:absolute;top:0px;left:0px;width:1440px;height:720px;max-width:1440px;" src="./img/bgq.jpg" />
                <img data-u="caption" data-t="8" style="position:absolute;top:201px;left:-372px;width:415px;height:434px;max-width:415px;" src="./img/motor03.png" />
                <img data-u="caption" data-t="9" style="position:absolute;top:739px;left:1020px;width:377px;height:247px;max-width:377px;" src="./img/motor01.png" />
                <img data-u="caption" data-t="10" style="position:absolute;top:499px;left:642px;width:170px;height:249px;max-width:170px;" src="./img/qualidade.png" />
                <img data-u="caption" data-t="11" style="position:absolute;top:-324px;left:567px;width:300px;height:300px;max-width:300px;" src="./img/inmetro.png" />
                <img data-u="caption" data-t="12" style="position:absolute;top:508px;left:600px;width:278px;height:216px;max-width:278px;" src="./img/caixa-madeira.png" />
            </div></a>
           <a href="./inscricao-curso"> <div>
                <img data-u="image" src="./img/bgc.jpg" />
                <img style="position:absolute;top:0px;left:-64px;width:743px;height:678px;max-width:743px;" src="./img/porta2.png" />
                <img style="position:absolute;top:-1px;left:-6px;width:297px;height:621px;max-width:297px;" src="./img/treinamento.png" />
                <img style="position:absolute;top:619px;left:0px;width:1440px;height:56px;max-width:1440px;border:1px solid #000;border-radius:6px;background-clip:padding-box;box-sizing:border-box;" src="./img/faixa.png" />
                <div data-u="caption" data-t="13" style="position:absolute;top:632px;left:817px;width:587px;height:31px;mix-blend-mode:normal;font-family:'Fjalla One',sans-serif;font-size:28px;font-weight:400;color:#ffffff;font-style:italic;line-height:1.2;text-align:center;text-shadow:4px 6px 8px #000000;">MONTAGEM E MANUTENÇÃO DE PORTAS AUTOMÁTICAS</div>
                <img data-u="caption" data-t="14" style="position:absolute;top:157px;left:1000px;width:300px;height:300px;max-width:300px;box-shadow:0px 0px;" src="./img/selo-qualidade.png" />
                <img style="position:absolute;top:78px;left:292px;width:591px;height:643px;max-width:591px;" src="./img/man2.png" />
            </div></a>
           <a href="./fabricante-laminas-porta-aco"> <div>
                <img data-u="image" src="./img/lamina_baner.png" />
                <img data-u="caption" data-t="15" style="position:absolute;top:273px;left:-606px;width:602px;height:69px;max-width:602px;border:38px solid rgba(0,0,0,0.9);background-clip:padding-box;box-sizing:border-box;" src="./img/faixa.png" />
                <img data-u="caption" data-t="16" style="position:absolute;top:358px;left:-646px;width:634px;height:76px;max-width:634px;border:38px solid rgba(0,0,0,0.91);border-radius:0px;background-clip:padding-box;box-sizing:border-box;" src="./img/faixa.png" />
                <img data-u="caption" data-t="17" style="position:absolute;top:451px;left:-689px;width:681px;height:76px;max-width:681px;border:38px solid rgba(0,0,0,0.91);border-radius:0px;background-clip:padding-box;box-sizing:border-box;" src="./img/faixa.png" />
                <div data-u="caption" data-t="18" style="position:absolute;top:283px;left:-566px;width:561px;height:57px;font-family:'Poiret One',cursive;font-size:40px;font-weight:600;color:#ffffff;line-height:1.2;text-align:center;">Fabricante de Lâminas de Aço</div>
                <div data-u="caption" data-t="19" style="position:absolute;top:372px;left:-472px;width:452px;height:51px;font-family:'Poiret One',cursive;font-size:40px;font-weight:300;color:#ffffff;line-height:1.2;text-align:right;">- Transvisions (Perfurada)</div>
                <div data-u="caption" data-t="20" style="position:absolute;top:464px;left:-466px;width:452px;height:51px;font-family:'Poiret One',cursive;font-size:40px;font-weight:300;color:#ffffff;line-height:1.2;text-align:right;">- Meia Cana (Fechada)</div>
                <div data-u="caption" data-t="21" style="position:absolute;top:290px;left:209px;width:273px;height:40px;font-family:'Poiret One',cursive;font-size:40px;font-weight:800;color:#ffffff;line-height:1.2;text-align:center;">Produzimos em:</div>
                <div data-u="caption" data-t="22" style="position:absolute;top:379px;left:276px;width:340px;height:40px;font-family:'Poiret One',cursive;font-size:40px;color:#ffffff;line-height:1.2;text-align:center;">- Aço Galvanizado</div>
                <div data-u="caption" data-t="23" style="position:absolute;top:462px;left:393px;width:200px;height:40px;font-family:'Poiret One',cursive;font-size:40px;color:#ffffff;line-height:1.2;text-align:center;">- Aço Inox</div>
                <img data-u="caption" data-t="24" style="position:absolute;top:-750px;left:-7px;width:1453px;height:746px;max-width:1453px;" src="./img/trans-amarelo.png" />
            </div></a>
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb031" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>
</div>
<!-- ------------------------------------------------------------->

	<div class="container-fluid">
		<div class="row"
			style="margin-top: 80px; margin-bottom: 70px; margin-left: 0px; background-color: transparent;">
			<div class="col-md-4" id="busca1">
				<div class="caption">
					<a href="./porta-enrolar-aco-industrial"><img
						src="./imagens/porta_industrial/IMG_20150829_041443191.jpg"
						alt="Portas de aço Industriais"></a>
					<div class="caption">
						<h2>Portas Industriais</h2>
						<p>
							Não importa o tamanho da porta de a&ccedil;o;<br> Nós aceitamos o	
							desafio!,<br> deixei todo o trabalho com a gente
						</p><br>
						<p>
							<a href="./orcamento-porta-enrolar-automatica"
								class="btn btn-primary" role="button">Orçamento</a>
						</p><br>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="caption">
					<a href="./porta-enrolar-aco-comercial"><img
						src="./imagens/porta_comercial/DSC00620.png"
						alt="Portas de aço Comerciais"></a>
					<div class="caption">
						<h2>Portas comercias</h2>
						<p>Deixe a fachada do seu comercio apresentavél aos seus clientes,
							com as laminas transvision é possivel divulgar sua loja mesmo com
							as portas fechadas</p><br>
						<p>
							<a href="./orcamento-porta-enrolar-automatica"
								class="btn btn-primary" role="button">Orçamento</a>
						</p><br>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="caption">
					<a href="./porta-enrolar-aco-residencial"><img
						src="./imagens/porta_residencial/porta_residencial.jpg"
						alt="Portas de aço residenciais"></a>
					<div class="caption">
						<h2>Portas Residenciais</h2>
						<p>
							De uma cara nova a sua casa ou melhore o visual do seu
							condominio, escolha portas de qualidade , <br>Adquira j&aacute; a
							sua !
						</p><br>
						<p>
							<a href="./orcamento-porta-enrolar-automatica"
								class="btn btn-primary" role="button">Orçamento</a>
						</p><br>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid" style="margin-top: -18px;">
		<div class="row" style="background-color: transparent;">
			<div class="col-md-3">
				<div class="caption">
					<div class="caption">
						<h2>
							<a href="./quem-somos">Quem Somos</a>
						</h2>
						<p>Somos uma empresa completa, contamos com uma ampla gama de
							portas de aço, portas fabricadas sob medida para atender sua
							necessidade.</p>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="caption">
					<div class="caption">
						<h2>
							<a href="./pintura-eletrostatica-porta-enrolar-aco">Pintura
								Eletrostática</a>
						</h2>
						<p>Um tipo de tinta em p&oacute; que atrav&eacute;s de cargas
							el&eacute;tricas adere 100% ao metal diminuindo consideravelmente
							a perda de m&aacute;teria-prima, sendo possivel reciclar o
							p&oacute; n&atilde;o aderido e obtendo um aproveitamento maior do
							que a tinta liquida.</p>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="caption">
					<div class="caption">
						<h2>Produtos</h2>
						<p>Nossos produtos são fabricados com a mais alta técnologia,
							garantindo a segurança de seu estabelecimento residencial,
							comercial ou industrial.</p>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="caption">
					<div class="caption">
						<h2>
							<a href="./serralheiro-portas-aco">Serralheiro</a>
						</h2>
						<p>Se torne nosso parceiro e tenha descontos especias, conte com a
							gente para fabricar suas portas de aço automáticas.</p>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="caption">
					<div class="caption">
						<h2>Cursos de Como montar uma porta de aço</h2>
						<p>
							Ensinamos a montar e instalar uma porta de aço automatica, de
							forma simples e muito prática. <br />Consulte as turmas
							disponiveis
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="caption">
					<div class="caption">
						<h2>
							<a href="./video-como-montar-instalar-porta-aco-automatica">Assista
								nossos videos</a>
						</h2>
						<p>
							Aprenda a montar e instalar portas de enrolar de aço apenas
							assistindo há nossos videos, fique por dentro das novidades
							acessando o nosso canal do <a
								href="https://www.youtube.com/channel/UCPINybml0zMMiR-c_XrsjHQ">Youtube</a>
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="caption">
					<div class="caption">
						<h2>
							<a href="https://goo.gl/photos/YTenbCXuHDhy9i97A">Visite a nossa
								galeria</a>
						</h2>
						<p>Veja diversas fotos de portas instaladas em residências,
							comércios e indústrias</p>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="caption">
					<div class="caption">
						<h2>
							<a href="http://www.automatizadoresmegatron.com.br/">Parceria</a>
						</h2>
						<p>Com busca na qualidade e bem estar de nossos clientes,
							adquirimos parceria com a fábrica de Automatizadores Megatron
							certificada pelo ISO-9001 e com selo inmetro para seus produtos.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ----------------------------- Slide Cliente------------------- -->
	<div class="container" style="margin-top: 20px;">
		<div class="row">
			<div
				class="col-md-7 col-md-offset-5 col-lg-2  col-md-2 col-sm-4 col-xs-6">
				<div id="text_cliente"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="text_cliente">
					<h2 style="font-family: BebasKai; text-align: center;">Conheça
						algumas empresas que já adquiriram nossos produtos</h2>
					<br>
				</div>
			</div>
		</div>
	</div>
 <script src="./js/clientes.js" type="text/javascript"></script>

	<div id="jssor_2"
		style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 800px; height: 100px; overflow: hidden; visibility: hidden;">
		<!-- Loading Screen -->
		<div data-u="loading" class="jssorl-009-spin"
			style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; text-align: center; background-color: rgba(0, 0, 0, 0.7);">
			<img
				style="margin-top: -19px; position: relative; width: 38px; height: 38px;"
				src="" />
		</div>
		<div data-u="slides2"
			style="cursor: default; position: relative; top: 0px; left: 0px; width: 1000px; height: 100px; overflow: hidden;">

			<div>
				<a href="https://www.adidas.com.br/homem-outlet" target="_blank"> <img
					data-u="image" src="./imagens/clientes/adidas.png" />
				</a>
			</div>
			<div>
				<a href="https://grupoallnet.com.br/" target="_blank"> <img
					data-u="image" src="./imagens/clientes/allnet.png" />
				</a>
			</div>
			<div>
				<a href="https://www.americanas.com.br" target="_blank"> <img
					data-u="image" src="./imagens/clientes/americanas.jpg" />
				</a>
			</div>
			<div>
				<a href="https://www.correios.com" target="_blank"> <img
					data-u="image" src="./imagens/clientes/correios.png" />
				</a>
			</div>
			<div>
				<a href="https://www.burgerking.com.br/" target="_blank"> <img
					data-u="image" src="./imagens/clientes/burgerking.png" />
				</a>
			</div>
			<div>
				<a href="https://www.cacaushow.com.br/" target="_blank"> <img
					data-u="image" src="./imagens/clientes/cacaushow.jpg" />
				</a>
			</div>
			<div>
				<a href="https://www.carrefour.com.br" target="_blank"> <img
					data-u="image" src="./imagens/clientes/carrefour.png" />
				</a>
			</div>
			<div>
				<a href="https://www.marilan.com/" target="_blank"> <img
					data-u="image" src="./imagens/clientes/marilan.png" />
				</a>
			</div>
			<div>
				<a
					href="https://www.saopaulo.sp.gov.br/spnoticias/ultimas-noticias/centro-de-treinamento-paraolimpico-brasileiro-abre-nesta-segunda/"
					target="_blank"> <img data-u="image"
					src="./imagens/clientes/centroparaolimpico.png" />
				</a>
			</div>
			<div>
				<a href="https://institucional.habibs.com.br/promocoes/"
					target="_blank"> <img data-u="image"
					src="./imagens/clientes/habibbs.png" />
				</a>
			</div>
		</div>
	</div>
	<script type="text/javascript"> jssor_2_slider_init(); </script>
	<div class="linha_vertical-2"></div>



<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array( '5511959681162', '5511959065418','5511949523408','5511953536427','5511953874598'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	

<?php echo $footer;?>
</body>
</html>

