<?php require 'main.php';
require 'footer.php';?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-78104226-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-78104226-1');
</script>
<title>Porta Industrial Automática para Hangares, galpões, Fabricas e Industrias - Original Portas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<base>
<meta name="description"
	content="As portas industriais automáticas de enrolar normalmente são feitas em aço, o que lhes garante uma maior resistência e uma maior durabilidade">
<meta name="keywords"
	content="Portas Industriais Automáticas, portas,aço, automáticas, porta de enrolar ,portas para hangares, portas para fabricas, portas para galpões, motor porta de enrolar industrial,">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="-23.4664636;-46.5701426">
<meta name="geo.placename" content="São Paulo-SP">
<meta name="geo.region" content="SP-BR">
<meta name="ICBM" content="-23.4664636;-46.5701426">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="porta-enrolar-aco-industrial.php">
<meta name="author" content="TwoRock">
<link rel="shortcut icon" type='image/x-icon'  href="./favicon.ico">
<link sizes="32x32"  type='image/x-icon' href="./favicon.png" rel="icon"/>
<meta property="og:region" content="Brasil">
<meta property="og:title" content="Portas Industriais Automáticas - Original Portas">
<meta property="og:type" content="article">
<meta property="og:image" content="imagens/logotipo.png">
<meta property="og:url" content="porta-enrolar-aco-industrial.php">
<meta property="og:description" content="As portas industriais automáticas de enrolar normalmente são feitas em aço, o que lhes garante uma maior resistência e uma maior durabilidade">
<meta property="og:site_name" content="Original Portas">
<!------------------- bootstrap--------------------->
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"type="text/css" />
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="bootstrap/css/beta.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
<!-------------------- css    ---------------------->
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<link rel="stylesheet" type='text/css' href="css/style.css">
<!-- ----------------logo rede social----------------->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-----------------bloquer mouse direito------------------>
<script type="text/javascript">

function disableselect(e){ 
return false 
}
function reEnable(){ 
return true 
} 
//if IE4+ 
document.onselectstart=new Function ("return false") 
document.oncontextmenu=new Function ("return false") 
//if NS6 
if (window.sidebar){ 
document.onmousedown=disableselect 
document.onclick=reEnable 
} 
</script> 
<!-- -------------------Menu mobile------------------------------- -->
<script src="js/jquery-1.9.1.min.js"></script>
<script defer src="js/vendor/modernizr-2.6.2.min.js"></script>
<script defer type="text/javascript" src="js/jquery.slicknav.js"></script>
<script defer src="js/geral.js"></script>

<!-- Chat Jivo  -->


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'z2LoP4MASg';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- ------------------------------------  -->
</head>
<body ondragstart="return false">
<?php echo $main; ?>
<div class="container-fluid">
		<div class="row">
			<div id="ban_eletro3">
				<img src="imagens/porta_industrial/porta-enrolar-industrial.png" width="100%" height="auto">
			</div>
		</div>
	</div>




<div class="container">
    <div class="class_port">
    	<div class="row">
			<div id="id_port" class="col-md-6" >
				<h1> PORTA DE AÇO INDÚSTRIAL</h1>
					   	   <hr style="width: auto; height: 2px; background-color: #ccc;">
				    <p> Nossas portas de aço automáticas industriais, são fundamentais para garantir a segurança e praticidade para sua empresa. 
                        Todas as nossas portas são sob medida, atendendo às mais específicas necessidades, independente do tamanho.</p>
                        <p>As portas industriais automáticas de enrolar normalmente são feitas em aço, o que lhes garante uma maior resistência e durabilidade.</p>
					<br>
					<p> A maior porta industrial automática que produzimos e instalamos foi na fábrica de Alimentos Marilan, mesmo quando
					    diversas empresas especializadas em portas de aço recusaram o projeto, a Original Portas foi a única que aceitou o desafio, fabricamos as lâminas de aço para porta de enrolar industrial e também realizamos a instalação da porta de aço industrial</p>
				    <br>
				    <p> Portas Industriais são indicadas em:</p>
				    <p> 
				    - Indústrias<br>
				    - Galpões<br>
				    - Fábricas<br>
				    - Hangares<br>
				    - Usinas entre outros.</p>
				<p>Faça um orçamento conosco, além de cumprirmos todas as normas de qualidade exigidas, damos garantia de nossos produtos, entre em contato e tire todas as suas dúvidas.</p>
             </div>
            <div class="col-md-6" ><img alt="Porta indústrial" src="imagens/porta_industrial/fab_marilan.png">
		  </div>
	      <br>
	  </div>
   </div>        
</div>

<div id="whatsapp-chat">
<a href="https://wa.me/<?php  $array = array( '5511959681162', '5511959065418','5511949523408','5511953536427','5511953874598'); shuffle( $array ); echo current( $array );?>?text=Contato%20atraves%20do%20site%20Original%20Portas%20" target="_blank"><br><i class="fa fa-whatsapp" style="font-size:40px;"></i></a>
</div>	
<?php echo $footer;?>
</body>
</html>